import uma from './utils/umtrack-wx-game/lib/index.js'
uma.init({
  appKey:'60f96506ff4d74541c809fef',
  useOpenid:true,// default true
  autoGetOpenid:true,
  debug:true,
  uploadUserInfo:true// 上传用户信息，上传后可以看到有用户头像和昵称的分享信息
});
// 主程序
"use strict";

require('adapter-min.js');

__globalAdapter.init();

requirePlugin('cocos');

__globalAdapter.adaptEngine();

require('./ccRequire');

require('./src/settings'); // Introduce Cocos Service here


require('./main'); // TODO: move to common
// Adjust devicePixelRatio


cc.view._maxPixelRatio = 4;

if (cc.sys.platform !== cc.sys.WECHAT_GAME_SUB) {
  // Release Image objects after uploaded gl texture
  cc.macro.CLEANUP_IMAGE_CACHE = true;
}

window.boot();