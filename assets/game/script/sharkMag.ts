

const { ccclass, property } = cc._decorator;

@ccclass
export default class sharkMag {
    private static _instance: sharkMag = null;
    public static get Ins(): sharkMag {
        if (!this._instance || this._instance == null) {
            this._instance = new sharkMag();
        }
        return this._instance;
    }
    hp: Number;
    div: Number;
    power: Number;
    outLine: Number;
}
