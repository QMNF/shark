import GameMag from "../../home/script/manage/GameMag";
import StorageMag, { StorageKey } from "../../home/script/manage/StorageMag";


const { ccclass, property } = cc._decorator;

@ccclass
export default class ResultDialog extends cc.Component {

    @property({ type: cc.Node, tooltip: "胜利界面" })
    successNode: cc.Node = null;
    @property({ type: cc.Node, tooltip: "失败界面" })
    failNode: cc.Node = null;
    @property({ type: cc.Node, tooltip: "重生界面" })
    reLiveNode: cc.Node = null;
    @property({ type: cc.Node, tooltip: "一倍奖励" })
    continueBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "三倍奖励" })
    videoBtn: cc.Node = null;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    @property({ type: cc.Node, tooltip: "返回按钮组" })
    winBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "返回关卡" })
    RetLevelBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "下一关" })
    NextLevelBtn: cc.Node = null;
<<<<<<< HEAD
=======
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    @property({ type: cc.Node, tooltip: "一倍奖励退出" })
    returnBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "三倍奖励退出" })
    videoReturnBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "重生" })
    reliveBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "返回" })
    liveReturnBtn: cc.Node = null;
    @property({ type: cc.Label, tooltip: "胜利金币" })
    winCoinLab: cc.Label = null;
    @property({ type: cc.Label, tooltip: "胜利钻石" })
    winODLab: cc.Label = null;
    @property({ type: cc.Label, tooltip: "失败金币" })
    loseCoinLab: cc.Label = null;
    @property({ type: cc.Label, tooltip: "失败钻石" })
    loseODLab: cc.Label = null;
    @property(cc.Node)
    icon: cc.Node = null;
    @property(cc.Node)
    ODicon: cc.Node = null;
    @property(cc.Label)
    timer: cc.Label = null;

    data: any = null

    iconPos: cc.Vec3;
    ODiconPos: cc.Vec3;
    iconStartPos: cc.Vec2;
    ODiconStartPos: cc.Vec2;

    ReLiveWeitTime: number = 5



    onLoad() {
        this.iconStartPos = cc.v2(this.icon.x, this.icon.y)
        this.ODiconStartPos = cc.v2(this.ODicon.x, this.ODicon.y)
        this.continueBtn.on(cc.Node.EventType.TOUCH_START, this.onContinue, this);
        this.videoBtn.on(cc.Node.EventType.TOUCH_START, this.onVideo, this);
        this.returnBtn.on(cc.Node.EventType.TOUCH_END, this.onReturn, this);
        this.videoReturnBtn.on(cc.Node.EventType.TOUCH_END, this.onvideoReturn, this);
        this.reliveBtn.on(cc.Node.EventType.TOUCH_END, this.onRelive, this);
        this.liveReturnBtn.on(cc.Node.EventType.TOUCH_END, this.onNoreLive, this);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.RetLevelBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("onReturn", true)
        })
        this.NextLevelBtn.on(cc.Node.EventType.TOUCH_END, () => {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10009", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
            }
            cc.game.emit("nextLevel")
        })
        this.node.on("winSelect", this.winSelect, this)
    }

    winSelect() {
        this.winBtn.active = true;
        this.continueBtn.parent.active = false;
    }

    isSuccess(flag: number, target: cc.Node, data) {
        this.data = data;
        this.icon.active = false;
        this.ODicon.active = false;
        this.continueBtn.parent.active = true;
        this.winBtn.active = false;
<<<<<<< HEAD
=======
=======
    }
    isSuccess(flag: number, target: cc.Node, data) {
        this.data = data;
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.iconPos = target.parent.convertToNodeSpaceAR(target.convertToWorldSpaceAR(target.getChildByName("coin").position));
        this.ODiconPos = target.parent.convertToNodeSpaceAR(target.convertToWorldSpaceAR(target.getChildByName("diamond").position));
        this.failNode.active = false;
        this.successNode.active = false;
        this.reLiveNode.active = false;
        if (flag == 1) {
            this.successNode.active = true;
            this.setSucccessGift(data);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10005", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
            }
<<<<<<< HEAD
=======
=======
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            return;
        }
        if (flag == 2) {
            this.reLiveNode.active = true;
            this.schedule(this.ReLiveTimer, 1)
<<<<<<< HEAD
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
            return;
        }
        if (flag == 2) {
            this.reLiveNode.active = true;
            this.schedule(this.ReLiveTimer, 1)
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            return;
        }
        if (cc.sys.platform === cc.sys.WECHAT_GAME) {
            // @ts-ignore
            wx.uma.trackEvent("10006", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
        }
        this.failNode.active = true;
        this.setLoseGift(data);
    }
    setSucccessGift(data) {
        this.winCoinLab.string = data.winIcon + ""
        this.winODLab.string = data.winOD + ""
    }
    setLoseGift(data) {
        this.loseCoinLab.string = Math.floor(data.winIcon / 2) + ""
        this.loseODLab.string = Math.floor(data.winOD / 2) + ""
    }
    ReLiveTimer() {
        this.ReLiveWeitTime -= 1
        this.timer.string = this.ReLiveWeitTime + ""
        if (this.ReLiveWeitTime <= 0) {
            this.unschedule(this.ReLiveTimer);
            this.ReLiveWeitTime = 5
            this.timer.string = this.ReLiveWeitTime + ""
            this.onNoreLive();
        }
    }
    onContinue() {
        this.getRewared(1, true);
    }
    onVideo() {
        this.getRewared(3, true);
    }
    getRewared(rate: number, isNextLevel: boolean) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if (cc.sys.platform === cc.sys.WECHAT_GAME) {
            if (rate == 1 || rate == 0.5) {
                // @ts-ignore
                wx.uma.trackEvent("10007", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
            } else {
                // @ts-ignore
                wx.uma.trackEvent("10008", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
            }
        }
        this.icon.active = true
        this.ODicon.active = true

        // console.error("进入结算", this.iconPos, this.ODiconPos);
        this.icon.getComponent(cc.Animation).off(cc.Animation.EventType.FINISHED)
        this.ODicon.getComponent(cc.Animation).off(cc.Animation.EventType.FINISHED)

        this.icon.getComponent(cc.Animation).on(cc.Animation.EventType.FINISHED, () => {
            this.icon.active = false
            // console.error("进入结算");
            cc.game.emit("addCoin", this.data.winIcon * rate);
        })
        this.ODicon.getComponent(cc.Animation).on(cc.Animation.EventType.FINISHED, () => {
            this.ODicon.active = false
            // console.error("进入结算");
            cc.game.emit("addDiamond", this.data.winOD * rate);
            cc.tween(this.node)
                .delay(0.1)
                .call(() => {
                    if (isNextLevel) {
                        this.node.emit("winSelect")
                        // cc.game.emit("nextLevel")
                    } else {
                        // this.successNode.active = false;
                        // this.failNode.active = false;            
                        cc.game.emit("onReturn", false)
                    }
                    // this.node.active = false;
                })
                .start();
        })
        this.icon.getComponent(cc.Animation).play();
        this.ODicon.getComponent(cc.Animation).play();
<<<<<<< HEAD
=======
=======
        console.error("进入结算", this.iconPos, this.ODiconPos);
        this.successNode.active = false;
        this.failNode.active = false;
        this.icon.stopAllActions()
        this.ODicon.stopAllActions()
        this.icon.active = true;
        this.icon.scale = 1.4;
        this.icon.setPosition(this.iconStartPos)
        this.ODicon.active = true;
        this.ODicon.scale = 1.4;
        this.ODicon.setPosition(this.ODiconStartPos)
        cc.tween(this.icon)
            .then(cc.spawn(cc.moveTo(0.3, cc.v2(this.iconPos.x, this.iconPos.y)), cc.scaleTo(0.3, 0)))
            .call(() => {
                console.error("进入结算");
                cc.game.emit("addCoin", this.data.winIcon * rate);
            })
            .start();
        cc.tween(this.ODicon)
            .then(cc.spawn(cc.moveTo(0.3, cc.v2(this.ODiconPos.x, this.ODiconPos.y)), cc.scaleTo(0.3, 0)))
            .call(() => {
                console.error("进入结算");
                cc.game.emit("addDiamond", this.data.winOD * rate);
            })
            .delay(0.1)
            .call(() => {
                if (isNextLevel) {
                    cc.game.emit("nextLevel")
                } else {
                    cc.game.emit("onReturn")
                }
                this.node.active = false;
            })
            .start();

>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }
    onReturn() {
        this.getRewared(0.5, false);
        // cc.game.emit("onReturn");
    }
    onvideoReturn() {
        this.getRewared(1.5, false);
    }
    onRelive() {
        this.unschedule(this.ReLiveTimer);
        this.ReLiveWeitTime = 5
        this.timer.string = this.ReLiveWeitTime + ""
        cc.game.emit("relive");
<<<<<<< HEAD
        // console.log("relive")
=======
<<<<<<< HEAD
        // console.log("relive")
=======
        console.log("relive")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }
    onNoreLive() {
        GameMag.Ins.gameOver = true;
        cc.game.emit("gameOver", 0)
    }
}
