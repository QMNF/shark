import PoolMag from "../../home/script/manage/PoolMag";
import ToolMag from "../../home/script/manage/ToolMag";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Bullet extends cc.Component {

    angle: number = null;
    speed: number = 500;

    onEnable() {

    }

    setAngle(angle: number) {
        this.node.angle = angle;
        let radians = cc.misc.degreesToRadians(angle);
        const x = this.node.x + Math.cos(radians) * 2000;
        const y = this.node.y + Math.sin(radians) * 2000;
        cc.tween(this.node)
            .to(2, { position: cc.v3(x, y, 0) })
            .call(() => {
                // this.node.stopAllActions();
                PoolMag.Ins.putBullet(this.node);
            })
            .start();
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
<<<<<<< HEAD
        if (other.tag == 1 || other.tag == 10 || other.tag == 101 || other.tag == 201) return;
=======
<<<<<<< HEAD
        if (other.tag == 1 || other.tag == 10 || other.tag == 101 || other.tag == 201) return;
=======
        if (other.tag == 1 || other.tag == 10) return;
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.node.stopAllActions();
        PoolMag.Ins.putBullet(this.node);
    }
}
