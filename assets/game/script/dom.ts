// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameMag from "../../home/script/manage/GameMag";
import PoolMag from "../../home/script/manage/PoolMag";
import ToolMag from "../../home/script/manage/ToolMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class dom extends cc.Component {


    angle: number = null;
    speed: number = 500;

    @property(cc.Animation)
    fire: cc.Animation = null;

    onEnable() {

    }

    setAngle(angle: number) {
        this.node.angle = angle;
        this.fire.play();
        let radians = cc.misc.degreesToRadians(angle);
        const x = this.node.x + Math.cos(radians) * 2000;
        const y = this.node.y + Math.sin(radians) * 2000;
        cc.tween(this.node)
            .then(cc.moveTo(1.3, cc.v2(x, y)).easing(cc.easeCircleActionIn()))
            .call(() => {
                // if (this.node.parent)
                PoolMag.Ins.putDom(this.node);
            })
            .start();
    }

    showBoom() {
        const boom = PoolMag.Ins.getBoomNode();
        boom.parent = cc.find("Canvas");
        boom.position = this.node.position;
        this.node.stopAllActions();
        PoolMag.Ins.putDom(this.node);
        ToolMag.Ins.playDragonBone(boom, "newAnimation", 1, () => {
            PoolMag.Ins.putBoomNode(boom);
        })
    }

    // update (dt) {}
}
