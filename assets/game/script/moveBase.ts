import PoolMag, { PoolType } from "../../home/script/manage/PoolMag";
import GameMag from "../../home/script/manage/GameMag";

enum MoveType {
    BG,
    Wall,
    Water,
    WaterShade,
}
const { ccclass, property } = cc._decorator;

@ccclass
export default class MoveBase extends cc.Component {

    @property()
    speed: number = 0;
    @property({ tooltip: "检测间隔" })
    testTime: number = 0;
    @property({ type: cc.Enum(MoveType) })
    myType: number = MoveType.BG;

    count: number = 0;
    width: number = 0;
    testRect: cc.Rect = null;
    recovery: boolean = null;
    target: cc.Node = null;
    poolType: PoolType = null;

    onLoad() {
        this.testRect = cc.find("Canvas/gameMain/moveTestRect").getBoundingBoxToWorld();
        this.width = this.node.children[0].width;
        this.count = this.node.childrenCount;
    }

    sche: number = 0;
    update(dt) {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        this.node.x -= this.speed * dt;
        this.sche += dt;
        if (this.sche > this.testTime) {
            this.sche = 0;
            if (!this.testRect.containsRect(this.node.children[0].children[0].getBoundingBoxToWorld()) && !this.recovery) {
                // console.log("111111111", this.count);
                this.recovery = true;
                this.count++;
                PoolMag.Ins.putPoolNode(this.myType, this.node.children[0]);
                this.target = PoolMag.Ins.getPoolNode(this.myType);
                this.target.x = this.width * this.count;
                this.node.addChild(this.target);
                this.scheduleOnce(() => {
                    this.recovery = false;
                }, 0);
            }
        }
    }
    onDisable() {
        this.unscheduleAllCallbacks();
    }
}
