import PoolMag from "../../home/script/manage/PoolMag";


const { ccclass, property } = cc._decorator;

@ccclass
export default class BossBullet extends cc.Component {

    power: number = 0;

    init(angle: number, power: number) {
        this.power = power;
        this.node.angle = angle;
        let radians = cc.misc.degreesToRadians(angle);
        const x = this.node.x + Math.cos(radians) * 2000;
        const y = this.node.y + Math.sin(radians) * 2000;
        cc.tween(this.node)
            .to(3, { position: cc.v3(x, y, 0) })
            .call(() => {
                PoolMag.Ins.putBossBullet(this.node);
            })
            .start();
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.tag != 1) return;
        this.node.stopAllActions();
        PoolMag.Ins.putBossBullet(this.node);
    }
}
