import GameMag from "../../home/script/manage/GameMag";
import ToolMag from "../../home/script/manage/ToolMag";
import StorageMag, { StorageKey } from "../../home/script/manage/StorageMag";
import BossBullet from "./bossBullet";
import Elite from "./enemy/elite";
import PoolMag from "../../home/script/manage/PoolMag";
import GameMain from "./gameMain";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Shark extends cc.Component {

    @property(cc.Node)
    gun: cc.Node = null;
    @property(cc.ProgressBar)
    bloodBar: cc.ProgressBar = null;
    @property(GameMain)
    gameMain: GameMain = null;

    bitedNode: cc.Node = null;

    isDie: boolean = false;
    initHp: number = null;
    initDiv: number = null;
    initPower: number = null;
    initBitePower: number = null;
    ActiveSkill: any[] = [];
    PassiveSkills: any[] = [];

    loadShark() {
<<<<<<< HEAD
        // console.error("加载鲨鱼")
=======
<<<<<<< HEAD
        // console.error("加载鲨鱼")
=======
        console.error("加载鲨鱼")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        cc.game.on("changeSharkData", this.AttrData, this);
    }

    onEnable() {
        this.initHp = 0;
        this.initDiv = 0;
        this.initPower = 0;
        this.initBitePower = 0;
        this.isDie = false;
        this.node.angle = 0;
        this.gun.active = true;
        this.bloodBar.progress = 1;
        this.bloodBar.node.active = false;
        GameMag.Ins.sharkChangeSkin(this.node);
        ToolMag.Ins.playDragonBone(this.node, "stay", 0);
    }
    /**更新鲨鱼的属性 */
    AttrData() {
        this.initHp = 0;
        this.initDiv = 0;
        this.initPower = 0;
        this.initBitePower = 0;
        this.ActiveSkill = [];
        this.PassiveSkills = [];
        const sharkJson = GameMag.Ins.sharkJson;
        const usingData = StorageMag.Ins.getStorage(StorageKey.Using);
        let sumHpArr = [
            StorageMag.Ins.getStorage(StorageKey.Skill)[0].attr,
            sharkJson[1][usingData.yushen].hp * usingData.yushenStar,
            sharkJson[2][usingData.yuqi].hp * usingData.yuqiStar,
            sharkJson[3][usingData.yuwei].hp * usingData.yuweiStar,
            sharkJson[4][usingData.yuzui].hp * usingData.yuzuiStar,
            sharkJson[5][usingData.futou].hp * usingData.futouStar
        ];
        let sumPowerArr = [
            StorageMag.Ins.getStorage(StorageKey.Skill)[1].attr,
            sharkJson[0][usingData.gun].power * usingData.gunStar,
            sharkJson[1][usingData.yushen].power * usingData.yushenStar,
            sharkJson[2][usingData.yuqi].power * usingData.yuqiStar,
            sharkJson[3][usingData.yuwei].power * usingData.yuweiStar,
            sharkJson[4][usingData.yuzui].power * usingData.yuzuiStar,
            sharkJson[5][usingData.futou].power * usingData.futouStar

        ];
        let sumDivArr = [
            StorageMag.Ins.getStorage(StorageKey.Skill)[2].attr,
            sharkJson[0][usingData.gun].div * usingData.gunStar,
            sharkJson[1][usingData.yushen].div * usingData.yushenStar,
            sharkJson[2][usingData.yuqi].div * usingData.yuqiStar,
            sharkJson[3][usingData.yuwei].div * usingData.yuweiStar,
            sharkJson[4][usingData.yuzui].div * usingData.yuzuiStar,
            sharkJson[5][usingData.futou].div * usingData.futouStar
        ];
        let sumBitePowerArr = [
            sharkJson[0][usingData.gun].bitePower * usingData.gunStar,
            sharkJson[1][usingData.yushen].bitePower * usingData.yushenStar,
            sharkJson[2][usingData.yuqi].bitePower * usingData.yuqiStar,
            sharkJson[3][usingData.yuwei].bitePower * usingData.yuweiStar,
            sharkJson[4][usingData.yuzui].bitePower * usingData.yuzuiStar,
            sharkJson[5][usingData.futou].bitePower * usingData.futouStar,
        ];
        let sumSkillArr = [
            sharkJson[0][usingData.gun].skill,
            sharkJson[1][usingData.yushen].skill,
            sharkJson[2][usingData.yuqi].skill,
            sharkJson[3][usingData.yuwei].skill,
            sharkJson[4][usingData.yuzui].skill,
            sharkJson[5][usingData.futou].skill,
        ]
        let sumSkillTypeArr = [
            sharkJson[0][usingData.gun].skillType,
            sharkJson[1][usingData.yushen].skillType,
            sharkJson[2][usingData.yuqi].skillType,
            sharkJson[3][usingData.yuwei].skillType,
            sharkJson[4][usingData.yuzui].skillType,
            sharkJson[5][usingData.futou].skillType,
        ]
        let sumSkillPowerArr = [
            sharkJson[0][usingData.gun].SkillPower * usingData.gunStar,
            sharkJson[1][usingData.yushen].SkillPower * usingData.yushenStar,
            sharkJson[2][usingData.yuqi].SkillPower * usingData.yuqiStar,
            sharkJson[3][usingData.yuwei].SkillPower * usingData.yuweiStar,
            sharkJson[4][usingData.yuzui].SkillPower * usingData.yuzuiStar,
            sharkJson[5][usingData.futou].SkillPower * usingData.futouStar,
        ]

        sumHpArr.forEach((num, i) => {
            this.initHp += num;
        });
        sumPowerArr.forEach((num, i) => {
            this.initPower += num
        })
        sumBitePowerArr.forEach((num, i) => {
            this.initBitePower += num
        })
        sumDivArr.forEach((num, i) => {
            this.initDiv += num
        })
        sumSkillTypeArr.forEach((type, i) => {
            if (type == "主动") {
                this.ActiveSkill.push({ name: sumSkillArr[i], power: sumSkillPowerArr[i] });
            } else {
                this.PassiveSkills.push({ name: sumSkillArr[i], power: sumSkillPowerArr[i] })
            }
        })
        // console.log("this.ActiveSkill", this.ActiveSkill, "this.PassiveSkills", this.PassiveSkills);
        // console.log("攻击", sumHpArr, sumPowerArr)
        this.PassiveSkills.forEach((element, i) => {
            if (element.name == "强力") {
                this.initPower += element.power;
            } else if (element.name == "健体") {
                this.initHp += element.power;
            } else if (element.name == "坚固") {
                this.initDiv += element.power;
            } else if (element.name == "锋利") {
                this.initPower += element.power;
            }
        })
        GameMag.Ins.sharkHp = this.initHp;
        GameMag.Ins.gameHp = this.initHp;
        GameMag.Ins.sharkDiv = this.initDiv;
        GameMag.Ins.sharkPower = this.initPower;;
        GameMag.Ins.sharkBitePower = this.initBitePower;
        GameMag.Ins.ActiveSkill = this.ActiveSkill;
        GameMag.Ins.PassiveSkills = this.PassiveSkills;

        // console.log("血量", this.initHp, "攻击力", this.initPower, "咬合力", this.initBitePower, "防御", this.initDiv);
        this.gameMain.SkillInfo(this.ActiveSkill, this.PassiveSkills);
        cc.game.emit("changeEnd");
    }
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (this.isDie || GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        if (other.tag == 10) { //boss子弹
            this.bloodBar.node.active = true;
            this.unschedule(this.hideBlood);
            this.scheduleOnce(this.hideBlood, 2);
            let dmg = 0;
            if (other.node.getComponent(BossBullet).power - GameMag.Ins.sharkDiv <= 0) {
                dmg = 1
            } else {
                dmg = other.node.getComponent(BossBullet).power - GameMag.Ins.sharkDiv
            }
            GameMag.Ins.gameHp -= dmg
            this.bloodBar.progress = GameMag.Ins.gameHp / GameMag.Ins.sharkHp;
            if (this.bloodBar.progress <= 0) {
                this.isDie = true;
                this.gun.active = false;
                // GameMag.Ins.gameOver = true;
                GameMag.Ins.gamePause = true;
                this.bloodBar.node.active = false;
                ToolMag.Ins.playDragonBone(this.node, "die", 1, () => {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    if (GameMag.Ins.isReLive) {
                        cc.game.emit("gameOver", 2)
                    } else {
                        cc.game.emit("gameOver", 0);//游戏失败
                    }
                });
                return;
<<<<<<< HEAD
=======
=======
                    cc.game.emit("gameOver", 2);//游戏失败
                });
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            }
        }
        cc.game.emit("addKnifePower");
        if (other.tag == 13 && !GameMag.Ins.sharkBiting) {
            if (other.getComponent(Elite).isdie()) return
            ToolMag.Ins.playDragonBone(this.node, "bite", 0);
            this.node.setPosition(other.node.x - 100, GameMag.Ins.airPosY - 40)
            other.node.getComponent(Elite).bited(other.node);
            this.bitedNode = other.node;
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateLong();
            return;
        }
<<<<<<< HEAD
        if (other.tag == 2 || other.tag == 201 || other.tag == 101 || other.tag == 10) return;
        cc.game.emit("addKnifePower");
=======
<<<<<<< HEAD
        if (other.tag == 2 || other.tag == 201 || other.tag == 101 || other.tag == 10) return;
        cc.game.emit("addKnifePower");
=======
        if (other.tag == 2 || other.tag == 201 || other.tag == 101) return;
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        let node = this.node;
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        ToolMag.Ins.playDragonBone(this.node, "bite_1", 1, () => {
<<<<<<< HEAD
            // console.log('回到stay状态')
=======
<<<<<<< HEAD
            // console.log('回到stay状态')
=======
            console.log('回到stay状态')
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            ToolMag.Ins.playDragonBone(node, "stay", 0);
        });
    }
    update(dt) {

        this.bloodBar.node.setPosition(this.node.x, this.node.y + 120);
    }
    addhp(addHp) {
        GameMag.Ins.gameHp += addHp
        if (GameMag.Ins.gameHp > GameMag.Ins.sharkHp) {
            GameMag.Ins.gameHp = GameMag.Ins.sharkHp
        }
        this.bloodBar.progress = GameMag.Ins.gameHp / GameMag.Ins.sharkHp
        this.bloodBar.node.active = true;
        this.unschedule(this.hideBlood);
        this.scheduleOnce(this.hideBlood, 2);
    }
    hideBlood() {
        this.bloodBar.node.active = false;
    }
    onCollisionExit(other: cc.BoxCollider, self: cc.BoxCollider) {

    }
    onDestroy() {
        cc.game.off("changeSharkData", this.AttrData, this);
    }
}
