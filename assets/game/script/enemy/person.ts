import ToolMag from "../../../home/script/manage/ToolMag";
import GameMag from "../../../home/script/manage/GameMag";
import PoolMag from "../../../home/script/manage/PoolMag";
import SoundMag from "../../../home/script/manage/SoundMag";
import StorageMag, { StorageKey } from "../../../home/script/manage/StorageMag";

enum Actions {
    Walk0 = "walk_0",
    Walk1 = "walk_1", //随机散步
    Run0 = "walk_2",
    Run1 = "walk_3", //随机惊慌奔跑
    Swimming0 = "swimming_0",
    Swimming1 = "swimming_1",
    Injured = "injured", //受伤
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class Person extends cc.Component {


    isSwimming: boolean = null;
    isDie: boolean = null;
    flying: boolean = null;
    action: Actions = Actions.Walk0;
    speed: number = 300;
    maxHp: number = 100
    PersonHp: number = 100;
    slotArr: string[] = ["chest", "Arm_R", "hand_R", "Arm_L", "hand_L", "head", "Eyebrow_R", "Eyebrow_L", "thigh_L", "lower_leg_L", "foot_L", "thigh_R", "lower_leg_R", "foot_R"];

    firstShow() {
        this.setSkin();
        this.node.x = ToolMag.Ins.randomNum(-300, 550);
    }
    /**
     * @param flag  true:在地面   false:在水面
     */
    setPos(flag: boolean) {
        this.setSkin();
        this.node.x = (ToolMag.Ins.screenSize.width / 2) + ToolMag.Ins.randomNum(10, 150);
        if (flag) {
            this.node.y = GameMag.Ins.groundPosY;
            GameMag.Ins.sharkJumping ? this.runAction() : this.walkAction();
            return;
        }
        this.node.y = GameMag.Ins.waterPosY;
        this.swimmingAction();
    }
    setSkin() {
        const armature = this.getComponent(dragonBones.ArmatureDisplay).armature();
        const index = ToolMag.Ins.randomNum(0, 8);//总共8只小人
        for (let i = 0; i < this.slotArr.length; i++) {
            armature.getSlot(this.slotArr[i]).displayIndex = index;
        }
    }
    /***
     * @param flying  从香蕉船飞出来的过程不让攻击
     */
    isFlyByBoat(flying: boolean) {
        this.flying = flying;
    }
    onEnable() {
        this.isDie = false;
        this.isSwimming = false;
        this.speed = Math.floor(ToolMag.Ins.randomNum(200, 270));
        this.PersonHp = 50 * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5));
        cc.game.on("freshPersonRun", this.freshPersonRun, this);
        // cc.audioEngine.stopAllEffects();
    }
    onDisable() {
        this.node.stopAllActions();
        cc.game.emit("showPerson");
        cc.game.off("freshPersonRun", this.freshPersonRun, this);
    }
    //随机获取一种行走动作
    walkAction() {
        Math.random() > 0.5 ? this.playAction(Actions.Walk0, 0) : this.playAction(Actions.Walk1, 0);
    }
    //随机获取一种奔跑动作
    runAction() {
        Math.random() > 0.5 ? this.playAction(Actions.Run0, 0) : this.playAction(Actions.Run1, 0);
    }
    swimmingAction() {
        this.isSwimming = true;
        Math.random() > 0.5 ? this.playAction(Actions.Swimming0, 0) : this.playAction(Actions.Swimming1, 0);
    }
    freshPersonRun() {
        if (this.isSwimming) return;
        if (this.action === Actions.Walk0 || this.action === Actions.Walk1) {
            this.runAction();
        }
    }
    playAction(animName: Actions, times: number, cb: Function = null) {
        // console.log(animName);
        this.action = animName;
        ToolMag.Ins.playDragonBone(this.node, animName, times, cb);
    }
    update(dt) {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        if (this.node.x < -(ToolMag.Ins.screenSize.width / 1.8)) {
            // console.log("左");
            this.recoverySelf();
            return;
        }
        this.node.x -= this.speed * dt;
        // if (this.action === Actions.Walk0 || this.action === Actions.Walk1 || this.action === Actions.Swimming0 || this.action === Actions.Swimming1) {
        //     this.node.x -= 200 * dt;
        //     return;
        // }
        // if ((this.action === Actions.Run0 || this.action === Actions.Run1) && this.node.x < 0) {
        //     this.node.x += 30 * dt;
        //     return;
        // }
    }
    die() {
        this.isDie = true;
        this.playAction(Actions.Injured, 1, () => {
            cc.game.emit("showGameCoin", this.node, 0);
            let radians = cc.misc.degreesToRadians(ToolMag.Ins.randomNum(30, 140));
            const x = this.node.x + Math.cos(radians) * 600;
            const y = this.node.y + Math.sin(radians) * 600;
            cc.tween(this.node)
                .delay(0.1)
                .to(0.7, { position: cc.v3(x, y, 0) })
                .call(() => {
                    this.recoverySelf();
                })
                .start();
            cc.game.emit("personDie");
        });
        // cc.game.emit("showGameCoin", this.node, 0);
    }
    recoverySelf() {
        PoolMag.Ins.putEnemy(this.node, null, true);
    }
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.tag == 1 || other.tag == 2 || other.tag == 101 || other.tag == 201 && !this.flying && !this.isDie) {
            if (ToolMag.Ins.randomNum(0, 10) == 1)
                cc.audioEngine.playEffect(SoundMag.Ins.getHumanSound(ToolMag.Ins.randomNum(0, SoundMag.Ins.getHumanSoundNum() - 1)), false)
            cc.game.emit("freshPersonRun");
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
            ToolMag.Ins.showBlood(this.node)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            let power = GameMag.Ins.sharkPower;
            if (other.tag == 1) {
                power = GameMag.Ins.sharkBitePower;
            }
            if (other.tag == 201) {
                power = GameMag.Ins.ActiveSkill[0].power
            }
            if (other.tag == 101) {
<<<<<<< HEAD
                power = GameMag.Ins.sharkPower;
            }
            ToolMag.Ins.showBlood(this.node, power)
=======
<<<<<<< HEAD
                power = GameMag.Ins.sharkPower;
            }
            ToolMag.Ins.showBlood(this.node, power)
=======
                power = GameMag.Ins.ActiveSkill[0].power
            }
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.PersonHp -= power
            if (this.PersonHp <= 0) {
                this.die();
            }
        }
    }
}
