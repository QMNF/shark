import GameMag from "../../../../home/script/manage/GameMag";
import PoolMag from "../../../../home/script/manage/PoolMag";
import SoundMag from "../../../../home/script/manage/SoundMag";
import ToolMag from "../../../../home/script/manage/ToolMag";
import Enemy from "../enemy";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Enemy10 extends Enemy {

    movePos: cc.Vec3[] = null;
    movePosArr: cc.Vec3[] = [];

    onEnable() {
        super.onEnable();
        if (!this.movePos) {
            this.movePos = [
                cc.v3(ToolMag.Ins.screenSize.width / 2 - 200, this.node.y, 0),
                cc.v3(ToolMag.Ins.screenSize.width / 2 - 500, this.node.y, 0)
            ];
        }
    }
    init(flag: number) {
        super.init(flag);
        if (this.flag == 14) {
            this.movePosArr = [
                cc.v3((ToolMag.Ins.screenSize.width / 2) - 110, -300, 0),
                cc.v3((ToolMag.Ins.screenSize.width / 2) - 410, -90, 0),
                cc.v3((ToolMag.Ins.screenSize.width / 2) - 110, -90, 0),
                cc.v3((ToolMag.Ins.screenSize.width / 2) - 410, -300, 0),
                cc.v3((ToolMag.Ins.screenSize.width / 2) - 250, -180, 0),
            ];
        }
        cc.tween(this.node)
            .to(0.5, { position: cc.v3(this.node.x - 200, this.node.y, 0) })
            .delay(0.2)
            .call(() => {
                if (this.flag == 14) {
                    this.schedule(this.moveTimer14, 2.5);
                } else {
                    this.schedule(this.moveTimer, 2.5);
                }
            })
            .start();
    }
    moveTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || !this.node) {
            return;
        }
        this.bulletAngle = cc.v2(0, 1).signAngle(this.shark.getPosition().subtract(this.node.getPosition()).normalize()) * 180 / Math.PI;
        this.schedule(this.fireTimer, 0.2, 1);
        cc.tween(this.node)
            .to(1.5, { position: this.movePos[ToolMag.Ins.randomNum(0, 1)] })
            .start();
    }
    moveTimer14() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || !this.node) {
            return;
        }
        this.bulletAngle = cc.v2(0, 1).signAngle(this.shark.getPosition().subtract(this.node.getPosition()).normalize()) * 180 / Math.PI;
        this.schedule(this.fireTimer, 0.2, 1);
        let index = ToolMag.Ins.randomNum(0, 4);
        let pos = this.movePosArr[index];
        cc.tween(this.node)
            .to(1.5, { position: pos })
            .start();
    }
    die() {
        super.die();
        this.unschedule(this.moveTimer);
        this.unschedule(this.moveTimer14);
        GameMag.Ins.showBoom(this.node);
        cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(SoundMag.Ins.getboomSoundNum() - 1), false)
        // console.log(this.node);
        this.scheduleOnce(() => {
            PoolMag.Ins.putEnemy(this.node, this.flag);
        }, 0.3);
        cc.game.emit("showGameCoin", this.node, this.flag);
        cc.game.emit("bossDie");
    }
    onDisable() {
        super.onDisable();
        this.unschedule(this.moveTimer);
        this.unschedule(this.moveTimer14);
    }
    update(dt){
        this.blood.node.scaleX = this.node.scaleX
    }
}