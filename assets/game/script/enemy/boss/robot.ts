import GameMag from "../../../../home/script/manage/GameMag";
import PoolMag from "../../../../home/script/manage/PoolMag";
import ToolMag from "../../../../home/script/manage/ToolMag";
import Enemy from "../enemy";

enum RobotAction {
    Fire = "fire",
    Stay = "stay",
    ToLeft = "forward",
    ToRight = "retreat",
    Hurt = "injured"
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class Robot extends Enemy {
    update(dt){
        this.blood.node.scaleX = this.node.scaleX
    }

    slotArr: string[] = ["body", "Arm_R", "hand_R", "hand_L", "thigh_R", "lower_leg_R", "foot_R", "pedal_R", "thigh_L", "lower_leg_L", "foot_L", "pedal_L"];

    onEnable() {
        super.onEnable();
        this.node.setPosition(0, 600);
        cc.tween(this.node)
            .to(0.1, { position: cc.v3(0, GameMag.Ins.airPosY, 0) })
            .delay(0.2)
            .call(() => {
                this.moveTimer();
                this.schedule(this.moveTimer, 3);
            })
            .start();
    }
    init(flag: number) {
        super.init(flag);
        const armature = this.getComponent(dragonBones.ArmatureDisplay).armature();
        for (let i = 0; i < this.slotArr.length; i++) {
            armature.getSlot(this.slotArr[i]).displayIndex = this.flag - 16;
        }
    }
    moveTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || !this.node) {
            return;
        }
        this.bulletAngle = cc.v2(0, 1).signAngle(this.shark.getPosition().subtract(this.node.getPosition()).normalize()) * 180 / Math.PI;
        this.schedule(this.fireTimer, 0.2, 2);
        let dir = -1;
        if (this.shark.x < this.node.x) {
            dir = 1;
        }
        this.node.scaleX = dir;
        ToolMag.Ins.playDragonBone(this.node, RobotAction.Fire, 1, () => {
            if (this.shark.x < this.node.x) {
                ToolMag.Ins.playDragonBone(this.node, RobotAction.ToLeft, 1);
            } else {
                ToolMag.Ins.playDragonBone(this.node, RobotAction.ToRight, 1);
            }
            let ps: cc.Vec3 = null;
            if (this.node.x + 210 > ToolMag.Ins.screenSize.width / 2) {
                ps = cc.v3(this.node.x - 200, this.node.y, 0);
            } else if (this.node.x - 210 < -ToolMag.Ins.screenSize.width / 2) {
                ps = cc.v3(this.node.x + 200, this.node.y, 0);
            } else {
                ps = cc.v3(this.node.x + (200 * -dir), this.node.y, 0);
            }
            cc.tween(this.node)
                .to(1, { position: ps })
                .call(() => {
                    ToolMag.Ins.playDragonBone(this.node, RobotAction.Stay, 0);
                })
                .start();
        });
    }
    die() {
        super.die();
        this.unschedule(this.moveTimer);
        ToolMag.Ins.playDragonBone(this.node, RobotAction.Hurt, 0);
        GameMag.Ins.showBoom(this.node);
<<<<<<< HEAD
        // console.log(this.node);
=======
<<<<<<< HEAD
        // console.log(this.node);
=======
        console.log(this.node);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.scheduleOnce(() => {
            PoolMag.Ins.putEnemy(this.node, this.flag);
        }, 0.3);
        cc.game.emit("showGameCoin", this.node, this.flag);
        cc.game.emit("bossDie");
    }
    onDisable() {
        super.onDisable();
        this.unschedule(this.moveTimer);
    }
}
