import Enemy from "../enemy";
import ToolMag from "../../../../home/script/manage/ToolMag";
import GameMag, { EnemyAction } from "../../../../home/script/manage/GameMag";
import PoolMag from "../../../../home/script/manage/PoolMag";
import SoundMag from "../../../../home/script/manage/SoundMag";


const { ccclass, property } = cc._decorator;
//飞行员
@ccclass
export default class Boss1010 extends Enemy {

    onEnable() {
        this.node.setPosition(520, 600);
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Attack2, 0);
        cc.tween(this.node)
            .to(0.1, { position: cc.v3(this.node.x, (ToolMag.Ins.screenSize.height / 2) - 230, 0) })
            .delay(0.2)
            .call(() => {
                this.moveTimer();
                this.schedule(this.moveTimer, 3);
            })
            .start();
    }
    onDisable() {
        super.onDisable();
        this.unschedule(this.moveTimer);
    }

    update(dt) {
        this.blood.node.scaleX = this.node.scaleX;
    }
    moveTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || !this.node) {
            return;
        }
        this.bulletAngle = cc.v2(0, 1).signAngle(this.shark.getPosition().subtract(this.node.getPosition()).normalize()) * 180 / Math.PI;
        // console.log(this.bulletAngle);
        // const action = this.freshAngleAction();
        this.schedule(this.fireTimer, 0.2, 1);
        let dir = 1;
        if (this.shark.x < this.node.x) {
            dir = -1;
        }
        this.node.scaleX = dir;
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Attack2, 1, () => {
            let ps: cc.Vec3 = null;
            if (this.node.x + 210 > ToolMag.Ins.screenSize.width / 2) {
                ps = cc.v3(this.node.x - 200, this.node.y, 0);
            } else if (this.node.x - 210 < -ToolMag.Ins.screenSize.width / 2) {
                ps = cc.v3(this.node.x + 200, this.node.y, 0);
            } else {
                ps = cc.v3(this.node.x + (200 * dir), this.node.y, 0);
            }
            cc.tween(this.node)
                .to(1, { position: ps })
                .call(() => {
                    ToolMag.Ins.playDragonBone(this.node, EnemyAction.Attack2, 0);
                })
                .start();
        });



        // if (this.node.x - 250 < -ToolMag.Ins.screenSize.width / 2) {
        //     this.freshAction(-1, cc.v3(this.node.x + 50, this.node.y, 0));
        //     return;
        // }
        // if (this.node.x + 250 > ToolMag.Ins.screenSize.width / 2) {
        //     this.freshAction(1, cc.v3(this.node.x - 50, this.node.y, 0))
        //     return;
        // }
        // let dir = this.bulletAngle > 0 ? -1 : 1; //判断鲨鱼在哪个方向
        // this.freshAction(dir, cc.v3(this.node.x + (200 * dir), this.node.y, 0));
    }
    freshAction(dir: number, pos: cc.Vec3) {
        this.node.scaleX = dir;
        cc.tween(this.node)
            .to(1, { position: cc.v3(this.node.x + (200 * dir), this.node.y, 0) })
            .start();
    }
    freshAngleAction(): EnemyAction {
        let angle = Math.abs(this.bulletAngle);
        let action: EnemyAction = null;
        if (angle >= 180 && angle >= 150) {
            action = EnemyAction.Attack0;
        } else if (angle > 150 && angle >= 120) {
            action = EnemyAction.Attack1;
        } else if (angle > 120 && angle >= 90) {
            action = EnemyAction.Attack2;
        } else if (angle > 90) {
            action = EnemyAction.Attack3;
        }
        return EnemyAction.Attack2;
    }
    die() {
        super.die();
        cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(SoundMag.Ins.getboomSoundNum() - 1), false)
        // console.log("死亡")
        GameMag.Ins.showBoom(this.node);
<<<<<<< HEAD
        // console.log(this.node);
=======
<<<<<<< HEAD
        // console.log(this.node);
=======
        console.log(this.node);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Die, 1);
        this.scheduleOnce(() => {
            PoolMag.Ins.putEnemy(this.node, this.flag);
        }, 0.3);
        cc.game.emit("showGameCoin", this.node, this.flag);
        cc.game.emit("bossDie");
    }
}
