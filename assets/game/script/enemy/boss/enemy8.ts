import Enemy from "../enemy";
import ToolMag from "../../../../home/script/manage/ToolMag";
import GameMag, { EnemyAction } from "../../../../home/script/manage/GameMag";
import PoolMag from "../../../../home/script/manage/PoolMag";
import SoundMag from "../../../../home/script/manage/SoundMag";


const { ccclass, property } = cc._decorator;
//潜水员
@ccclass
export default class Enemy8 extends Enemy {

    
    movePosArr: cc.Vec3[] = [];

    onEnable() {
        super.onEnable();
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Stay, 0);
        const initPosY = cc.v3((ToolMag.Ins.screenSize.width / 2) - 100, -250, 0);
        this.movePosArr = [
            cc.v3((ToolMag.Ins.screenSize.width / 2) - 100, -150, 0),
            cc.v3((ToolMag.Ins.screenSize.width / 2) - 410, -345, 0),
            cc.v3((ToolMag.Ins.screenSize.width / 2) - 100, -150, 0),
            cc.v3((ToolMag.Ins.screenSize.width / 2) - 410, -345, 0),
            cc.v3((ToolMag.Ins.screenSize.width / 2) - 250, -250, 0),
        ];
        this.node.setPosition(520, -600);
        cc.tween(this.node)
            .to(0.3, { position: initPosY })
            .delay(0.2)
            .call(() => {
                this.moveTimer();
                this.schedule(this.moveTimer, 3);
            })
            .start();
    }
    onDisable() {
        super.onDisable();
        this.unschedule(this.moveTimer);
    }
    update(dt){
        this.blood.node.scaleX = this.node.scaleX
    }
    moveTimer() {
        // console.log(GameMag.Ins.gamePause, GameMag.Ins.gameOver);
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || !this.node) {
            return;
        }
        this.bulletAngle = cc.v2(0, 1).signAngle(this.shark.getPosition().subtract(this.node.getPosition()).normalize()) * 180 / Math.PI;
        // console.log(this.bulletAngle);
        // let dir = this.node.x < pos.x ? -1 : 1; //判断方向,朝左是1
        // this.freshAction(dir, pos);
        this.schedule(this.fireTimer, 0.2, 1);
        this.scheduleOnce(() => {
            ToolMag.Ins.playDragonBone(this.node, EnemyAction.Swimming, 0);
            let index = ToolMag.Ins.randomNum(0, 4);
            let pos = this.movePosArr[index];
            cc.tween(this.node)
                .to(1, { position: pos })
                .call(() => {
                    ToolMag.Ins.playDragonBone(this.node, EnemyAction.Stay, 0);
                })
                .start();
        }, 0.5);
    }
    freshAction(dir: number, pos: cc.Vec3) {
        this.node.scaleX = dir;
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Swimming, 0);
        cc.tween(this.node)
            .to(0.7, { position: pos })
            .call(() => {
                this.schedule(this.fireTimer, 0.2, 1);
                ToolMag.Ins.playDragonBone(this.node, EnemyAction.Stay, 0);
            })
            .start();
    }
    die() {
        super.die();
        cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(SoundMag.Ins.getboomSoundNum() - 1), false)
        GameMag.Ins.showBoom(this.node);
        console.log(this.node);
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Die, 1);
        this.scheduleOnce(() => {
            PoolMag.Ins.putEnemy(this.node, this.flag);
        }, 0.3);
        cc.game.emit("showGameCoin", this.node, this.flag);
        cc.game.emit("bossDie");
    }
}
