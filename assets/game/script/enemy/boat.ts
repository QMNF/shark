import ToolMag from "../../../home/script/manage/ToolMag";
import PoolMag from "../../../home/script/manage/PoolMag";
import Person from "./person";
import GameMag from "../../../home/script/manage/GameMag";
import Enemy from "./enemy";
import SoundMag from "../../../home/script/manage/SoundMag";

enum Actions {
    Action0 = "byBoat_0", //4  6  7
    Action1 = "byBoat_1", //5  4  2
    Action2 = "byBoat_2"  //3  0  1
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class Boat extends Enemy {

    skinArr: any[] = [
        [4, 6, 7],
        [5, 4, 2],
        [3, 0, 1]
    ]
    action: number = 0;
    touchBoat: boolean = false;
    isBoom: boolean = false;

    onEnable() {
        super.onEnable();
        this.touchBoat = false;
        this.isBoom = false;
        this.blood.progress = 1;
        const actions: Actions[] = [Actions.Action0, Actions.Action1, Actions.Action2];
        this.action = ToolMag.Ins.randomNum(0, 2);
        this.playAction(actions[this.action], 0);
    }
    playAction(animName: Actions, times: number) {
        ToolMag.Ins.playDragonBone(this.node, animName, times);
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        super.onCollisionEnter(other, self);
        // if (other.tag == 11) {
        //     this.touchBoat = true;
        // }
        if ((other.tag == 1 || other.tag == 2 || other.tag == 201 || other.tag == 101) && this.blood.progress <= 0 && !this.isBoom) {
            this.isBoom = true;
            GameMag.Ins.showBoom(this.node);
            // console.log(this.node);
            this.boatDie();
        }
    }
    onCollisionExit(other: cc.BoxCollider, self: cc.BoxCollider) {
        if (other.tag == 11) {
            this.touchBoat = false;
        }
    }
    boatDie() {
        super.boatDie();
        cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(ToolMag.Ins.randomNum(0, SoundMag.Ins.getboomSoundNum() - 2)), false)
        this.skinArr[this.action].forEach((id, i) => {
            let node = PoolMag.Ins.getEnemy(true);
            node.parent = this.personParent;
            node.getComponent(Person).setSkin(id);
            node.position = this.node.position;
            const flyToWater = Math.random() > 0.5 ? true : false;
            node.getComponent(Person).isFlyByBoat(true);
            cc.tween(node)
                .then(cc.jumpTo(0.6,
                    cc.v2(
                        this.node.x + (this.node.x * (Math.random() > 0.5 ? 1 : -1) + ToolMag.Ins.randomNum(-150, 150)),
                        flyToWater ? GameMag.Ins.waterPosY : GameMag.Ins.groundPosY
                    ),
                    170,
                    1))
                .call(() => {
                    if (flyToWater) node.getComponent(Person).swimmingAction();
                })
                .delay(0.15)
                .call(() => {
                    // console.error("船爆炸")
                    node.getComponent(Person).isFlyByBoat(false);
                })
                .start();
            if (i >= 2) PoolMag.Ins.putEnemy(this.node, this.flag);
        });
        cc.game.emit("showGameCoin", this.node, this.flag);
        cc.game.emit("eliteDie");
    }
    bloodTimer() {
        this.blood.node.active = false;
    }

    update(dt) {
        if (this.node.x < -(ToolMag.Ins.screenSize.width / 2) + 220
            || this.touchBoat
            || this.isAttacked
            || GameMag.Ins.gamePause
            || GameMag.Ins.gameOver) return;
        this.node.x -= 200 * dt;
    }
}