import ToolMag from "../../../home/script/manage/ToolMag";
import GameMag, { EnemyAction } from "../../../home/script/manage/GameMag";
import Enemy, { EnemyPos } from "./enemy";
import PoolMag from "../../../home/script/manage/PoolMag";
import Person from "./person";
import SoundMag from "../../../home/script/manage/SoundMag";

const { ccclass, property } = cc._decorator;
//精英
@ccclass
export default class Elite extends Enemy {

    sche: number = 0;
    randomTime: number = 5;
    backFlag: boolean = null;
    isbite: boolean = null;

    onEnable() {
        super.onEnable();
        this.backFlag = true;
        if (this.enemyPos === EnemyPos.InAir) {
            cc.game.on("bitingPlane", this.bitingPlane, this);
            cc.game.on("releaseByBite", this.releaseByBite, this);
        }
    }
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        super.onCollisionEnter(other, self);
        // if (this.isDie) return;
        // if (other.tag == 2 && this.blood.progress <= 0) {
        //     this.die();
        // }
    }
    bloodTimer() {
        this.blood.node.active = false;
    }
    die() {
        // super.die();
        if (this.isbite) {
            this.isbite = false
            cc.game.emit("bitedEnd");//被鲨鱼咬死了
        }
        cc.game.emit("showGameCoin", this.node, this.flag);
        GameMag.Ins.showBoom(this.node);
        cc.game.emit("eliteDie");
        if (this.Collider.tag == 13) {
            cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(SoundMag.Ins.getboomSoundNum() - 1), false)
        } else {
            cc.audioEngine.playEffect(SoundMag.Ins.getboomSound(ToolMag.Ins.randomNum(0, SoundMag.Ins.getboomSoundNum() - 2)), false)
        }
        // console.log(this.node);
        PoolMag.Ins.putEnemy(this.node, this.flag);
        // this.showPerson();
    }
    showPerson() {
        let node = PoolMag.Ins.getEnemy(true);
        node.parent = this.personParent;
        node.getComponent(Person).setSkin(ToolMag.Ins.randomNum(0, 8));
        node.position = this.node.position;
        const flyToWater = Math.random() > 0.5 ? true : false;
        node.getComponent(Person).isFlyByBoat(true);
        cc.tween(node)
            .then(cc.jumpTo(0.4,
                cc.v2(
                    this.node.x + (this.node.x + ToolMag.Ins.randomNum(0, 90)),
                    flyToWater ? GameMag.Ins.waterPosY : GameMag.Ins.groundPosY
                ),
                150,
                1))
            .call(() => {
                if (flyToWater) node.getComponent(Person).swimmingAction();
            })
            .delay(0.1)
            .call(() => {
                node.getComponent(Person).isFlyByBoat(false);
                // console.log("精英怪回收")
                PoolMag.Ins.putEnemy(this.node, this.flag);
            })
            .start();
    }
    isdie(): boolean {
        return this.blood.progress <= 0
    }
    update(dt) {
        if (this.isDie || GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        if (this.bitedNode && this.node == this.bitedNode) return;//被鲨鱼咬住的时候
        this.sche += dt;
        if (this.sche > this.randomTime) {
            this.backFlag = false;
            this.sche = 0;
            this.randomTime = ToolMag.Ins.randomNum(5, 8);
            cc.tween(this.node)
                .to(4, { position: cc.v3(this.node.x + 400 > ToolMag.Ins.screenSize.width / 2 ? ToolMag.Ins.screenSize.width / 2 - this.node.width / 2 : this.node.x + 400, this.node.y, 0) })
                .call(() => {
                    this.backFlag = true;
                })
                .start();
            return;
        }
        if (this.node.x < -(ToolMag.Ins.screenSize.width / 2) + 250) return;
        if (!this.backFlag) return;
        this.node.x -= this.speed * dt;
    }
    bitedNode: cc.Node = null;
    /**被鲨鱼咬住了 */
    bited(node: cc.Node) {
        if (this.isDie) return;
        cc.game.emit("showTaps");
        this.isbite = true;
        this.bitedNode = node;
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Hurt, 0);
    }
    /**被鲨鱼咬住了并下坠 */
    bitingPlane(num) {
        if (this.node != this.bitedNode || this.isDie) return;
        this.node.y -= num;
        // console.log(this.node.y);
        if (this.node.y <= 80 && !this.isDie) { //20下
            this.bitedNode = null;
            this.isDie = true;
            this.die();
            cc.game.emit("bitedEnd");//被鲨鱼咬死了
        }
    }
    /**被鲨鱼咬后没有坠毁就释放了 */
    releaseByBite() {
        if (this.node != this.bitedNode) return;
        this.bitedNode = null;
        this.node.y = GameMag.Ins.airPosY;
        ToolMag.Ins.playDragonBone(this.node, EnemyAction.Stay, 0);
    }
}
