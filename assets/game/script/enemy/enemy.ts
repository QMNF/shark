import ToolMag from "../../../home/script/manage/ToolMag";
import GameMag, { EnemyAction } from "../../../home/script/manage/GameMag";
import StorageMag, { StorageKey } from "../../../home/script/manage/StorageMag";
import PoolMag from "../../../home/script/manage/PoolMag";
import BossBullet from "../bossBullet";
import SoundMag from "../../../home/script/manage/SoundMag";
import dom from "../dom";

export enum EnemyPos {
    Null, //潜水类
    InAir,
    InLand,
    InWater
}

const { ccclass, property } = cc._decorator;
// 精英、BOSS血量和攻击 每过5关翻倍
@ccclass
export default class Enemy extends cc.Component {

    @property()
    speed: number = 1000;
    @property(cc.ProgressBar)
    blood: cc.ProgressBar = null;
    @property({ type: cc.Node, tooltip: "14和15" })
    img: cc.Node = null;
    @property({ type: cc.Node, tooltip: "子弹的初始位置" })
    firePos: cc.Node = null;
    @property({ type: cc.Enum(EnemyPos), tooltip: "null标识潜水员和潜水艇" })
    enemyPos: EnemyPos = EnemyPos.InLand;
    @property(cc.Animation)
    aim: cc.Animation = null;

    shark: cc.Node = null;
    bulletBox: cc.Node = null;
    Collider: cc.Collider = null;
    isDie: boolean = null;
    isAttacked: boolean = null;
    initHp: number = null;
    nowHp: number = null;
    initPower: number = null;
    bulletAngle: number = 0;
    enemyData: any = null;
    flag: number = null;//敌人的标识
    personParent: cc.Node = null;

    onLoad() {
        this.shark = cc.find("Canvas/gameMain/shark");
        this.personParent = cc.find("Canvas/gameMain/Terrain/personBox");
        this.bulletBox = cc.find("Canvas/gameMain/Terrain/bulletBox");
        this.Collider = this.node.getComponent(cc.Collider);
    }
    onEnable() {
        this.isDie = false;
        this.isAttacked = false;
        this.node.opacity = 255;
        if (this.blood) this.blood.progress = 1;
        this.node.x = ToolMag.Ins.screenSize.width / 2;
        switch (this.enemyPos) {
            case EnemyPos.InWater:
                this.node.zIndex = 2000;
                this.node.y = GameMag.Ins.waterPosY;
                break;
            case EnemyPos.InAir:
                this.node.zIndex = 0;
                this.node.y = GameMag.Ins.airPosY;
                break;
            case EnemyPos.InLand:
                this.node.zIndex = 1000;
                this.node.y = GameMag.Ins.groundPosY;
                break;
            default:
                break;
        }
        cc.game.on("allDie", this.die, this);
        cc.game.on("allDie", this.boatDie, this);
    }
    isBoss(): boolean {
        if (this.flag && this.flag > 7) {
            return true;
        }
        return false;
    }
    playAimAnim() {
        if (!this.aim) return;
        this.aim.node.active = true;
        this.aim.play("aimAnim")
    }
    init(flag: number) {
        this.flag = flag;
<<<<<<< HEAD
        // console.error("flag", this.flag, this.node)
        if (this.aim)
            this.aim.node.active = false;
        this.enemyData = GameMag.Ins.enemyJson[flag];
        // console.log(this.enemyData, flag);
        this.initHp = this.enemyData.hp * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5));
        this.nowHp = this.initHp;
        // console.error(this.nowHp, "bossHp");
=======
<<<<<<< HEAD
        // console.error("flag", this.flag, this.node)
        if (this.aim)
            this.aim.node.active = false;
        this.enemyData = GameMag.Ins.enemyJson[flag];
        // console.log(this.enemyData, flag);
        this.initHp = this.enemyData.hp * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5));
        this.nowHp = this.initHp;
        // console.error(this.nowHp, "bossHp");
=======
        console.error("flag", this.flag, this.node)
        if (this.aim)
            this.aim.node.active = false;
        this.enemyData = GameMag.Ins.enemyJson[flag];
        console.log(this.enemyData, flag);
        this.initHp = this.enemyData.hp * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5));
        this.nowHp = this.initHp;
        console.error(this.nowHp, "bossHp");
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.initPower = this.enemyData.power * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5));
        this.schedule(this.playAimAnim, 8)
        if (!this.isBoss()) {
            if (this.flag == 4) {

            } else if (this.flag == 7) {
                ToolMag.Ins.playDragonBone(this.node, EnemyAction.Stay, 0);
            } else {
                ToolMag.Ins.playDragonBone(this.node, "newAnimation", 0);
            }
            return;
        }
        this.node.zIndex = 10000;
        if (this.flag == 10) {
            ToolMag.Ins.playDragonBone(this.node, EnemyAction.Attack2, 0);
        } else if (this.flag == 11 || this.flag == 12) {
            ToolMag.Ins.playDragonBone(this.node, EnemyAction.Fire, 0);
        } else if (this.flag == 14 || this.flag == 15) {
            cc.tween(this.img)
                .then(cc.sequence(
                    cc.moveTo(0.5, this.img.x, this.img.y + 3),
                    cc.moveTo(0.5, this.img.x, this.img.y - 3)
                ))
                .repeatForever()
                .start();
        }
    }
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (this.isDie || GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        if (other.tag == 1 || other.tag == 2 || other.tag == 101 || other.tag == 201) {
            if (this.isBoss()) {
                this.hurtedShake();
                let power = GameMag.Ins.sharkPower;
                if (other.tag == 1) {
                    power = GameMag.Ins.sharkBitePower;
                }
                if (other.tag == 201) {
                    power = GameMag.Ins.ActiveSkill[0].power
                    other.node.getComponent(dom).showBoom();
                }
                if (other.tag == 101) {
                    power = GameMag.Ins.sharkPower;
                }
                if (power > this.nowHp) {
                    power = this.nowHp
                }
<<<<<<< HEAD
                ToolMag.Ins.showBlood(this.node, power)
=======
<<<<<<< HEAD
                ToolMag.Ins.showBlood(this.node, power)
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                this.nowHp -= power
                this.blood.progress = this.nowHp / this.initHp;
                if (this.blood.progress <= 0) {
                    this.isDie = true;
                    this.node.opacity = 0;
                    this.blood.node.active = false;
                    this.die();
                }
                this.blood.node.active = true;
                this.unschedule(this.bloodTimer);
                this.scheduleOnce(this.bloodTimer, 2);
                cc.game.emit("freshBossBlood", false, power, (over: boolean) => {
                    if (!over) return;
                });
                return;
            };
            // console.log(GameMag.Ins.sharkPower, this.initHp);
            let power = GameMag.Ins.sharkPower;
            if (other.tag == 1) {
                power = GameMag.Ins.sharkBitePower;
            }
            if (other.tag == 201) {
                power = GameMag.Ins.ActiveSkill[0].power
                other.node.getComponent(dom).showBoom();
            }
            if (other.tag == 101) {
<<<<<<< HEAD
                power = GameMag.Ins.sharkPower;
            }
            ToolMag.Ins.showBlood(this.node, power)
=======
<<<<<<< HEAD
                power = GameMag.Ins.sharkPower;
            }
            ToolMag.Ins.showBlood(this.node, power)
=======
                power = GameMag.Ins.ActiveSkill[0].power
            }
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.nowHp -= power
            this.blood.progress = this.nowHp / this.initHp;
            if (this.blood.progress <= 0) {
                this.isDie = true;
                this.node.opacity = 0;
                this.blood.node.active = false;
                this.die();
                return;
            }
            this.hurtedShake();
            this.blood.node.active = true;
            this.unschedule(this.bloodTimer);
            this.scheduleOnce(this.bloodTimer, 2);
        }
    }
    die() {
        // cc.game.emit("showGameCoin", this.node, this.flag);
    }
    boatDie() {
        // cc.game.emit("showGameCoin", this.node, this.flag);
    }
    /**被击中的震动效果 */
    hurtedShake() {
        this.isAttacked = true;
        this.node.stopAllActions();
        cc.tween(this.node)
            .then(
                cc.sequence(
                    cc.moveTo(0.02, this.node.x + 5, this.node.y),
                    cc.moveTo(0.02, this.node.x - 5, this.node.y)
                )
            )
            .repeat(2)
            .call(() => {
                this.isAttacked = false;
            })
            .start();
    }
    bloodTimer() {
        this.blood.node.active = false;
    }
    fireTimer() {
        if (this.firePos.childrenCount > 0) {
            this.firePos.children.forEach(element => {
                this.showBullet(element);
            });
            return;
        }
        this.showBullet(this.firePos);
    }
    showBullet(target: cc.Node) {
        let node = PoolMag.Ins.getBossBullet();
        node.parent = this.bulletBox;
        const wps = target.parent.convertToWorldSpaceAR(target.getPosition());
        const lps = node.parent.convertToNodeSpaceAR(wps);
        node.setPosition(lps);
        node.getComponent(BossBullet).init(this.bulletAngle + 90, this.enemyData.power);
        cc.audioEngine.playEffect(SoundMag.Ins.getgunSound(), false);

    }
    onDisable() {
        cc.game.off("allDie", this.die, this);
        cc.game.off("allDie", this.boatDie, this);
        this.node.stopAllActions();
        this.unschedule(this.fireTimer);
        this.unschedule(this.bloodTimer);
    }
}
