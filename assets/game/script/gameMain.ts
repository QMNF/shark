import ToolMag from "../../home/script/manage/ToolMag";
import GameMag, { Enemys } from "../../home/script/manage/GameMag";
import PoolMag from "../../home/script/manage/PoolMag";
import Bullet from "./bullet";
import Person from "./enemy/person";
import StorageMag, { StorageKey } from "../../home/script/manage/StorageMag";
import Enemy from "./enemy/enemy";
import ResultDialog from "./resultDialog";
import levelTip from "../../home/script/levelTip";
import SoundMag from "../../home/script/manage/SoundMag";
import dom from "./dom";
import Shark from "./shark";

enum sharkAction {
    Stay = "stay",
    Attack = "bite",
    Die = "die"
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameMain extends cc.Component {

    @property(cc.SpriteAtlas)
    atlas: cc.SpriteAtlas = null;
    @property(cc.Node)
    homeMain: cc.Node = null;
    @property({ type: cc.Node, tooltip: "可点击移动区域" })
    moveArea: cc.Node = null;
    @property({ type: cc.Node, tooltip: "可点击攻击区域" })
    attackArea: cc.Node = null;
    @property(cc.Node)
    sharkBone: cc.Node = null;
    @property(cc.Node)
    sharkGun: cc.Node = null;
    @property(cc.Node)
    sharkKnife: cc.Node = null;
    @property(cc.Node)
    bulletStartPos: cc.Node = null;
    @property(cc.Node)
    waterFlower: cc.Node = null;
    @property(cc.Node)
    pauseBtn: cc.Node = null;
    @property(cc.ProgressBar)
    bossBlood: cc.ProgressBar = null;
    @property({ type: cc.ProgressBar, tooltip: "关卡目标数量" })
    personBar: cc.ProgressBar = null;
    @property({ type: cc.Node, tooltip: "点击屏幕拖拽飞机的两个按钮" })
    taps: cc.Node = null;
    @property({ type: cc.Label, tooltip: "" })
    level: cc.Label = null;
    @property({ type: cc.Node, tooltip: "" })
    levelWarn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "关卡显示" })
    levelTip: cc.Node = null;
    @property({ type: cc.Node, tooltip: "title数据显示" })
    currency: cc.Node = null;



    @property({ type: cc.Node, tooltip: "子弹父节点" })
    bulletBox: cc.Node = null;
    @property({ type: cc.Node, tooltip: "小人父节点" })
    personBox: cc.Node = null;
    @property({ type: cc.Node, tooltip: "精英的父节点" })
    eliteBox: cc.Node = null;
    @property({ type: cc.Node, tooltip: "敌人掉落的金币的父节点" })
    coinBox: cc.Node = null;

    @property(cc.Node)
    moveBigBtn: cc.Node = null;
    @property(cc.Node)
    moveSmallBtn: cc.Node = null;
    @property(cc.Node)
    attackBigBtn: cc.Node = null;
    @property(cc.Node)
    attackSmallBtn: cc.Node = null;
    @property({ type: [cc.Node], tooltip: "技能面板" })
    SkillList = new Array<cc.Node>();
    @property({ type: cc.Node, tooltip: "斧头攻击" })
    KnifeBtn: cc.Node = null;
    @property({ type: cc.Sprite, tooltip: "斧头爆发能量槽" })
    KnifePowerMeter: cc.Sprite = null;
    @property(cc.Node)
    flash: cc.Node = null;
    @property(cc.Node)
    fireRing: cc.Node = null;

    @property({ type: cc.Node, tooltip: "结算界面" })
    resultDialog: cc.Node = null;
    @property({ type: cc.Node, tooltip: "暂停界面" })
    pauseDialog: cc.Node = null;
    @property({ type: cc.Node, tooltip: "返回" })
    returnBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "重玩" })
    restartBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "继续" })
    resumeBtn: cc.Node = null;

    @property({ type: cc.Node, tooltip: "新手引导" })
    firstGameNode: cc.Node = null;

    screenSize: cc.Size = null;
    circleDefaultPos: cc.Vec2 = cc.v2(-350, -200);
    sharkDefaultPos: cc.Vec2 = cc.v2(-180, -130);
    speed: number = 800;
    jumpLine: number = 30;
    movedt: number = 0;
    moveDistance: number = 0;
    bossBloodNum: number = 0;
    AllbossBloodNum: number = 0;
    KillPersonNum: number = 0;
    KillEliteNum: number = 0;
    bossNum = 0;
    isAutoAim: boolean = false;
    randomAim: number = -1;
    jumpSoundID: number = 0;
    data: any = null;

    ActiveSkill: any[] = [];
    PassiveSkills: any[] = [];

    tapTime: number = 0;
    isTap: boolean = false;
    isKnifeAtt: boolean = false;
    isKnifeBut: boolean = false;
<<<<<<< HEAD
    isMove: boolean = false;

    PromptIndex = 0;
=======
<<<<<<< HEAD
    isMove: boolean = false;

    PromptIndex = 0;
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97


    onLoad() {
        this.screenSize = ToolMag.Ins.screenSize;
        this.moveArea.width = (this.screenSize.width / 2) - 70;
        // this.attackArea.width = this.moveArea.width;
        this.SkillList.forEach((item, index) => {
            // item.getChildByName("cd").active = false;
            item.on(cc.Node.EventType.TOUCH_END, this.clickSkill, this);
        })
        // this.KnifeBtn.on(cc.Node.EventType.TOUCH_END, this.KnifeAttack, this)

        this.moveArea.on(cc.Node.EventType.TOUCH_START, this._touchMoveAreaStrat, this);
        this.moveArea.on(cc.Node.EventType.TOUCH_MOVE, this._touchMoveAreaMove, this);
        this.moveArea.on(cc.Node.EventType.TOUCH_END, this._touchMoveAreaEnd, this);
        this.moveArea.on(cc.Node.EventType.TOUCH_CANCEL, this._touchMoveAreaEnd, this);

        this.attackArea.on(cc.Node.EventType.TOUCH_START, this._touchAttackAreaStrat, this);
        this.attackArea.on(cc.Node.EventType.TOUCH_MOVE, this._touchAttackAreaMove, this);
        this.attackArea.on(cc.Node.EventType.TOUCH_END, this._touchAttackAreaEnd, this);
        this.attackArea.on(cc.Node.EventType.TOUCH_CANCEL, this._touchAttackAreaEnd, this);

        this.pauseBtn.on(cc.Node.EventType.TOUCH_START, this.gamePause, this);
<<<<<<< HEAD
        this.returnBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("onReturn", false)
        }, this);
=======
<<<<<<< HEAD
        this.returnBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("onReturn", false)
        }, this);
=======
        this.returnBtn.on(cc.Node.EventType.TOUCH_END, this.onReturn, this);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        // this.restartBtn.on(cc.Node.EventType.TOUCH_END, this.onRestart, this);
        this.resumeBtn.on(cc.Node.EventType.TOUCH_END, this.onResume, this);
        this.firstGameNode.getChildByName("panel").on(cc.Node.EventType.TOUCH_END, this.showPrompt, this)
        cc.game.on("freshBossBlood", this.freshBossBlood, this);
        cc.game.on("KnifeBurst", this.KnifeBurst, this);
        cc.game.on("relive", this.relive, this)
        cc.game.on("gameOver", this.gameOver, this);
        cc.game.on("nextLevel", this.nextLevel, this);
        cc.game.on("onReturn", this.onReturn, this);
        cc.game.on("onRestart", this.onRestart, this);
        cc.game.on("showTaps", this.showTaps, this);
        cc.game.on("bitedEnd", this.bitedEnd, this);
        cc.game.on("showGameCoin", this.showGameCoin, this);
        cc.game.on("addKnifePower", () => {
            if (this.isKnifeBut) return;
            if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
            this.KnifePowerMeter.fillRange += 0.01
<<<<<<< HEAD
            if (this.KnifePowerMeter.fillRange >= 1) {
                cc.game.emit("KnifeBurst");
            }
=======
<<<<<<< HEAD
            if (this.KnifePowerMeter.fillRange >= 1) {
                cc.game.emit("KnifeBurst");
            }
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        })
        cc.game.on("showPerson", () => {
            if (Math.random() > 0.3) return;
            Math.random() > 0.5 ? this.showWalkPersonTimer() : this.showSwimPersonTimer();
        }, this);

        cc.game.on("eliteDie", () => {
            if (this.KillEliteNum > 0)
                this.KillEliteNum -= 1;
<<<<<<< HEAD
            // console.error("精英怪死亡")
=======
<<<<<<< HEAD
            // console.error("精英怪死亡")
=======
            console.error("精英怪死亡")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.killEnemy();
        })
        cc.game.on("bossDie", () => {
            if (this.KillEliteNum > 0)
                this.KillEliteNum -= 1;
<<<<<<< HEAD
            // console.error("boss死亡")
=======
<<<<<<< HEAD
            // console.error("boss死亡")
=======
            console.error("boss死亡")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.killEnemy();
        })

        cc.game.on("personDie", () => {
            if (this.KillPersonNum > 0)
                this.KillPersonNum -= 1;
            this.killEnemy();
        })
    }

    killEnemy() {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if (this.personBar.progress <= 0) return;
        this.personBar.progress = (this.KillPersonNum + this.KillEliteNum) / (this.data.personNum + this.data.eliteNum)
        this.personBar.node.getChildByName("num").getComponent(cc.Label).string = (this.KillPersonNum + this.KillEliteNum) + ""
        // console.log("KillPersonNum", this.KillPersonNum)
<<<<<<< HEAD
=======
=======
        this.personBar.progress = (this.KillPersonNum + this.KillEliteNum) / (this.data.personNum + this.data.eliteNum)
        this.personBar.node.getChildByName("num").getComponent(cc.Label).string = (this.KillPersonNum + this.KillEliteNum) + ""
        console.log("KillPersonNum", this.KillPersonNum)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if ((this.KillPersonNum + this.KillEliteNum) == this.data.bossArr.length) {
            if (this.data.bossArr.length && this.bossNum > 0) {
                this.data.bossArr.forEach(element => {
                    this.AllbossBloodNum += (GameMag.Ins.enemyJson[Number(element) - 1001].hp * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5)));
                    this.bossBloodNum += (GameMag.Ins.enemyJson[Number(element) - 1001].hp * Math.pow(2, Math.floor(StorageMag.Ins.getStorage(StorageKey.Level) / 5)));
                    this.bossNum--;
                });
<<<<<<< HEAD
                // console.log("AllbossBloodNum", this.AllbossBloodNum);
=======
<<<<<<< HEAD
                // console.log("AllbossBloodNum", this.AllbossBloodNum);
=======
                console.log("AllbossBloodNum", this.AllbossBloodNum);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                this.showWarn();
                this.showBoss(this.data);
            }
        }
        if (this.personBar.progress <= 0) {
            if (!GameMag.Ins.gameOver) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                cc.kSpeed(0.2)
                this.scheduleOnce(() => {
                    cc.kSpeed(1);
                    GameMag.Ins.gameOver = true;
                    // console.log("游戏胜利");
                    cc.game.emit("gameOver", 1);
                }, 0.5)
<<<<<<< HEAD
=======
=======
                console.log("游戏胜利");
                GameMag.Ins.gameOver = true;
                cc.game.emit("gameOver", 1);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            }
        }
    }
    clickSkill(event) {
<<<<<<< HEAD
        // console.log(event.target.name)
=======
<<<<<<< HEAD
        // console.log(event.target.name)
=======
        console.log(event.target.name)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if (!this.SkillCD(event.target, 5)) return
        if (event.target.name == "手雷") {
            this.fireDom();
        } else if (event.target.name == "治疗") {
            this.addHp();
        }
        // console.log(GameMag.Ins.killNum, GameMag.Ins.needKillNum)
    }
    KnifeAttack() {
        if (!this.isKnifeAtt && !this.isKnifeBut) {
<<<<<<< HEAD
            // this.sharkKnife.stopAllActions();
=======
<<<<<<< HEAD
            // this.sharkKnife.stopAllActions();
=======
            this.sharkKnife.stopAllActions();
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.isKnifeAtt = true
            this.sharkKnife.active = true;
            cc.tween(this.sharkKnife)
                .to(0.3, { angle: -360 })
                .call(() => {
                    this.sharkKnife.angle = 0
                    // this.KnifePowerMeter.fillRange += 0.05
<<<<<<< HEAD
                    // console.error(this.KnifePowerMeter.fillRange)
                    this.isKnifeAtt = false
                    this.sharkKnife.active = false;
=======
<<<<<<< HEAD
                    // console.error(this.KnifePowerMeter.fillRange)
                    this.isKnifeAtt = false
                    this.sharkKnife.active = false;
=======
                    console.error(this.KnifePowerMeter.fillRange)
                    this.isKnifeAtt = false
                    this.sharkKnife.active = false;
                    if (this.KnifePowerMeter.fillRange >= 1) {
                        cc.game.emit("KnifeBurst");
                    }
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                })
                .start();
        }
    }
    KnifeBurst() {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.sharkKnife.stopAllActions();
        // console.error(this.sharkKnife)
        this.sharkKnife.active = true;
        this.flash.active = true;
        this.isKnifeAtt = true
<<<<<<< HEAD
=======
=======
        console.error(this.sharkKnife)
        this.sharkKnife.active = true;
        this.flash.active = true;
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.isKnifeBut = true;
        this.flash.getComponent(cc.Animation).play();
        this.fireRing.active = true
        this.fireRing.getComponent(cc.Animation).play();
        cc.tween(this.sharkKnife)
            .then(cc.sequence(cc.rotateBy(0.2, 360), cc.callFunc(() => {
                if (!GameMag.Ins.gamePause && !GameMag.Ins.gameOver)
                    this.KnifePowerMeter.fillRange -= 0.05
<<<<<<< HEAD
                // console.error(this.KnifePowerMeter.fillRange)
=======
<<<<<<< HEAD
                // console.error(this.KnifePowerMeter.fillRange)
=======
                console.error(this.KnifePowerMeter.fillRange)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                if (this.KnifePowerMeter.fillRange <= 0) {
                    this.sharkKnife.active = false;
                    this.sharkKnife.angle = 0
                    this.sharkKnife.stopAllActions();
                    this.flash.active = false;
                    this.flash.getComponent(cc.Animation).stop();
                    this.fireRing.active = false;
                    this.fireRing.getComponent(cc.Animation).stop();
                    this.KnifePowerMeter.fillRange = 0
<<<<<<< HEAD
                    this.isKnifeAtt = false
=======
<<<<<<< HEAD
                    this.isKnifeAtt = false
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    this.isKnifeBut = false;
                }
            })))
            .repeatForever()
            .start();

    }
    resultDialogNode: cc.Node = null;
    gameOver(flag: number) {
        // if (StorageMag.Ins.getStorage(StorageKey.Level) % 5 != 0) {
        //     this.nextLevel();
        //     return;
        // }
        this.resultDialog.active = true;
        this.bossBlood.node.active = false;
        if (this.resultDialogNode) {
            this.resultDialogNode.active = true;
            this.resultDialogNode.getComponent(ResultDialog).isSuccess(flag, this.currency, this.data);
            return;
        }
        cc.resources.load("resultDialog", cc.Prefab, (err, res: cc.Prefab) => {
            if (err) {
                console.error("resultDialog=>err", err);
            }
            this.resultDialogNode = cc.instantiate(res);
            this.resultDialogNode.parent = this.resultDialog;
            this.resultDialogNode.getComponent(ResultDialog).isSuccess(flag, this.currency, this.data);
        });
    }
    relive() {
        console.log("inrelive");
        if (this.bossBloodNum > 0)
            this.bossBlood.node.active = true;
        ToolMag.Ins.playDragonBone(this.sharkBone, "stay", 0);
        this.sharkBone.getComponent(Shark).isDie = false;
<<<<<<< HEAD
        GameMag.Ins.isReLive = false
=======
<<<<<<< HEAD
        GameMag.Ins.isReLive = false
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        GameMag.Ins.gamePause = false
        GameMag.Ins.gameHp = GameMag.Ins.sharkHp;
        this.sharkBone.getComponent(Shark).addhp(GameMag.Ins.gameHp);
        this.onResume();
        this._touchMoveAreaEnd();
        this._touchAttackAreaEnd();
    }
    nextLevel() {
        this.resultDialogNode.active = false;
        this.bossBlood.progress = 1;
        this.personBar.progress = 1;
        this.bossBloodNum = 0;
        this.AllbossBloodNum = 0;
        this.tapTime = 0;
        this.eliteBox.removeAllChildren();
        GameMag.Ins.killNum = 0;
        GameMag.Ins.needKillNum = 0;
        StorageMag.Ins.freshLevel();
        StorageMag.Ins.freshMaxLevel(StorageMag.Ins.getStorage(StorageKey.Level))
        if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 11) {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10020")
            }
        }
        if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 21) {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10021")
            }
        }
        if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 31) {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10022")
            }
        }
        GameMag.Ins.gameOver = false;
        this.setdata(GameMag.Ins.levelJson[StorageMag.Ins.getStorage(StorageKey.Level) - 1])
        if (cc.sys.platform === cc.sys.WECHAT_GAME) {
            // @ts-ignore
            wx.uma.trackEvent("10004", { level: StorageMag.Ins.getStorage(StorageKey.Level) })
        }
        this._touchMoveAreaEnd();
        this._touchAttackAreaEnd();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.schedule(this.showEnemysTimer, 0.02)
        this.showLevel();
    }
    LevelOrder() {
        // console.error(this.data);
<<<<<<< HEAD
=======
=======
        this.showEnemys();
        this.showLevel();
    }
    LevelOrder() {
        console.error(this.data);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.KillPersonNum = this.data.personNum;
        this.KillEliteNum = this.data.eliteNum + this.data.bossArr.length;
        this.bossNum = this.data.bossArr.length;

    }
    showLevel() {

        this.LevelOrder();
        this.personBar.node.active = true
        const level = StorageMag.Ins.getStorage(StorageKey.Level);
        this.level.string = "第 " + level + " 关";
        this.personBar.node.getChildByName("num").getComponent(cc.Label).string = (this.KillPersonNum + this.KillEliteNum) + ""
        // this.levelTip.getComponent(levelTip).show(level);
        this.level.node.stopAllActions();
    }
    showWarn() {
        this.levelWarn.opacity = 0;
        this.levelWarn.active = true;
        this.levelWarn.stopAllActions();
        cc.tween(this.levelWarn)
            .to(0.5, { opacity: 255 })
            .delay(0.2)
            .to(0.5, { opacity: 0 })
            .delay(0.2)
            .to(0.5, { opacity: 255 })
            .delay(0.2)
            .to(0.5, { opacity: 0 })
            .call(() => {
                this.levelWarn.opacity = 0;
                this.levelWarn.active = false;
            }).start()
    }

    SkillInfo(ActiveSkill: any[], PassiveSkills: any[]) {
        this.ActiveSkill = ActiveSkill;
        this.PassiveSkills = PassiveSkills;
    }

    SkillCD(target: cc.Node, CD: number) {
        if (target.getChildByName("cd").getComponent(cc.Sprite).fillRange < 1) return false
        target.getChildByName("cd").getComponent(cc.Sprite).fillRange = 0;
        cc.tween(target.getChildByName("cd"))
            .then(cc.sequence(cc.callFunc(() => {
                if (target.getChildByName("cd").getComponent(cc.Sprite).fillRange < 1) {
                    target.getChildByName("cd").getComponent(cc.Sprite).fillRange += GameMag.Ins.gameOver || GameMag.Ins.gamePause ? 0 : 0.0001
                } else {
                    // target.getChildByName("cd").getComponent(cc.Sprite).fillRange = 0
                    // target.getChildByName("cd").active = false;
                    target.getChildByName("cd").stopAllActions();
                }
            }), cc.delayTime(1 / (10000 / CD)))).repeatForever().start();
        return true
    }
    onEnable() {
        this.node.stopAllActions();
        this.showLevel();
        GameMag.Ins.gamePause = false;
        GameMag.Ins.gameOver = false;
        GameMag.Ins.sharkJumping = false;
        GameMag.Ins.sharkBiting = false;
        this.bossBlood.progress = 1;
        this.KnifePowerMeter.fillRange = 0
        this.bossBlood.node.active = false;
        this.personBar.progress = 1;
        this.SkillList[0].getChildByName("cd").getComponent(cc.Sprite).fillRange = 1
        this.SkillList[1].getChildByName("cd").getComponent(cc.Sprite).fillRange = 1
        if (this.resultDialogNode) this.resultDialogNode.active = false;
        this.pauseDialog.active = false;
        this.taps.active = false;
        this.moveArea.active = true;
        this.attackArea.active = true;
        this.waterFlower.y = this.jumpLine * 3.2;
        this.moveBigBtn.parent.setPosition(this.circleDefaultPos);
        this.attackBigBtn.parent.setPosition(-this.circleDefaultPos.x, this.circleDefaultPos.y);
        this.sharkBone.setPosition(this.sharkDefaultPos);
        ToolMag.Ins.playDragonBone(this.sharkBone, sharkAction.Stay, 0);
        this.sharkGun.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame("gun" + StorageMag.Ins.getStorage(StorageKey.Using).gun);
        this.sharkKnife.children.forEach((child) => {
            child.active = false;
        })
        this.sharkKnife.children[StorageMag.Ins.getStorage(StorageKey.Using).futou].active = true;
        this.bossBloodNum = 0;
        this.AllbossBloodNum = 0;
        GameMag.Ins.killNum = 0;
        GameMag.Ins.needKillNum = 0;
        GameMag.Ins.isReLive = true
        this.firstGame();
        this.schedule(this.FirstShowPersonTimer, 0.02, 5);
        this.schedule(this.showEnemysTimer, 0.02)
        //最小化显示暂停界面
        //新手教程
    }

    showEnemysTimer() {
        if (!GameMag.Ins.gamePause) {
            this.showEnemys();
            this.unschedule(this.showEnemysTimer);
        }
<<<<<<< HEAD
    }

    firstGame() {
        this.firstGameNode.active = false;
        let firstGame = StorageMag.Ins.getStorage(StorageKey.FirstGame)
        if (!firstGame) return;
        this.firstGameNode.active = true;
        GameMag.Ins.gamePause = true;
        this.showPrompt();
    }
=======
    }

    firstGame() {
        this.firstGameNode.active = false;
        let firstGame = StorageMag.Ins.getStorage(StorageKey.FirstGame)
        if (!firstGame) return;
        this.firstGameNode.active = true;
        GameMag.Ins.gamePause = true;
        this.showPrompt();
    }
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    showPrompt() {
        this.firstGameNode.children.forEach(item => {
            if (item.name != "panel")
                item.active = false;
        })
        this.PromptIndex++;
        if (this.PromptIndex >= this.firstGameNode.childrenCount) {
            this.firstGameNode.active = false;
            GameMag.Ins.gamePause = false;
            StorageMag.Ins.freshFirstGame(0)
            return;
        }
        this.firstGameNode.children[this.PromptIndex].active = true;
    }

    onDisable() {
        this.levelWarn.stopAllActions();
        this.node.stopAllActions();
        this.unschedule(this.showWalkPersonTimer);
        this.unschedule(this.showSwimPersonTimer);
    }

    setdata(data) {
        // console.log(data);
        this.data = data;
        if (this.data)
            return true;
        return false
    }

    showEnemys() {
        //生成小人
        // console.log("生成小人")
        this.unschedule(this.showWalkPersonTimer);
        this.unschedule(this.showSwimPersonTimer);
        this.schedule(this.showWalkPersonTimer, 3);
        this.schedule(this.showSwimPersonTimer, 1);
        // this.data = GameMag.Ins.levelJson[StorageMag.Ins.getStorage(StorageKey.Level) - 1];//当前的关卡数据
        this.showElite(this.data);
        // console.log(GameMag.Ins.levelJson);
        // console.log(data);
    }
    //第一次进游戏先生成几个小人,不让画面太干
    FirstShowPersonTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        const node = PoolMag.Ins.getEnemy(true);
        node.parent = this.personBox;
        node.getComponent(Person).firstShow();
    }
    showWalkPersonTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        if (this.personBox.childrenCount >= 5) return
        const node = PoolMag.Ins.getEnemy(true);
        node.parent = this.personBox;
        node.getComponent(Person).setPos(true);
    }
    showSwimPersonTimer() {
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver) return;
        const node = PoolMag.Ins.getEnemy(true);
        node.parent = this.personBox;
        node.getComponent(Person).setPos(false);
    }
    showBoss(data) {
        this.bossBlood.node.active = true;
        this.personBar.node.active = false;
        let time = 0;
        data.bossArr.forEach(element => {
            time = ToolMag.Ins.randomNum(1, 2);
            cc.tween(this.node)
                .delay(time)
                .call(() => {
                    this.getEnemy(Number(element - 1001));
                })
                .start();
        });
    }
    elitesArr: number[] = [1, 2, 3, 4, 5, 6, 7];
    showElite(data) {
        // this.getEnemy(8);
        // this.getEnemy(9);
        // this.getEnemy(10);
        // this.getEnemy(11);
        // this.getEnemy(12);
        // this.getEnemy(13);
        // this.getEnemy(14);//潜水艇的运动轨迹跟潜水员做成一样
        // this.getEnemy(15);
        // this.getEnemy(16);
        // this.scheduleOnce(() => {
        //     this.getEnemy(4);
        // }, 1);
        // return
        let delayTime = 5;
        for (let i = 0; i < data.eliteNum; i++) {
            delayTime += ToolMag.Ins.randomNum(1, 3, false);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            // console.log(delayTime);
=======
            console.log(delayTime);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
            cc.tween(this.node)
                .delay(delayTime)
                .call(() => {
                    this.elitesArr.sort(() => Math.random() - 0.5);
                    this.getEnemy(this.elitesArr[0]);
                })
                .start();
        }
    }
    getEnemy(id) {
        const result = PoolMag.Ins.getEnemy(false, id);
        if (result instanceof cc.Node) {
            result.parent = this.eliteBox;
            result.getComponent(Enemy).init(id);
            return;
        }
        result.then(
            (res) => {
                res.parent = this.eliteBox;
                res.getComponent(Enemy).init(id);
            },
            (err) => { console.log(err) }
        );
    }
    /**返回主界面 */
    onReturn(isWin: boolean) {
        if (isWin) {
            StorageMag.Ins.freshLevel();
            StorageMag.Ins.freshMaxLevel(StorageMag.Ins.getStorage(StorageKey.Level))
            if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 11) {
                if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                    // @ts-ignore
                    wx.uma.trackEvent("10020")
                }
            }
            if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 21) {
                if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                    // @ts-ignore
                    wx.uma.trackEvent("10021")
                }
            }
            if (StorageMag.Ins.getStorage(StorageKey.MaxLevel) == 31) {
                if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                    // @ts-ignore
                    wx.uma.trackEvent("10022")
                }
            }
        }
        this.node.stopAllActions();
        GameMag.Ins.gamePause = true;
        GameMag.Ins.gameOver = true;
        this.homeMain.active = true;
        this.pauseDialog.active = false;
        this.resultDialog.active = false;
        this.sharkKnife.children.forEach((child) => {
            child.active = false;
        })
        this.node.active = false;
        this.personBox.children.forEach(node => {
            PoolMag.Ins.putEnemy(node, null, true);
        })
        this.eliteBox.children.forEach(node => {
            PoolMag.Ins.putEnemy(node, node.getComponent(Enemy).flag, false);
        });
        if (!this.eliteBox.childrenCount) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            if (isWin) {
                cc.game.emit("onSwitchLevel")
            } else {
                cc.game.emit("initHomeUI")
            }
<<<<<<< HEAD
=======
=======
            cc.game.emit("initHomeUI")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            return;
        }
        for (let i = 0; i < this.eliteBox.childrenCount; i++) {
            PoolMag.Ins.putEnemy(this.eliteBox.children[i], this.eliteBox.children[i].getComponent(Enemy).flag, false);
            if (this.eliteBox.childrenCount == 0) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                if (isWin) {
                    cc.game.emit("onSwitchLevel")
                } else {
                    cc.game.emit("initHomeUI")
                }
<<<<<<< HEAD
=======
=======
                cc.game.emit("initHomeUI")
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            }
            i--;
        }
    }
    /**重新开始 */
    onRestart() {
        this.node.stopAllActions();
        this.node.active = false;
        this.personBox.children.forEach(node => {
            PoolMag.Ins.putEnemy(node, null, true);
        })
        this.eliteBox.children.forEach(node => {
            PoolMag.Ins.putEnemy(node, node.getComponent(Enemy).flag, false);
        });
        this.scheduleOnce(() => {
            this.onResume();
            if (this.resultDialogNode) this.resultDialogNode.active = false;
            this.node.active = true;
        }, 0.1);
    }
    /**继续游戏 */
    onResume() {
        GameMag.Ins.gamePause = false;
        this.pauseDialog.active = false;
        if (this.resultDialogNode) this.resultDialogNode.active = false;
    }
    /**暂停游戏 */
    gamePause() {
        GameMag.Ins.gamePause = true;
        this.pauseDialog.active = true;
    }
    //移动区域
    moveAreaStartPos: cc.Vec2 = null;
    moveAreaTouchEnd: boolean = null;
    nowMoveAngle: number = 0;
    moveAreaDis: number = 0;
    moveAreaDx: number = 0;
    moveAreaDy: number = 0;
    _touchMoveAreaStrat(e: cc.Event.EventTouch) {
        // console.error("Move_touchStrat");
        this.moveAreaTouchEnd = false;
        this.moveAreaStartPos = e.getStartLocation();
        let lps = this.moveSmallBtn.parent.convertToNodeSpaceAR(e.getLocation());
        this.moveBigBtn.setPosition(lps);
        this.moveSmallBtn.setPosition(lps);
        this.moveBigBtn.parent.scale = 1.2
        this.moveBigBtn.parent.opacity = 255
        this.moveBigBtn.parent.active = true;
        this.moveBigBtn.parent.stopAllActions();
        SoundMag.Ins.swim_ID = cc.audioEngine.playEffect(SoundMag.Ins.getsharkSwim(), true);
    }
    _touchMoveAreaMove(e: cc.Event.EventTouch) {
        if (!this.moveAreaStartPos) return;
        this.isMove = true;
        this.moveAreaDx = e.getLocation().x - this.moveAreaStartPos.x;
        this.moveAreaDy = e.getLocation().y - this.moveAreaStartPos.y;
        this.moveAreaDis = Math.sqrt(this.moveAreaDx * this.moveAreaDx + this.moveAreaDy * this.moveAreaDy);
        if (this.moveAreaDis > this.moveBigBtn.width / 2) {
            this.moveAreaDx = (this.moveAreaDx * this.moveBigBtn.width / 2 / this.moveAreaDis);
            this.moveAreaDy = (this.moveAreaDy * this.moveBigBtn.width / 2 / this.moveAreaDis);
        }
        this.nowMoveAngle = (Math.atan2(this.moveAreaDx, this.moveAreaDy) / Math.PI * 180) - 90;
        this.moveSmallBtn.setPosition(this.moveBigBtn.x + this.moveAreaDx, this.moveBigBtn.y + this.moveAreaDy);
    }
    _touchMoveAreaEnd() {
        // console.log("_touchEnd");
        if (GameMag.Ins.sharkBiting) return;
        this.isMove = false;
        this.sharkBone.angle = 0;
        this.nowMoveAngle = 0;
        this.moveAreaStartPos = null;
        this.moveBigBtn.setPosition(cc.v2(0, 0));
        this.moveSmallBtn.setPosition(cc.v2(0, 0));
        this.moveBigBtn.parent.scale = 1.0
        cc.tween(this.moveBigBtn.parent).
            then(cc.fadeOut(0.2)).
            call(() => {
                this.moveBigBtn.parent.active = false;
            }).
            call(() => {
                this.moveBigBtn.parent.opacity = 255;
            });
        this.moveAreaTouchEnd = true;
        cc.audioEngine.stopEffect(SoundMag.Ins.swim_ID);
    }

    //攻击区域
    attackAreaStartPos: cc.Vec2 = null;
    attackAreaTouchEnd: boolean = null;
    nowAttackAngle: number = 0;
    attackAreaDis: number = 0;
    attackAreaDx: number = 0;
    attackAreaDy: number = 0;
    _touchAttackAreaStrat(e: cc.Event.EventTouch) {
        this.isTap = true;
        this.attackAreaStartPos = e.getStartLocation();
        let lps = this.attackSmallBtn.parent.convertToNodeSpaceAR(e.getLocation());
        // this.attackBigBtn.setPosition(lps);
        // this.attackSmallBtn.setPosition(lps);
        this.unschedule(this.fireTimer);
        this.schedule(this.fireTimer, 0.2);
        this.isAutoAim = true;
        this.randomAim = 0
        this.attackSmallBtn.scale = 1.2
<<<<<<< HEAD
        // console.log(this.eliteBox.children[this.randomAim])
=======
<<<<<<< HEAD
        // console.log(this.eliteBox.children[this.randomAim])
=======
        console.log(this.eliteBox.children[this.randomAim])
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }
    _touchAttackAreaMove(e: cc.Event.EventTouch) {
        if (!this.attackAreaStartPos) return;
        this.attackAreaDx = e.getLocation().x - this.attackAreaStartPos.x;
        this.attackAreaDy = e.getLocation().y - this.attackAreaStartPos.y;
        this.attackAreaDis = Math.sqrt(this.attackAreaDx * this.attackAreaDx + this.attackAreaDy * this.attackAreaDy);
        if (this.attackAreaDis / this.attackBigBtn.width <= 0.05) return;
        if (this.attackAreaDis > this.attackBigBtn.width / 2) {
            this.attackAreaDx = (this.attackAreaDx * this.attackBigBtn.width / 2 / this.attackAreaDis);
            this.attackAreaDy = (this.attackAreaDy * this.attackBigBtn.width / 2 / this.attackAreaDis);
        }
        this.attackSmallBtn.setPosition(this.attackBigBtn.x + this.attackAreaDx, this.attackBigBtn.y + this.attackAreaDy);
        this.sharkGun.angle = (cc.v2(0, 1).signAngle(e.getStartLocation().subtract(e.getLocation()).normalize()) * 180 / Math.PI) - 90;
        this.isAutoAim = false;
    }
    _touchAttackAreaEnd() {
        if (GameMag.Ins.sharkBiting) return;
        this.isTap = false;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        // console.error("tapTime：" + this.tapTime);
        if (this.tapTime <= 0.2 && this.tapTime > 0) {
            this.autoAim(this.eliteBox.children[0]);
            this.fireTimer();
        }
        this.tapTime = 0;
<<<<<<< HEAD
=======
=======
        console.error("tapTime：" + this.tapTime);
        if (this.tapTime <= 0.2) {
            this.autoAim(this.eliteBox.children[0]);
            this.fireTimer();
        }
        this.tapTime = 0
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.unschedule(this.fireTimer);
        this.sharkGun.angle = 0;
        this.attackAreaStartPos = null;
        this.attackSmallBtn.scale = 1
        this.attackBigBtn.setPosition(cc.v2(0, 0));
        this.attackSmallBtn.setPosition(cc.v2(0, 0));
        this.attackAreaTouchEnd = true;
        this.isAutoAim = false
    }
    /**发射子弹 */
    fireTimer() {
        const node = PoolMag.Ins.getBullet();
        node.opacity = 0;
        node.parent = this.bulletBox;
        const wps = this.bulletStartPos.parent.convertToWorldSpaceAR(this.bulletStartPos.getPosition());
        const lps = node.parent.convertToNodeSpaceAR(wps);
        node.setPosition(lps);
        node.getComponent(Bullet).setAngle(this.sharkGun.angle);
        node.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame("bullet" + StorageMag.Ins.getStorage(StorageKey.Using).gun);
        node.opacity = 255;
        cc.audioEngine.playEffect(SoundMag.Ins.getgunSound(StorageMag.Ins.getStorage(StorageKey.Using).gun), false);

    }
    fireDom() {
        cc.tween(this.node)
            .then(cc.sequence(cc.delayTime(0.2), cc.callFunc(() => {
                this.autoAim(this.eliteBox.children[0]);
                const node = PoolMag.Ins.getDom();
                node.opacity = 0;
                node.parent = this.bulletBox;
                const wps = this.bulletStartPos.parent.convertToWorldSpaceAR(this.bulletStartPos.getPosition());
                const lps = node.parent.convertToNodeSpaceAR(wps);
                node.setPosition(lps);
                node.getComponent(dom).setAngle(this.sharkGun.angle);
                node.opacity = 255;
                this.sharkGun.angle = 0;
            })))
            .repeat(3)
            .start();
    }
    addHp() {
        GameMag.Ins.sharkHp += (GameMag.Ins.ActiveSkill[0].name == "治疗" ? GameMag.Ins.ActiveSkill[0].power : GameMag.Ins.ActiveSkill[1].power)
        this.sharkBone.getComponent(Shark).addhp(GameMag.Ins.sharkHp);
    }
    /**鲨鱼跳跃 */
    sharkJumpAction(num: number) {
        GameMag.Ins.sharkJumping = true;
        cc.game.emit("freshPersonRun");
        cc.audioEngine.stopEffect(this.jumpSoundID);
        this.jumpSoundID = cc.audioEngine.playEffect(SoundMag.Ins.getsharkJump(), false);
        this.waterFlower.x = this.sharkBone.x;
        this.waterFlower.active = true;
        ToolMag.Ins.playDragonBone(this.waterFlower, "Sprite", 1, () => {
            this.waterFlower.active = false;
        });
        // let sqe = cc.sequence(cc.spawn(cc.jumpTo(0.7, cc.v2(this.sharkBone.x, 0), 260, 1), cc.rotateTo(0.5, 45))
        //     , cc.callFunc(() => {
        //         GameMag.Ins.sharkJumping = false;
        //         this.moveDistance = 0;
        //     }))
        if (this.moveDistance >= 0.5) {
            cc.tween(this.sharkBone)
                .call(() => { cc.audioEngine.playEffect(SoundMag.Ins.getWater(0), false) })
<<<<<<< HEAD
                .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, 180 * 1.5)), cc.rotateTo(0.4, 0)))
                .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, -200 * 1.5)), cc.rotateTo(0.4, 45)))
=======
<<<<<<< HEAD
                .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, 180 * 1.5)), cc.rotateTo(0.4, 0)))
                .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, -200 * 1.5)), cc.rotateTo(0.4, 45)))
=======
                .then(cc.spawn(cc.moveBy(0.35, cc.v2(0, 180)), cc.rotateTo(0.25, 0)))
                .then(cc.spawn(cc.moveBy(0.35, cc.v2(0, -200)), cc.rotateTo(0.25, 45)))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                .call(() => { cc.audioEngine.playEffect(SoundMag.Ins.getWater(1), false) })
                .call(() => {
                    this.moveDistance = 0;
                    this.sharkBone.angle = 0;
                })
                .call(() => {
                    GameMag.Ins.sharkJumping = false;
                })
                .start();
            return;
            // sqe = cc.sequence(cc.spawn(cc.jumpTo(0.7, cc.v2(this.sharkBone.x, 0), 360, 1),cc.moveTo(0.3,cc.v2(this.sharkBone.x+num, this.sharkBone.y)), cc.rotateTo(0.5, 45))
            //     , cc.callFunc(() => {
            //         GameMag.Ins.sharkJumping = false;
            //         this.moveDistance = 0;
            //     }))

        }
        // this.sharkBone.runAction(sqe);
        cc.tween(this.sharkBone)
<<<<<<< HEAD
            .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, 130 * 1.5)), cc.rotateTo(0.4, 0)))
            .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, -150 * 1.5)), cc.rotateTo(0.4, 45)))
=======
<<<<<<< HEAD
            .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, 130 * 1.5)), cc.rotateTo(0.4, 0)))
            .then(cc.spawn(cc.moveBy(0.4, cc.v2(0, -150 * 1.5)), cc.rotateTo(0.4, 45)))
=======
            .then(cc.spawn(cc.moveBy(0.35, cc.v2(0, 130)), cc.rotateTo(0.2, 0)))
            .then(cc.spawn(cc.moveBy(0.35, cc.v2(0, -150)), cc.rotateTo(0.2, 45)))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            .call(() => {
                this.moveDistance = 0;
                this.sharkBone.angle = 0;
            })
            .call(() => {
                GameMag.Ins.sharkJumping = false;
            })
            .start();
    }
    sche: number = 0;
    update(dt) {
        this.currency.active = GameMag.Ins.gamePause || GameMag.Ins.gameOver;
        if (GameMag.Ins.gamePause || GameMag.Ins.gameOver || GameMag.Ins.sharkBiting) {
            return;
        }
        if (this.isTap) {
            this.tapTime += dt;
        }
        if (this.sharkBone.x > (-this.screenSize.width / 2)) {
            this.sharkBone.x -= 0.2;
        }
        if (this.moveAreaStartPos) {
            // console.log(this.nowMoveAngle);
            const radians = cc.misc.degreesToRadians(-this.nowMoveAngle);
            if (this.nowMoveAngle == 0 || this.nowMoveAngle == -90) return;
            let targetPos: cc.Vec2 = cc.v2(Math.cos(radians) * 1000, Math.sin(radians) * 1000);
            let startPos: cc.Vec2 = this.sharkBone.getPosition();
            let normalizeVec: cc.Vec2 = targetPos.subtract(startPos).normalize();

            if (this.sharkBone.x < (-this.screenSize.width / 2) + 100) {
                this.sharkBone.x = (-this.screenSize.width / 2) + 100;
            }
            if (this.sharkBone.x > (this.screenSize.width / 2) - (this.sharkBone.width / 2)) {
                this.sharkBone.x = (this.screenSize.width / 2) - (this.sharkBone.width / 2);
            }
            if (this.sharkBone.y < (-this.screenSize.height / 2) + (this.sharkBone.height / 2)) {
                this.sharkBone.y = (-this.screenSize.height / 2) + (this.sharkBone.height / 2);
            }
            if (this.isMove) {
                if (GameMag.Ins.sharkJumping) {
                    this.sharkBone.x += this.movedt * this.speed * dt;
                } else {
                    this.movedt = normalizeVec.x
                    if (normalizeVec.x > 0) {
                        this.sharkBone.x += normalizeVec.x * this.speed * 1.1 * dt;
                    } else {
                        this.sharkBone.x += normalizeVec.x * this.speed * 0.7 * dt;
                    }
                }
                if (!GameMag.Ins.sharkJumping)
                    this.sharkBone.y += normalizeVec.y * this.speed * 0.7 * dt;
                this.moveDistance += dt;

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                if (this.sharkBone.y > this.jumpLine && !GameMag.Ins.sharkJumping) {
                    this.sharkJumpAction(normalizeVec.x);
                }
                if (!GameMag.Ins.sharkJumping)
                    this.sharkBone.angle = cc.v2(1, 0).signAngle(normalizeVec) * 180 / Math.PI;
                // console.log(this.sharkBone.angle);
                this.sharkBone.angle = cc.misc.clampf(this.sharkBone.angle, -50, 60);
                if (normalizeVec.x <= 0) {
                    this.sharkBone.angle = 0
                }
<<<<<<< HEAD
            }
=======
            }
=======
            if (GameMag.Ins.sharkJumping) {
                this.sharkBone.x += this.movedt * this.speed * dt;
            } else {
                this.movedt = normalizeVec.x
                this.sharkBone.x += normalizeVec.x * this.speed * dt;
            }
            if (!GameMag.Ins.sharkJumping)
                this.sharkBone.y += normalizeVec.y * this.speed * dt;
            this.moveDistance += dt;

            if (this.sharkBone.y > this.jumpLine && !GameMag.Ins.sharkJumping) {
                this.sharkJumpAction(normalizeVec.x);
            }
            if (!GameMag.Ins.sharkJumping)
                this.sharkBone.angle = cc.v2(1, 0).signAngle(normalizeVec) * 180 / Math.PI;
            // console.log(this.sharkBone.angle);
            this.sharkBone.angle = cc.misc.clampf(this.sharkBone.angle, -50, 60);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        }
        if (this.isAutoAim) {
            this.autoAim(this.eliteBox.children[this.randomAim]);
        }
    }
    /**将枪口朝向敌人 */
    autoAim(other: cc.Node) {
        if (!other) return;
        let otherPos = this.eliteBox.convertToWorldSpaceAR(other.position)
        let GunPos = this.sharkGun.getParent().convertToWorldSpaceAR(this.sharkGun.position)
        let orientationX = otherPos.x - GunPos.x;
        let orientationY = otherPos.y - GunPos.y;
        let dir = cc.v2(orientationY, orientationX);
        // //计算夹角弧度(cc.v2(0,1)表示物体基于Y轴方向)
        let angle2 = dir.signAngle(cc.v2(1, 0))
        // //弧度转换成欧拉角
        let olj = ToolMag.Ins.radianToAngle(angle2);
        // //物体朝向
        this.sharkGun.angle = olj + 90;
    }
    /**出现boss才会有,刷新boss的血条 */
    freshBossBlood(isBite: boolean, power: number, cb: Function) {
        if (this.bossBlood.progress < 0) return;
        // let power = 0;
        // if (isBite) {
        //     power = GameMag.Ins.sharkBitePower
        // }
        // power = GameMag.Ins.sharkPower
        this.bossBloodNum -= power
        this.bossBlood.progress = this.bossBloodNum / this.AllbossBloodNum;
        if (this.bossBlood.progress <= 0) {
            cc.game.emit("allDie");
            cb && cb(true);
            return;
        }
        cb && cb(false);
    }

    showTaps() {
        this.sharkBone.stopAllActions();
        this._touchMoveAreaEnd();
        this._touchAttackAreaEnd();
        GameMag.Ins.sharkBiting = true;
        this.taps.active = true;
        // this.taps.getChildByName("tapBtnleft").setPosition(this.moveBigBtn.parent.position)
        // this.taps.getChildByName("tapBtnright").setPosition(this.attackBigBtn.parent.position);
        this.moveBigBtn.parent.active = false;
        this.attackBigBtn.parent.active = false;
        this.KnifeBtn.active = false;
        this.SkillList[0].parent.active = false;
        this.sharkBone.angle = 60;
        this.scheduleOnce(this.bitedEndTime, 5);
        this.moveArea.active = false;
        this.attackArea.active = false;
        this.taps.children.forEach((item, index) => {
            if (item.name != "tapBtnleft" && item.name != "tapBtnright") return
            cc.tween(item)
                .delay(index * 0.15)
                .then(cc.sequence(cc.callFunc(() => {
                    item.getChildByName("bling").active = true;
                }), cc.scaleTo(0.1, 1.1), cc.scaleTo(0.05, 1.0), cc.callFunc(() => {
                    item.getChildByName("bling").active = false;
                    this.sharkBone.y -= 0.1;
                    cc.game.emit("bitingPlane", 0.1);
                })))
                .repeatForever().start();
        })
    }
    onTapBtn(e) {
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        this.sharkBone.y -= 5;
        cc.game.emit("bitingPlane", 5);
    }
    bitedEndTime() {
        cc.game.emit("releaseByBite");
        this.bitedEnd();
    }
    //结束撕咬
    bitedEnd() {
        this.sharkBone.angle = 0;
        this.moveArea.active = true;
        this.attackArea.active = true;
        this.moveBigBtn.parent.active = true;
        this.KnifeBtn.active = true;
        this.attackBigBtn.parent.active = true;
        this.SkillList[0].parent.active = true;
        this.taps.active = false;
        this.sharkBone
        cc.tween(this.sharkBone)
            .to(0.02, { position: cc.v3(this.sharkBone.x, this.jumpLine - 100, 0) })
            .call(() => {
                GameMag.Ins.sharkBiting = false;
                GameMag.Ins.sharkJumping = false;
            })
            .start();
        this.unschedule(this.bitedEndTime);
        this.taps.children.forEach((item) => {
            if (item.name != "tapBtnleft" && item.name != "tapBtnright") return
            item.stopAllActions;
        })
    }
<<<<<<< HEAD
    showGameCoin(target: cc.Node, flag: number) {
        let num = Number(GameMag.Ins.enemyJson[Number(flag)].coin)
=======
<<<<<<< HEAD
    showGameCoin(target: cc.Node, flag: number) {
        let num = Number(GameMag.Ins.enemyJson[Number(flag)].coin)
=======
    showGameCoin(target: cc.Node, coinNum: number) {
        let num = Number(GameMag.Ins.enemyJson[Number(coinNum)].coin)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        // console.log(target, num);
        if (flag > 0) {
            num = Math.floor(num / 10)
        }
        for (let i = 0; i < num; i++) {
            let coin = PoolMag.Ins.getCoinNode();
            coin.stopAllActions();
            coin.parent = this.coinBox;
            coin.setPosition(target.position)
            flag > 0 ? coin.scale = 1.3 : coin.scale = 1
            cc.tween(coin)
                .call(() => {
                    coin.getComponent(cc.Label).string = ""
                    coin.getChildByName("gameCoin").active = true;

                })
                .then(cc.jumpTo(0.5, cc.v2(coin.x + (Math.random() * 300 - 150), (Math.random() * 100) - 50), (Math.random() * 100) + 200, 1).easing(cc.easeCircleActionInOut()))
                .then(cc.delayTime(0.6))
                .call(() => {
                    coin.getComponent(cc.Label).string = "+" + (flag > 0 ? 10 : 1)
                    coin.getChildByName("gameCoin").active = false;
                    cc.game.emit("addCoin", (flag > 0 ? 10 : 1));
                })
                .delay(0.2)
                .call(() => {
                    PoolMag.Ins.putCoinNode(coin);
                })
                .start();
        }
    }
}
