// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import StorageMag, { StorageKey } from "./manage/StorageMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class varyItem extends cc.Component {

    @property(cc.Label)
    headName: cc.Label = null;
    @property(cc.Label)
    bodyName: cc.Label = null;
    @property(cc.Label)
    finsName: cc.Label = null;
    @property(cc.Label)
    tailName: cc.Label = null;
    @property(cc.Label)
    gunName: cc.Label = null;
    @property(cc.Label)
    knifeName: cc.Label = null;
    @property(cc.Label)
    AllName: cc.Label = null;

    @property(cc.Sprite)
    head_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    body_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    fins_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    tail_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    gun_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    knife_Icon: cc.Sprite = null;

    @property(cc.Node)
    headStarNode: cc.Node = null
    @property(cc.Node)
    bodyStarNode: cc.Node = null
    @property(cc.Node)
    finsStarNode: cc.Node = null
    @property(cc.Node)
    tailStarNode: cc.Node = null
    @property(cc.Node)
    gunStarNode: cc.Node = null
    @property(cc.Node)
    knifeStarNode: cc.Node = null
    @property(cc.Node)
    unlock: cc.Node = null;
    @property(cc.Node)
    open: cc.Node = null;
    @property(cc.Label)
    Lock_Pay: cc.Label = null;


    @property(cc.SpriteAtlas)
    imgList: cc.SpriteAtlas = null;

    data: any = null

    // LIFE-CYCLE CALLBACKS:


    setInfo(data) {
        this.data = data;

        this.unlock.active = !data[0].data.Allgeted;

        for (let key in this.data) {
            if (data[key].data.ID % 1000 == 6) {
                this.knifeName.string = data[key].data.name
                this.knife_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.knifeStarNode, data[key].data.star)
                this.node.getChildByName("knife").getChildByName("park_lock").active = !data[key].data.geted;
            }
            if (data[key].data.ID % 1000 == 5) {
                this.gunName.string = data[key].data.name
                this.gun_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.gunStarNode, data[key].data.star)
                this.node.getChildByName("gun").getChildByName("park_lock").active = !data[key].data.geted;
            }
            if (data[key].data.ID % 1000 == 2) {
                this.bodyName.string = data[key].data.name
                this.body_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.bodyStarNode, data[key].data.star)
                this.node.getChildByName("body").getChildByName("park_lock").active = !data[key].data.geted;
            }
            if (data[key].data.ID % 1000 == 3) {
                this.finsName.string = data[key].data.name
                this.fins_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.finsStarNode, data[key].data.star)
                this.node.getChildByName("fins").getChildByName("park_lock").active = !data[key].data.geted;
            }
            if (data[key].data.ID % 1000 == 4) {
                this.tailName.string = data[key].data.name
                this.tail_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.tailStarNode, data[key].data.star)
                this.node.getChildByName("tail").getChildByName("park_lock").active = !data[key].data.geted;
            }
            if (data[key].data.ID % 1000 == 1) {
                this.headName.string = data[key].data.name
                this.head_Icon.spriteFrame = this.imgList.getSpriteFrame(data[key].data.img)
                this.setStarNum(this.headStarNode, data[key].data.star)
                this.node.getChildByName("head").getChildByName("park_lock").active = !data[key].data.geted;
            }
        }
        this.AllName.string = data[0].type + "基因";
        this.Lock_Pay.string = data[0].data.unlockAllDO + "";
        this.Lock_Pay.node.color = data[0].data.unlockAllDO > StorageMag.Ins.getStorage(StorageKey.Currency).diamond ? cc.color(255, 0, 19) : cc.color(255, 255, 255)
    }

    setClick(parent: cc.Node, Node: cc.Node) {
        if (parent != this.node) {
            this.node.children.forEach((icon, index) => {
                if (icon.name != "bg" && icon.name != "point" && icon.name != "labNode" && icon.name != "unlock") {
                    icon.getChildByName("light").active = false
                }
            })
            return
        }
        this.node.children.forEach((icon, index) => {
            if (icon.name != "bg" && icon.name != "point" && icon.name != "labNode" && icon.name != "unlock") {
                icon.getChildByName("light").active = false
            }
        })
        Node.getChildByName("light").active = true;
    }

    onLoad() {
        this.node.on("addstar", (node: cc.Node, num) => {
            // console.log("varyItem", node);
            this.setStarNum(node.getChildByName("starNode"), num)
        }, this);
        cc.game.on("nextUnlock", (index) => {
            let num = 0;
            if (!this.node.parent) return
            if (this.node.parent.children.indexOf(this.node) - index != -1) {
                return;
            }
            for (let key in this.data) {
                if (this.data[key].data.geted)
                    num++
            }
            cc.game.emit("UnlockRet", index, num == 6);
        }, this);
<<<<<<< HEAD
        cc.game.on("UnlockRet", this.lockRet, this)
=======
<<<<<<< HEAD
        cc.game.on("UnlockRet", this.lockRet, this)
=======
        cc.game.on("UnlockRet", (index, ok) => {
            if (!this.node.parent) return
            if (this.node.parent.children.indexOf(this.node) != index)
                return;
            if (!ok)
                return;
            cc.game.emit("addDiamond", -this.data[0].data.unlockAllDO, (success) => {
                if (success) {
                    this.data.forEach((item, i) => {
                        item.data.Allgeted = true;
                        StorageMag.Ins.freshGetData(item.data.name, item.data.geted, item.data.star);
                    })
                    this.setInfo(this.data);
                } else {
                    console.error("失败")
                }
            });
        })
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        cc.game.on("setClick", this.setClick, this);
        this.open.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("nextUnlock", this.node.parent.children.indexOf(this.node));
        }, this);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }

    lockRet(index, ok) {
        if (!this.node.parent) return
        if (this.node.parent.children.indexOf(this.node) != index)
            return;
        if (!ok)
            return;
        cc.game.emit("addDiamond", -this.data[0].data.unlockAllDO, (success) => {
            if (success) {
                if (Math.floor(this.data[0].data.ID / 1000) == 3) {
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10012")
                    }
                } else if (Math.floor(this.data[0].data.ID / 1000) == 4){
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10013")
                    }
                }else if(Math.floor(this.data[0].data.ID / 1000) == 5){
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10014")
                    }
                }
                this.data.forEach((item, i) => {
                    item.data.Allgeted = true;
                    StorageMag.Ins.freshGetData(item.data.name, item.data.geted, item.data.star);
                })
                this.setInfo(this.data);
            } else {
                console.error("失败")
            }
        });
<<<<<<< HEAD
=======
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }

    setStarNum(targer: cc.Node, Num: number) {
        switch (Num) {
            case 1:
                targer.children[0].children[0].active = true;
                targer.children[1].children[0].active = false;
                targer.children[2].children[0].active = false;
                targer.parent.getChildByName("park_lock").active = false;
                break;
            case 2:
                targer.children[0].children[0].active = true;
                targer.children[1].children[0].active = true;
                targer.children[2].children[0].active = false;
                targer.parent.getChildByName("park_lock").active = false;

                break;
            case 3:
                targer.children[0].children[0].active = true;
                targer.children[1].children[0].active = true;
                targer.children[2].children[0].active = true;
                targer.parent.getChildByName("park_lock").active = false;
                break;
            default:
                targer.children[0].children[0].active = false;
                targer.children[1].children[0].active = false;
                targer.children[2].children[0].active = false;
                targer.parent.getChildByName("park_lock").active = false;
                break;
        }
    }

    onDestroy() {
        cc.game.on("setClick", this.setClick, this);
        this.node.off("addstar", (node: cc.Node, num) => {
            // console.log("varyItem", node);
            this.setStarNum(node.getChildByName("starNode"), num)
        }, this);
        this.open.off(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("nextUnlock", this.node.parent.children.indexOf(this.node));
        }, this);
        cc.game.off("nextUnlock", (index) => {
            let num = 0;
            if (this.node.parent.children.indexOf(this.node) - index != -1) {
                return;
            }
            for (let key in this.data) {
                if (this.data[key].data.geted)
                    num++
            }
            cc.game.emit("UnlockRet", index, num == 6);
        }, this);
<<<<<<< HEAD
        cc.game.off("UnlockRet", this.lockRet, this)
=======
<<<<<<< HEAD
        cc.game.off("UnlockRet", this.lockRet, this)
=======
        cc.game.off("UnlockRet", (index, ok) => {
            if (this.node.parent.children.indexOf(this.node) != index)
                return;
            if (!ok)
                return;
            cc.game.emit("addDiamond", -this.data[0].data.unlockAllDO, (success) => {
                if (success) {
                    this.data.forEach((item, i) => {
                        item.data.Allgeted = true;
                        // StorageMag.Ins.freshGetData(item.data.name, item.data.geted, item.data.star);
                    })
                    this.setInfo(this.data);
                } else {
                    console.error("失败")
                }
            });
        })
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }

    // update (dt) {}
}
