// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameMain from "../../game/script/gameMain";
import HomeMain from "./homeMain";
import GameMag from "./manage/GameMag";
import SoundMag from "./manage/SoundMag";
import StorageMag, { StorageKey } from "./manage/StorageMag";
import ToolMag from "./manage/ToolMag";
import varyPage from "./varyPage";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelClick extends cc.Component {

    @property(cc.Node)
    Levellist: cc.Node = null;
    @property(cc.Node)
    backBtn: cc.Node = null;
    @property(cc.Node)
    makeBtn: cc.Node = null;
    @property(cc.Node)
    varyBtn: cc.Node = null;
    @property(cc.Node)
    makePage: cc.Node = null;
    @property(cc.Node)
    varyPage: cc.Node = null;
    @property(cc.Label)
    levelName: cc.Label = null;
    @property(cc.Label)
    levelNum: cc.Label = null;
    @property(cc.Prefab)
    page: cc.Prefab = null;
    @property(cc.AudioClip)
    makeBG: cc.AudioClip = null;
    @property(cc.AudioClip)
    varyBG: cc.AudioClip = null;

    @property(cc.Node)
    varyPageRedPoint: cc.Node = null;

    @property(cc.Node)
    left: cc.Node = null;
    @property(cc.Node)
    right: cc.Node = null;

    nowPage: number = 0

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let view = this.Levellist.getComponent(cc.PageView);
        this.makeBtn.on(cc.Node.EventType.TOUCH_END, () => {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10011")
            }
            this.node.active = false;
            GameMag.Ins.isHomeIn = false;
            this.makePage.active = true;
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            GameMag.Ins.isHomeIn = false;
            SoundMag.Ins.makeBG_ID = cc.audioEngine.playMusic(this.makeBG, true);
        }, this);
        this.varyBtn.on(cc.Node.EventType.TOUCH_END, () => {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10010")
            }
            this.node.active = false;
            GameMag.Ins.isHomeIn = false;
            this.varyPage.active = true;
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            GameMag.Ins.isHomeIn = false;
            // this.varyPage.getComponent(varyPage).setmakeData();
            SoundMag.Ins.makeBG_ID = cc.audioEngine.playMusic(this.varyBG, true);
        }, this)
        this.left.on(cc.Node.EventType.TOUCH_END, () => {
            view.setCurrentPageIndex(--this.nowPage);
        })
        this.right.on(cc.Node.EventType.TOUCH_END, () => {
            view.setCurrentPageIndex(++this.nowPage);
        })
        this.levelNum.string = `最高关卡：${StorageMag.Ins.getStorage(StorageKey.MaxLevel)}/${GameMag.Ins.levelJson.length}`;
        for (let i = 0; i < GameMag.Ins.levelList.length; i++) {
            let page = cc.instantiate(this.page);
            view.content.addChild(page);
            page.setPosition(cc.v2(0, 0))
            page.getChildByName("node").children.forEach((level, index) => {
                if (level.name != "line" && level.name != "LevelLab") {
                    level.on(cc.Node.EventType.TOUCH_END, this.clickLevel, this);
                }
                // console.error("关卡", GameMag.Ins.levelJson[level.parent.parent.parent.children.indexOf(level.parent.parent) * 10 + (index)])
                level.parent.parent.getChildByName("LevelLab").children[index].getComponent(cc.Label).string = `第${GameMag.Ins.levelJson[level.parent.parent.parent.children.indexOf(level.parent.parent) * 10 + (index)].nums}关`
                level.parent.parent.getChildByName("LevelLab").children[index].name = GameMag.Ins.levelJson[level.parent.parent.parent.children.indexOf(level.parent.parent) * 10 + (index)].nums + ""
            })
        }
        this.backBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("freshHomeShark");
            cc.game.emit("initHomeUI")
            this.node.active = false;
        }, this);
        this.Levellist.on("page-turning", () => {
            this.levelName.string = (view.getCurrentPageIndex() + 1) + " " + GameMag.Ins.levelList[view.getCurrentPageIndex()].name;
            // console.log(GameMag.Ins.levelList[view.getCurrentPageIndex()].num)
        })

    }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    setRedPoint() {
        this.varyPageRedPoint.active = this.varyPage.getComponent(varyPage).isRedPoint();
    }


<<<<<<< HEAD
=======
=======
    
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97

    show() {
        let view = this.Levellist.getComponent(cc.PageView);
        let MaxLevel = StorageMag.Ins.getStorage(StorageKey.MaxLevel)
        this.nowPage = Math.floor(MaxLevel / 10) < MaxLevel / 10 ? Math.floor(MaxLevel / 10) : Math.floor(MaxLevel / 10) - 1
        view.setCurrentPageIndex(this.nowPage);
        this.levelName.string = (view.getCurrentPageIndex() + 1) + " " + GameMag.Ins.levelList[view.getCurrentPageIndex()].name;
        this.Levellist.getComponent(cc.PageView).content.children.forEach((page) => {
            page.getChildByName("node").children.forEach((level, index) => {
                // console.error("关卡", GameMag.Ins.levelJson[level.parent.parent.parent.children.indexOf(level.parent.parent) * 10 + (index)])
                level.getChildByName("this").stopAllActions();
                if (Number(level.parent.parent.getChildByName("LevelLab").children[index].name) > StorageMag.Ins.getStorage(StorageKey.MaxLevel)) {
                    level.getChildByName("red").active = false;
                    level.getChildByName("gold").active = false;
                    level.getChildByName("this").active = false;
                    level.getChildByName("shark").active = false;
                } else if (Number(level.parent.parent.getChildByName("LevelLab").children[index].name) < StorageMag.Ins.getStorage(StorageKey.MaxLevel)) {
                    level.getChildByName("red").active = false;
                    level.getChildByName("gold").active = true;
                    level.getChildByName("this").active = false;
                    level.getChildByName("shark").active = false;
                } else if (Number(level.parent.parent.getChildByName("LevelLab").children[index].name) == StorageMag.Ins.getStorage(StorageKey.MaxLevel)) {
                    level.getChildByName("red").active = true;
                    level.getChildByName("gold").active = false;
                    level.getChildByName("this").active = true;
                    level.getChildByName("shark").active = true;
                    cc.tween(level.getChildByName("this"))
<<<<<<< HEAD
                        .then(cc.sequence(cc.moveBy(0.4, cc.v2(0, 10)), cc.moveBy(0.4, cc.v2(0, -10))))
=======
<<<<<<< HEAD
                        .then(cc.sequence(cc.moveBy(0.4, cc.v2(0, 10)), cc.moveBy(0.4, cc.v2(0, -10))))
=======
                        .then(cc.sequence(cc.moveBy(0.5, cc.v2(0, 10)), cc.moveBy(0.5, cc.v2(0, -10))))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                        .repeatForever()
                        .start();
                }
                level.getChildByName("boss").active = Number(level.parent.parent.getChildByName("LevelLab").children[index].name) % 5 == 0
            })
        })
        this.setRedPoint();
    }

    start() {

    }

    onEnable() {

    }

    clickLevel(e: cc.Event.EventTouch) {
        let i = (e.target as cc.Node).parent.parent.parent.children.indexOf((e.target as cc.Node).parent.parent) * 10 + Number(e.target.name)
        // console.log(i)
        const data = GameMag.Ins.levelJson[i - 1]
        // console.error(StorageMag.Ins.getStorage(StorageKey.MaxLevel))
        if (i > StorageMag.Ins.getStorage(StorageKey.MaxLevel)) return;
        StorageMag.Ins.setLevel(i);
        if (cc.find("Canvas/gameMain").getComponent(GameMain).setdata(data)) {
            cc.find("Canvas").getComponent(HomeMain).Joingame();
        }
    }

    // update (dt) {}
}
