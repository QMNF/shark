// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class makeItem extends cc.Component {

    @property(cc.Label)
    Name: cc.Label = null;
    @property(cc.Label)
    Skill_Name: cc.Label = null;
    @property(cc.Label)
    Skill_Info: cc.Label = null;

    @property(cc.Sprite)
    Part_Icon: cc.Sprite = null;
    @property(cc.Sprite)
    Skill_Icon: cc.Sprite = null;

    @property(cc.Node)
    starNode: cc.Node = null;

    @property(cc.SpriteAtlas)
    imgShark: cc.SpriteAtlas = null;
    @property(cc.SpriteAtlas)
    imghome: cc.SpriteAtlas = null;

    data: any = null

    // LIFE-CYCLE CALLBACKS:

    setInfo(data) {
        this.data = data;
        this.Name.string = data.name;
        this.Skill_Name.string = "【" + data.skill + "】";
        this.Skill_Info.string = this.setskillInfo(data);
        this.Skill_Icon.spriteFrame = this.imghome.getSpriteFrame(data.skill == "手雷" || data.skill == "治疗" ? data.skill + "1" : data.skill);
        this.Part_Icon.spriteFrame = this.imgShark.getSpriteFrame(data.img);
        this.setStarNum(data.star);
    }
    setskillInfo(data) {
        switch (data.skill) {
            case "强力":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}攻击`
            case "手雷":
                return `发射炮弹，造成${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}伤害`
            case "治疗":
                return `使用后恢复${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}生命`
            case "健体":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}生命`
            case "坚固":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}防御`
            case "锋利":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}攻击`
            default:
                return "";
        }
    }

    setStarNum(Num: number) {
        switch (Num) {
            case 1:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = false;
                this.starNode.children[2].children[0].active = false;

                break;
            case 2:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = true;
                this.starNode.children[2].children[0].active = false;

                break;
            case 3:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = true;
                this.starNode.children[2].children[0].active = true;
                break;
            default:
                break;
        }
    }

    // update (dt) {}
}
