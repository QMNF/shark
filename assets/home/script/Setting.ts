// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import StorageMag, { StorageKey } from "./manage/StorageMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Setting extends cc.Component {

    @property(cc.Node)
    voice: cc.Node = null
    @property(cc.Node)
    noVoice: cc.Node = null
    @property(cc.Node)
    shock: cc.Node = null
    @property(cc.Node)
    noShock: cc.Node = null

    onLoad() {
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
        console.error("voice3", StorageMag.Ins.getStorage(StorageKey.HaveVoice))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.voice.active = StorageMag.Ins.getStorage(StorageKey.HaveVoice) == 1
        this.noVoice.active = !this.voice.active
        this.shock.active = StorageMag.Ins.getStorage(StorageKey.HaveShock) == 1
        this.noShock.active = !this.shock.active
        this.voice.on(cc.Node.EventType.TOUCH_END, () => {
            this.voice.active = false;
            this.noVoice.active = true;
            StorageMag.Ins.freshHaveVoice(0);
            this.setVolume();
        })
        this.noVoice.on(cc.Node.EventType.TOUCH_END, () => {
            this.voice.active = true;
            this.noVoice.active = false;
            StorageMag.Ins.freshHaveVoice(1);
            this.setVolume();
        })
        this.shock.on(cc.Node.EventType.TOUCH_END, () => {
            this.shock.active = false;
            this.noShock.active = true;
            StorageMag.Ins.freshHaveShock(0);
        })
        this.noShock.on(cc.Node.EventType.TOUCH_END, () => {
            this.shock.active = true;
            this.noShock.active = false;
            StorageMag.Ins.freshHaveShock(1);
        })
    }

    setVolume() {
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
        console.error("voice2", StorageMag.Ins.getStorage(StorageKey.HaveVoice))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if (StorageMag.Ins.getStorage(StorageKey.HaveVoice)) {
            cc.audioEngine.setMusicVolume(0.8);
            cc.audioEngine.setEffectsVolume(0.8);
        } else {
            cc.audioEngine.setMusicVolume(0);
            cc.audioEngine.setEffectsVolume(0);
        }

    }

    start() {

    }

    update(dt) { }
}
