import ToolMag from "./manage/ToolMag";
import PoolMag from "./manage/PoolMag";
import GameMag from "./manage/GameMag";
import outline from "./outline";
import levelTip from "./levelTip";
import StorageMag, { StorageKey } from "./manage/StorageMag";
import SoundMag from "./manage/SoundMag";
import LevelClick from "./LevelClick";
import varyPage from "./varyPage";
import Currency from "./currency";
<<<<<<< HEAD
import Shark from "../../game/script/shark";
=======
<<<<<<< HEAD
import Shark from "../../game/script/shark";
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97


const { ccclass, property } = cc._decorator;

@ccclass
export default class HomeMain extends cc.Component {

    @property(cc.SpriteAtlas)
    atlas: cc.SpriteAtlas = null;
    @property(cc.Node)
    homeBg: cc.Node = null;
    @property({ type: cc.Node, tooltip: "大厅水流" })
    homeWater: cc.Node = null;
    @property(cc.Node)
    gameMain: cc.Node = null;
    @property(cc.Node)
    outline: cc.Node = null;
    @property({ type: cc.Node, tooltip: "游戏内的背景图" })
    background: cc.Node = null;
    @property({ type: cc.Node, tooltip: "关卡显示" })
    levelTip: cc.Node = null;
    @property({ type: cc.Node, tooltip: "关卡选择" })
    LevelClick: cc.Node = null;
    @property(cc.Node)
    startGame: cc.Node = null;
    @property(cc.Node)
    startLabel: cc.Node = null;
    @property(cc.Node)
    girl: cc.Node = null;
    @property(cc.Node)
    walls: cc.Node = null;
    @property(cc.Node)
    waters: cc.Node = null;
    @property(cc.Node)
    waterShades: cc.Node = null;
    @property(cc.Node)
    shark: cc.Node = null;
    @property(Shark)
    sharkInfo: Shark = null;
    @property(cc.Sprite)
    sharkGun: cc.Sprite = null;
    @property(cc.Node)
    currency: cc.Node = null;
    @property(cc.Node)
    settingNode: cc.Node = null;
    @property(cc.Node)
    setting: cc.Node = null;
    @property(cc.Node)
    ADpage: cc.Node = null;

    @property(cc.Node)
    levels: cc.Node = null;
    @property(cc.Node)
    costs: cc.Node = null;
    @property(cc.Node)
    attributeNum: cc.Node = null;

    @property(cc.AudioClip)
    homeBG: cc.AudioClip = null;
    @property(cc.AudioClip)
    makeBG: cc.AudioClip = null;
    @property(cc.AudioClip)
    varyBG: cc.AudioClip = null;

    @property(cc.Node)
    btns: cc.Node = null;
    @property(cc.Node)
    AdBtns: cc.Node = null;
    @property(cc.Label)
    varyNum: cc.Label = null;

    @property({ type: cc.Node, tooltip: "改造按钮" })
    makeBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "变异按钮" })
    varyBtn: cc.Node = null;
    @property({ type: cc.Node, tooltip: "改造界面" })
    makePage: cc.Node = null;
    @property({ type: cc.Node, tooltip: "变异界面" })
    varyPage: cc.Node = null;
<<<<<<< HEAD
    @property({ type: cc.Node, tooltip: "变异界面" })
    varyPageRedPoint: cc.Node = null;
=======
<<<<<<< HEAD
    @property({ type: cc.Node, tooltip: "变异界面" })
    varyPageRedPoint: cc.Node = null;
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97

    isHome: boolean = false;



    onLoad() {
        var self = this;
        this.gameMain.active = false;
        this.startGame.active = true;
        this.makePage.active = false;
        this.varyPage.active = false;
        this.settingNode.active = false
        this.homeBg.active = true;
        // PoolMag.Ins.initPools((success) => {
        //     if (!success) return;
        //     this.startGame.active = true;
        // });
        this.startGame.on(cc.Node.EventType.TOUCH_END, this.onSwitchLevel, this);
        this.makeBtn.on(cc.Node.EventType.TOUCH_END, () => {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10010")
            }
            this.makePage.active = true;
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            GameMag.Ins.isHomeIn = true;
            SoundMag.Ins.makeBG_ID = cc.audioEngine.playMusic(this.makeBG, true);
        }, this);
        this.varyBtn.on(cc.Node.EventType.TOUCH_END, () => {
            if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                // @ts-ignore
                wx.uma.trackEvent("10011")
            }
            this.varyPage.active = true;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.varyPageRedPoint.active = false
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            GameMag.Ins.isHomeIn = true;
<<<<<<< HEAD
=======
=======
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            GameMag.Ins.isHomeIn = true;
            this.varyPage.getComponent(varyPage).setmakeData();
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            SoundMag.Ins.makeBG_ID = cc.audioEngine.playMusic(this.varyBG, true);
        }, this)
        this.setting.on(cc.Node.EventType.TOUCH_END, () => { this.settingNode.active = true; }, this)
        this.settingNode.getChildByName("panel").on(cc.Node.EventType.TOUCH_END, () => { this.settingNode.active = false }, this)
        cc.game.on("isOutLine", this.isOutLine, this)
        cc.game.on("freshHomeShark", this.freshHomeShark, this);
        cc.game.on("initHomeUI", this.initHomeUI, this);
        cc.game.on("changeEnd", this.changeVaryNum, this);
        cc.game.on("onSwitchLevel", this.onSwitchLevel, this);
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        cc.game.on("varyInit", () => {
            self.varyPage.getComponent(varyPage).init(this.varyPage.getComponent(varyPage).setmakeData);
            self.schedule(this.setRedPoint, 1)
        }, this)
        if (cc.sys.platform === cc.sys.WECHAT_GAME) {
            // @ts-ignore
            wx.uma.trackEvent("10003")
        }
    }
<<<<<<< HEAD

    setRedPoint() {
        this.varyPageRedPoint.active = this.varyPage.getComponent(varyPage).isRedPoint();
    }

    changeVaryNum() {
=======
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250

    setRedPoint() {
        this.varyPageRedPoint.active = this.varyPage.getComponent(varyPage).isRedPoint();
    }

    changeVaryNum() {
        this.varyNum.string =
            (GameMag.Ins.sharkDiv
                + GameMag.Ins.sharkHp
                + GameMag.Ins.sharkPower
                + GameMag.Ins.sharkBitePower
            ) + ""
    }
    changeVaryNum() {
        console.error("更改变异指数")
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.varyNum.string =
            (GameMag.Ins.sharkDiv
                + GameMag.Ins.sharkHp
                + GameMag.Ins.sharkPower
                + GameMag.Ins.sharkBitePower
            ) + ""
    }
    isOutLine(time: number) {
        this.outline.getComponent(outline).show((time) / 60000);

    }
    freshHomeShark() {
        const using = StorageMag.Ins.getStorage(StorageKey.Using);
        const gunID = using ? using.gun : 0;
        this.sharkGun.spriteFrame = this.atlas.getSpriteFrame("gun" + gunID);
        GameMag.Ins.sharkChangeSkin(this.shark);
    }

    initHomeUI() {
        var self = this;
        // cc.audioEngine.stopMusic();
        SoundMag.Ins.homeBG_ID = cc.audioEngine.playMusic(this.homeBG, true)
        // cc.audioEngine.playMusic(SoundMag.Ins.homeBG, true)
        // let data = StorageMag.Ins.getStorage(StorageKey.Currency);
        GameMag.Ins.isgame = false;
        this.ADpage.active = false;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.varyPage.active = true;
        this.varyPage.active = false;
        this.currency.active = true;
        //this.currency.getChildByName("San").active = true;
        // this.currency.getChildByName("SanLab").active = true;
        this.currency.getComponent(Currency).init();
        this.homeWater.stopAllActions();
        this.initUpgradeUI();
        this.sharkInfo.loadShark()
<<<<<<< HEAD
=======
=======
        this.currency.active = true;
        this.currency.getChildByName("San").active = true;
        this.currency.getChildByName("SanLab").active = true;
        this.currency.getComponent(Currency).init();
        this.homeWater.stopAllActions();
        this.initUpgradeUI();
        var self = this;
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.levelTip.getComponent(levelTip).show(StorageMag.Ins.getStorage(StorageKey.MaxLevel))
        cc.game.emit("changeSharkData");
        cc.tween(this.homeWater)
            .then(cc.sequence(cc.moveBy(1, cc.v2(-84, 0)), cc.callFunc(() => {
                self.homeWater.x = 0
            })))
            .repeatForever()
            .start();
        this.background.children.forEach(item => {
            item.active = false;
        });
        const index = Math.floor(Math.random() * 3);
        // console.log(index);
        this.girl.active = index === 1;
        let node: cc.Node = null;
        const box = this.background.children[index];
        if (box.children.length === 0) {
            PoolMag.Ins.initBackground(index, (prefab: cc.Prefab) => {
                for (let i = 0; i < 3; i++) {
                    node = cc.instantiate(prefab);
<<<<<<< HEAD
                    // console.log(node)
=======
<<<<<<< HEAD
                    // console.log(node)
=======
                    console.log(node)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    node.x = node.width * (i + 1);
                    node.parent = box;
                }
                box.active = true;
            });
        } else {
            box.active = true;
        }
        // this.mlevelTip.show(StorageMag.Ins.getStorage(StorageKey.Level));
        this.freshHomeShark();
        const action = cc.sequence(
            cc.spawn(
                cc.fadeOut(1),
                cc.scaleTo(0.7, 1.2)
            ),
            cc.spawn(
                cc.fadeIn(1),
                cc.scaleTo(0.7, 1)
            )
        );
        cc.tween(this.startLabel)
            .then(action)
            .repeatForever()
            .start();
    }
    onEnable() {

    }
    initlevelTip() {

    }
    initUpgradeUI() {
        let data = StorageMag.Ins.getStorage(StorageKey.Skill);
        this.levels.children.forEach((node, i) => {
            node.getComponent(cc.Label).string = "等级：" + data[i].lv;
            let str = "";
            switch (i) {
                case 0:
                    str = "生命："
                    break;
                case 1:
                    str = "攻击："
                    break;
                case 2:
                    str = "防御："
                    break;
                case 3:
                    str = "离线："
                    break;
            }
            this.costs.children[i].getComponent(cc.Label).string = String(data[i].cost);
            this.attributeNum.children[i].getComponent(cc.Label).string = str + String(i === 3 ? Math.floor(data[i].attr * 100) + "%" : data[i].attr);
        });
    }
    upgradeBtn(e, index) {
        index = Number(index);
        let data = StorageMag.Ins.getStorage(StorageKey.Skill);
        let num = data[index].cost;
        cc.game.emit("addCoin", -num, (success) => {
            if (!success) return;
            cc.audioEngine.playEffect(SoundMag.Ins.getLvlUp(), false);
            if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
                ToolMag.Ins.vibrateShort();
            const res = StorageMag.Ins.freshSkillData(index);
<<<<<<< HEAD
            // console.log("freshSkillData", res);
=======
<<<<<<< HEAD
            // console.log("freshSkillData", res);
=======
            console.log("freshSkillData", res);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            let str = "";
            switch (index) {
                case 0:
                    str = "生命："
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10018")
                    }
                    break;
                case 1:
                    str = "攻击："
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10016")
                    }
                    break;
                case 2:
                    str = "防御："
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10017")
                    }
                    break;
                case 3:
                    str = "离线："
                    if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                        // @ts-ignore
                        wx.uma.trackEvent("10019")
                    }
<<<<<<< HEAD
=======
=======
                    break;
                case 1:
                    str = "攻击："
                    break;
                case 2:
                    str = "防御："
                    break;
                case 3:
                    str = "离线："
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    break;
            }
            ToolMag.Ins.showUpLight(this.shark.getChildByName("up_Light"));
            ToolMag.Ins.showUpLight(e.target);
            this.levels.children[index].getComponent(cc.Label).string = "等级：" + this.cashUI(res.lv);
            this.costs.children[index].getComponent(cc.Label).string = this.cashUI(res.cost);
            this.attributeNum.children[index].getComponent(cc.Label).string = str + String(index === 3 ? Math.floor(res.attr * 100) + "%" : this.cashUI(res.attr));
            cc.game.emit("changeSharkData");
        });
    }
    cashUI(num: number) {
        let str = "";
        let tmp = parseInt(num + "").toString();
        if (num > 10000) {
            str = "w";
            tmp = (num / 10000).toFixed(1);
        }
        return tmp + str;
    }
    Joingame() {
        // console.log("进入游戏")
        GameMag.Ins.gamePause = false;
        GameMag.Ins.gameOver = false;
        this.walls.active = true;
        this.waters.active = true;
        this.waterShades.active = true;
        this.startLabel.stopAllActions();
        this.startLabel.opacity = 255
        cc.game.emit("changeSharkData");
        this.gameMain.active = true;
        this.LevelClick.active = false;
        this.homeBg.active = false;
        this.currency.active = false;
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        //this.currency.getChildByName("San").active = false;
        // this.currency.getChildByName("SanLab").active = false;
        if (cc.sys.platform === cc.sys.WECHAT_GAME) {
            // @ts-ignore
            wx.uma.trackEvent("10004",{level:StorageMag.Ins.getStorage(StorageKey.Level)})
        }
<<<<<<< HEAD
        GameMag.Ins.isgame = true;
=======
        GameMag.Ins.isgame = true;
=======
        this.currency.getChildByName("San").active = false;
        this.currency.getChildByName("SanLab").active = false;
        GameMag.Ins.isgame = true;
        console.error(SoundMag.Ins.getGameBG(SoundMag.Ins.getGameBGNum() - 1))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        SoundMag.Ins.gameBG_ID = cc.audioEngine.playMusic(SoundMag.Ins.getGameBG(SoundMag.Ins.getGameBGNum() - 1), true)
    }
    onSwitchLevel() {
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
<<<<<<< HEAD
        if (cc.audioEngine.getState(SoundMag.Ins.homeBG_ID) != cc.audioEngine.AudioState.PLAYING) {
            SoundMag.Ins.homeBG_ID = cc.audioEngine.playMusic(this.homeBG, true)
        }
=======
<<<<<<< HEAD
        if (cc.audioEngine.getState(SoundMag.Ins.homeBG_ID) != cc.audioEngine.AudioState.PLAYING) {
            SoundMag.Ins.homeBG_ID = cc.audioEngine.playMusic(this.homeBG, true)
        }
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.LevelClick.active = true;
        this.LevelClick.getComponent(LevelClick).show();
    }

    update(dt) {
        let data = StorageMag.Ins.getStorage(StorageKey.Skill);
        let coin = StorageMag.Ins.getStorage(StorageKey.Currency).coin;
        data.forEach((skill, i) => {
            if (skill.cost > coin) {
                this.btns.children[i].getComponent(cc.Button).interactable = false;
                this.AdBtns.children[i].active = true;
            } else {
                this.btns.children[i].getComponent(cc.Button).interactable = true;
                this.AdBtns.children[i].active = false;
            }
        })
    }
}
