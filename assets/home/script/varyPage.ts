// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameMag from "./manage/GameMag";
import SoundMag from "./manage/SoundMag";
import StorageMag, { StorageKey } from "./manage/StorageMag";
import ToolMag from "./manage/ToolMag";
import varyItem from "./varyItem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class varyPage extends cc.Component {

    @property(cc.ScrollView)
    scroll: cc.ScrollView = null
    @property(cc.Label)
    partName: cc.Label = null
    @property(cc.Label)
    dpsLab: cc.Label = null
    @property(cc.Label)
    hpLab: cc.Label = null
    @property(cc.Label)
    divLab: cc.Label = null
    @property({ type: cc.Node, tooltip: "部件信息栏" })
    partInfo: cc.Node = null
    @property(cc.Label)
    skillName: cc.Label = null
    @property(cc.Label)
    skillInfo: cc.Label = null
    @property(cc.Sprite)
    skillIcon: cc.Sprite = null
    @property(cc.Label)
    addLab: cc.Label = null
    @property(cc.Label)
    unLockLab: cc.Label = null
    @property(cc.Node)
    getBtn: cc.Node = null
    @property(cc.Node)
    unlockBtn: cc.Node = null
    @property(cc.Node)
    starNode: cc.Node = null;
    @property(cc.Node)
    paopao: cc.Node = null;
    @property(cc.Node)
    noUnLock: cc.Node = null;
    @property(cc.Sprite)
    part_icon: cc.Sprite = null;

    @property(cc.Node)
    unLockWin: cc.Node = null;

    @property(cc.Node)
    backBtn: cc.Node = null
    @property(cc.Prefab)
    varyItem: cc.Prefab = null;
    @property(cc.SpriteAtlas)
    imgList: cc.SpriteAtlas = null;
    @property(cc.SpriteAtlas)
    imghome: cc.SpriteAtlas = null;
    @property(cc.SpriteAtlas)
    sharkpart: cc.SpriteAtlas = null;

    click_item: cc.Node;


    // LIFE-CYCLE CALLBACKS:

    nowIndex: number = null;
    lastIndex: number = null;
    varyData: any[] = [[], [], [], [], []]
    data = null;
    getedInfo = null;

    onLoad() {
        this.backBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("freshHomeShark");
            if (GameMag.Ins.isHomeIn) {
                cc.game.emit("initHomeUI")
            } else {
                cc.game.emit("onSwitchLevel");
            }
            this.node.active = false;
            // this.scroll.content.removeAllChildren();
        }, this);
        ToolMag.Ins.playDragonBone(this.paopao, "animation", 0)
        this.getBtn.on(cc.Node.EventType.TOUCH_END, this.addStar, this);
        this.unlockBtn.on(cc.Node.EventType.TOUCH_END, this.unlock, this);
        this.partInfo.active = false;
        cc.game.on("UnlockRet", (index, ok) => {
            if (ok) return
            this.noUnLock.active = true;
            this.noUnLock.stopAllActions();
            cc.tween(this.noUnLock)
                .delay(2.0)
                .to(0.5, { opacity: 0 })
                .call(() => {
                    this.noUnLock.active = false
                    this.noUnLock.opacity = 255
                })
                .start();
        })
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        cc.game.emit("varyInit");
    }

    setmakeData() {
        this.scroll.content.removeAllChildren();
<<<<<<< HEAD
=======
=======
    }

    setmakeData() {
        // this.scroll.content.removeAllChildren();
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.scroll.scrollToPercentVertical(1)
        let usingData = StorageMag.Ins.getStorage(StorageKey.Using);
        for (let i = 0; i < this.varyData.length; i++) {
            let item = cc.instantiate(this.varyItem);
            item.getComponent(varyItem).setInfo(this.varyData[i]);
            this.scroll.content.addChild(item);
            item.x = 0
            item.children.forEach((icon, index) => {
                if (icon.name != "bg" && icon.name != "point" && icon.name != "labNode" && icon.name != "unlock") {
                    icon.getChildByName("light").active = false
                    icon.on(cc.Node.EventType.TOUCH_END, this.clickItem, this);
                }
            })
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
            // if (i == usingData.gun) {
            //     item.getChildByName("gun").getChildByName("light").active = true
            // }
            // if (i == usingData.yushen) {
            //     item.getChildByName("body").getChildByName("light").active = true
            // }
            // if (i == usingData.yuzui) {
            //     item.getChildByName("head").getChildByName("light").active = true
            // }
            // if (i == usingData.yuqi) {
            //     item.getChildByName("fins").getChildByName("light").active = true
            // }
            // if (i == usingData.yuwei) {
            //     item.getChildByName("tail").getChildByName("light").active = true
            // }
            // if (i == usingData.futou) {
            //     item.getChildByName("knife").getChildByName("light").active = true
            // }
            // item.on(cc.Node.EventType.TOUCH_END, this.itemClick, this)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        }
        this.clickItem({ target: this.click_item ? this.click_item : this.scroll.content.children[0].getChildByName("head") })
    }

    getData() {
        this.varyData = [[], [], [], [], []]
        this.getedInfo = StorageMag.Ins.getStorage(StorageKey.Get);
        for (let i = 0; i < this.getedInfo.length; i++) {
            for (let j = 0; j < this.getedInfo[i].length; j++) {
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 2) {
                    this.varyData[0].push({
                        data: this.getedInfo[i][j],
                        type: "警长"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 3) {
                    this.varyData[1].push({
                        data: this.getedInfo[i][j],
                        type: "狙击手"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 4) {
                    this.varyData[2].push({
                        data: this.getedInfo[i][j],
                        type: "武士刀"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 5) {
                    this.varyData[3].push({
                        data: this.getedInfo[i][j],
                        type: "总督"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 6) {
                    this.varyData[4].push({
                        data: this.getedInfo[i][j],
                        type: "皮衣先生"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 7) {
                    this.varyData[5].push({
                        data: this.getedInfo[i][j],
                        type: "魂斗罗"
                    });
                }
                if (Math.floor(this.getedInfo[i][j].ID / 1000) == 8) {
                    this.varyData[6].push({
                        data: this.getedInfo[i][j],
                        type: "小丑"
                    });
                }
            }
        }
        // console.error("varyData", this.varyData);
        return true;
    }

    isRedPoint() {
        for (let i = 0; i < this.varyData.length; i++) {
            for (let j = 0; j < this.varyData[i].length; j++) {
                if (this.varyData[i][0].data.Allgeted) {
                    if (this.varyData[i][j].data.geted) {
                        if (this.varyData[i][j].data.star < 3) {
                            if (this.varyData[i][j].data.addStarDO <= StorageMag.Ins.getStorage(StorageKey.Currency).diamond) {
                                return true;
                            }
                        }
                    } else {
                        if (this.varyData[i][j].data.unLockPartDO && this.varyData[i][j].data.unLockPartDO <= StorageMag.Ins.getStorage(StorageKey.Currency).diamond) {
                            return true;
                        }
                    }
                } else {
                    if (this.varyData[i][0].data.unlockAllDO && this.varyData[i][0].data.unlockAllDO <= StorageMag.Ins.getStorage(StorageKey.Currency).diamond) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    onEnable() {
        this.lastIndex = 0;
        // GameMag.Ins.sharkChangeSkin(this.shark);
    }

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    init(callback: Function) {
        if (this.getData()) {
            this.scheduleOnce(callback, 0)
        }

<<<<<<< HEAD
=======
=======
    init() {
        this.getData();
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        // this.setmakeData();
    }

    clickItem(e) {
        if (e.target.name == "open") return;
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
<<<<<<< HEAD
        // console.log(e.target)
=======
<<<<<<< HEAD
        // console.log(e.target)
=======
        console.log(e.target)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.click_item = e.target;
        // e.target.parent.getComponent(varyItem).setClick(this.click_item)
        cc.game.emit('setClick', this.click_item.parent, this.click_item)
        let tmp = (e.target as cc.Node).parent.getComponent(varyItem).data;
        let data = null;
        for (let key in tmp) {
            if (e.target.name == "head") {
                if (tmp[key].data.ID % 1000 == 1)
                    data = tmp[key];
            }
            if (e.target.name == "body") {
                if (tmp[key].data.ID % 1000 == 2)
                    data = tmp[key];
            }
            if (e.target.name == "fins") {
                if (tmp[key].data.ID % 1000 == 3)
                    data = tmp[key];
            }
            if (e.target.name == "tail") {
                if (tmp[key].data.ID % 1000 == 4)
                    data = tmp[key];
            }
            if (e.target.name == "gun") {
                if (tmp[key].data.ID % 1000 == 5)
                    data = tmp[key];
            }
            if (e.target.name == "knife") {
                if (tmp[key].data.ID % 1000 == 6)
                    data = tmp[key];
            }
        }
        this.partInfo.active = true;
        this.setPartInfo(data.data);
    }

    start() {

    }

    setPartInfo(data) {
        this.data = data;
        // console.error("setPartInfo", this.data);
        this.partName.string = data.name;
        this.dpsLab.string = "+" + (data.power * data.star == 0 ? data.power : data.power * data.star)
        this.divLab.string = "+" + (data.div * data.star == 0 ? data.div : data.div * data.star)
        this.hpLab.string = "+" + (data.hp * data.star == 0 ? data.hp : data.hp * data.star)
        this.unlockBtn.active = data.star == 0
        this.getBtn.active = !(data.star == 3)
        this.addLab.node.active = !this.unlockBtn.active && !(data.star == 3)
        this.unLockLab.node.active = this.unlockBtn.active
        this.unLockLab.string = "x" + this.data.unLockPartDO
        this.addLab.string = "x" + this.data.addStarDO
        this.addLab.node.color = this.data.addStarDO > StorageMag.Ins.getStorage(StorageKey.Currency).diamond ? cc.color(255, 0, 19) : cc.color(255, 255, 255)
        this.unLockLab.node.color = this.data.unLockPartDO > StorageMag.Ins.getStorage(StorageKey.Currency).diamond ? cc.color(255, 0, 19) : cc.color(255, 255, 255)
        this.skillName.string = "【" + data.skill + "】"
        this.skillInfo.string = this.setskillInfo(data)
        this.skillIcon.spriteFrame = this.imghome.getSpriteFrame(data.skill == "手雷" || data.skill == "治疗" ? data.skill + "1" : data.skill);
        if (data.ID % 1000 == 3) {
            this.part_icon.node.scale = 1.2
        } else if (data.ID % 1000 == 4) {
            this.part_icon.node.scale = 1.2
        } else if (data.ID % 1000 == 5) {
            this.part_icon.node.scale = 1.5
        } else if (data.ID % 1000 == 1) {
            this.part_icon.node.scale = 1.2
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        } else if (data.ID % 1000 == 2) {
            this.part_icon.node.scale = 1
        }
        else {
            this.part_icon.node.scale = 0.5
<<<<<<< HEAD
=======
=======
        }
        else {
            this.part_icon.node.scale = 1.0
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        }
        this.part_icon.spriteFrame = this.sharkpart.getSpriteFrame(this.data.img)
        this.setStarNum(data.star);
    }

    setskillInfo(data) {
        switch (data.skill) {
            case "强力":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}攻击`
            case "手雷":
                return `发射炮弹，造成${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}伤害`
            case "治疗":
                return `使用后恢复${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}生命`
            case "健体":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}生命`
            case "坚固":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}防御`
            case "锋利":
                return `装配后增加${data.skillPower * data.star == 0 ? data.skillPower : data.skillPower * data.star}攻击`
            default:
                return "";
        }
    }

    addStar(e: cc.Event.EventTouch) {
<<<<<<< HEAD
        // console.error(e.target.name);
=======
<<<<<<< HEAD
        // console.error(e.target.name);
=======
        console.error(e.target.name);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        if (e.target.name == "unlockBtn") return
        if (this.data.star >= 3) return;
        cc.game.emit("addDiamond", -this.data.addStarDO, (success) => {
            if (success) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                if (cc.sys.platform === cc.sys.WECHAT_GAME) {
                    // @ts-ignore
                    wx.uma.trackEvent("10015")
                }
                cc.audioEngine.playEffect(SoundMag.Ins.getUpStar(), false);
<<<<<<< HEAD
                StorageMag.Ins.freshGetData(this.data.name, this.data.geted, this.data.star + 1)
                ToolMag.Ins.showUpLight(this.part_icon.node);
=======
                StorageMag.Ins.freshGetData(this.data.name, this.data.geted, this.data.star + 1)
                ToolMag.Ins.showUpLight(this.part_icon.node);
=======
                cc.audioEngine.playEffect(SoundMag.Ins.getUpStar(), false);
                StorageMag.Ins.freshGetData(this.data.name, this.data.geted, this.data.star + 1)
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                ToolMag.Ins.showUpLight(this.click_item);
                this.getData();
                this.setPartInfo(this.data);
                this.setUsingInfo();
                this.click_item.parent.emit("addstar", this.click_item, this.data.star);
                cc.game.emit("changeSharkData");
            } else {
                console.error("失败")
            }
        });
    }

    unlock() {
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        if (this.data.star > 0) {
            return;
        }
        cc.game.emit("addDiamond", -this.data.unLockPartDO, (success) => {
            if (success) {
                this.data.geted = true;
                StorageMag.Ins.freshGetData(this.data.name, this.data.geted, this.data.star + 1)
                this.getData();
                this.setPartInfo(this.data);
                this.showunLockWin();
                this.click_item.parent.emit("addstar", this.click_item, this.data.star);
            } else {
                console.error("失败")
            }
        });
    }

    showunLockWin() {
<<<<<<< HEAD
        cc.audioEngine.playEffect(SoundMag.Ins.getunLockPart(), false);
=======
<<<<<<< HEAD
        cc.audioEngine.playEffect(SoundMag.Ins.getunLockPart(), false);
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.unLockWin.active = true;
        this.unLockWin.getChildByName("light").stopAllActions();
        this.unLockWin.getChildByName("partName").getComponent(cc.Label).string = this.data.name;
        this.unLockWin.getChildByName("part_icon").getComponent(cc.Sprite).spriteFrame = this.sharkpart.getSpriteFrame(this.data.img)
        if (this.data.ID % 1000 == 3) {
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            this.unLockWin.getChildByName("part_icon").scale = 2.5
        } else if (this.data.ID % 1000 == 4) {
            this.unLockWin.getChildByName("part_icon").scale = 1.8
        } else if (this.data.ID % 1000 == 5) {
            this.unLockWin.getChildByName("part_icon").scale = 2
        } else if (this.data.ID % 1000 == 1) {
            this.unLockWin.getChildByName("part_icon").scale = 1.8
        } else if (this.data.ID % 1000 == 2) {
            this.unLockWin.getChildByName("part_icon").scale = 1.5
        }
        else {
            this.unLockWin.getChildByName("part_icon").scale = 0.8
        }
        cc.tween(this.unLockWin.getChildByName("light"))
            .then(cc.rotateBy(5, 360))
<<<<<<< HEAD
=======
=======
            this.unLockWin.getChildByName("part_icon").scale = 1.2
        } else if (this.data.ID % 1000 == 4) {
            this.unLockWin.getChildByName("part_icon").scale = 1.2
        } else if (this.data.ID % 1000 == 5) {
            this.unLockWin.getChildByName("part_icon").scale = 1.5
        } else if (this.data.ID % 1000 == 1) {
            this.unLockWin.getChildByName("part_icon").scale = 1.2
        }
        else {
            this.unLockWin.getChildByName("part_icon").scale = 1.0
        }
        cc.tween(this.unLockWin.getChildByName("light"))
            .then(cc.rotateBy(1.5, 360))
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
            .repeatForever()
            .start();
    }

    closeUnLockWin() {
        this.unLockWin.active = false;
    }

    setStarNum(Num: number) {
        switch (Num) {
            case 1:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = false;
                this.starNode.children[2].children[0].active = false;

                break;
            case 2:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = true;
                this.starNode.children[2].children[0].active = false;

                break;
            case 3:
                this.starNode.children[0].children[0].active = true;
                this.starNode.children[1].children[0].active = true;
                this.starNode.children[2].children[0].active = true;
                break;
            default:
                this.starNode.children[0].children[0].active = false;
                this.starNode.children[1].children[0].active = false;
                this.starNode.children[2].children[0].active = false;
                break;
        }
    }

    setUsingInfo() {
        let getData = StorageMag.Ins.getStorage(StorageKey.Get);
        let usingData = StorageMag.Ins.getStorage(StorageKey.Using);
        usingData.gunStar = getData[0][usingData.gun].star
        usingData.yushenStar = getData[1][usingData.yushen].star
        usingData.yuqiStar = getData[2][usingData.yuqi].star
        usingData.yuweiStar = getData[3][usingData.yuwei].star
        usingData.yuzuiStar = getData[4][usingData.yuzui].star
        usingData.futouStar = getData[5][usingData.futou].star
<<<<<<< HEAD
=======
<<<<<<< HEAD
=======
        console.error(usingData, getData);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        StorageMag.Ins.freshUsingData(usingData);
    }
}
