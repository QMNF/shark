// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameMag from "./manage/GameMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class levelTip extends cc.Component {

    @property(cc.Label)
    levelName: cc.Label = null;
    @property(cc.Label)
    LevelNum: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() { }

    show(level) {
        this.levelName.string = (Math.floor(level / 10) < level / 10 ? Math.floor(level / 10) + 1 : Math.floor(level / 10)) + " " + GameMag.Ins.levelList[Math.floor(level / 10) < level / 10 ? Math.floor(level / 10) : Math.floor(level / 10) - 1].name;
        this.LevelNum.string = `最高关卡：${level % 10 == 0 ? 10 : level % 10}/10`
    }
}
