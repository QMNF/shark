import StorageMag, { StorageKey } from "./StorageMag";
import PoolMag from "./PoolMag";
import ToolMag from "./ToolMag";

/**
 * tag 列表
 * 1 鲨鱼身体(判断被攻击)
 * 2 鲨鱼子弹
 * 
 * 10 boss子弹
 * 11 香蕉船
 * 12 boss身体
 * 13 直升机身体(撕咬用)
 */

/**body身体/chin鱼嘴/Fins鱼鳍/tail鱼尾 */
export enum SharkSkin {
    Gun = "gun",
    Body = "body",
    Chin = "chin",
    Fins = "Fins",
    Tail = "tail",
    Maxilla = "Maxilla"
}

export enum Enemys {
    E1009,
    E1010,
    E1011,
    E1012,
    E1013,
    E1014,
    E1015,
    E1016,
    E1017,
    E1018,
    E1019
}

export enum EnemyAction {
    Stay = "stay",
    Hurt = "injured",
    Die = "die",
    Fire = "fire",
    Swimming = "Swimming",
    Attack0 = "attack_0",
    Attack1 = "attack_1",
    Attack2 = "attack_2",
    Attack3 = "attack_3"
}


const { ccclass, property } = cc._decorator;

@ccclass
export default class GameMag extends cc.Component {

    private static _instance: GameMag = null;
    public static get Ins(): GameMag {
        if (!this._instance || this._instance == null) {
            this._instance = new GameMag();
        }
        return this._instance;
    }

<<<<<<< HEAD
    sharkJson: any[] = [[], [], [], [], [], []];
=======
<<<<<<< HEAD
    sharkJson: any[] = [[], [], [], [], [], []];
=======
    sharkJson: any[] = [[], [], [], [], [],[]];
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    enemyJson: any[] = [];
    levelJson: any[] = [];
    levelList: any[] = [];
    gradeJson: any[] = [];

    /**是否在游戏中 */
    isgame: boolean = false;
    /**游戏暂停 */
    gamePause: boolean = false;
    /**游戏结束 */
    gameOver: boolean = false;
    /**游戏加载 */
    isloading: boolean = true;
    /**初次复活 */
    isReLive: boolean = false;
    isHomeIn: boolean = true;
<<<<<<< HEAD
=======

    isHomeIn: boolean = true;
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97

    airPosY: number = 180;//出现在天空的初始位置
    groundPosY: number = 122;//出现在地面的初始位置
    waterPosY: number = 20;//出现在水面的初始位置

    /**鲨鱼是否正跳在空中 */
    sharkJumping: boolean = false;
    /**鲨鱼是否正在咬住飞机 */
    sharkBiting: boolean = false;

    /**当前关卡已消灭的敌人数 */
    killNum: number = 0;
    /** 需要消灭的敌人数,是通关的标记*/
    needKillNum: number = 0;

    /**鲨鱼进入游戏的技能属性 */
    sharkHp: number = 0;
    gameHp: number = 0;
    sharkDiv: number = 0;
    sharkPower: number = 0;
    sharkBitePower: number = 0;
    ActiveSkill: any[] = [];
    PassiveSkills: any[] = [];

<<<<<<< HEAD
    loadJsons(cb, target) {
=======
<<<<<<< HEAD
    loadJsons(cb, target) {
=======
    loadJsons(cb,target) {
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        let num = 5
        cc.resources.load("./json/Level", cc.JsonAsset, (err, res: cc.JsonAsset) => {
            if (err) {
                console.error("json加载Level", err);
                return;
            }
            console.log("Level", res.json)
            for (const key in res.json) {
                let arr = [];
                // console.log("精怪数量", res.json[key].精怪数量);
                if (res.json[key]["BOSS1 ID"] != 1000) {
                    arr.push(res.json[key]["BOSS1 ID"])
                }
                if (res.json[key]["BOSS2 ID"] != 1000) {
                    arr.push(res.json[key]["BOSS2 ID"])
                }
                if (res.json[key]["BOSS3 ID"] != 1000) {
                    arr.push(res.json[key]["BOSS3 ID"])
                }
                if (res.json[key]["BOSS4 ID"] != 1000) {
                    arr.push(res.json[key]["BOSS4 ID"])
                }
                if (res.json[key]["BOSS5 ID"] != 1000) {
                    arr.push(res.json[key]["BOSS5 ID"])
                }
                this.levelJson.push({
                    nums: Number(key),
                    eliteNum: Number(res.json[key].精怪数量),
                    bossArr: arr,
                    personNum: Number(res.json[key]["personNum"]),
                    winIcon: Number(res.json[key]["winIcon"]),
                    winOD: Number(res.json[key]["winOD"])
                });
                // if (typeof (res.json[key].boss) === "number") {
                //     this.levelJson.push({
                //         nums: res.json[key].nums,
                //         bossArr: res.json[key].boss == 0 ? null : [res.json[key].boss]
                //     });
                // } else {
                //     this.levelJson.push({
                //         nums: res.json[key].nums,
                //         bossArr: res.json[key].boss.split("、")
                //     });
                // }
            }
            num--;
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log(this.levelJson);
=======
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log(this.levelJson);
=======
            if (cb) cb(num,target);
            console.log(this.levelJson);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        });
        cc.resources.load("./json/levelList", cc.JsonAsset, (err, res: cc.JsonAsset) => {
            if (err) {
                console.error("json加载levelList", err);
                return;
            }
            // console.log("levelList", res.json)
            for (const key in res.json) {
                this.levelList.push({
                    name: key.toString(),
                    num: res.json[key].关卡数
                });
            }
            num--;
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("levelList", this.levelList);
=======
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("levelList", this.levelList);
=======
            if (cb) cb(num,target);
            console.log("levelList", this.levelList);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        });
        cc.resources.load("./json/Shark", cc.JsonAsset, (err, res: cc.JsonAsset) => {
            if (err) {
                console.error("json加载Shark", err);
                return;
            }
            // console.log("Shark", res.json)
            for (const key in res.json) {
                if (res.json[key].type === "枪械") {
                    this.sharkJson[0].push(res.json[key]);
                    continue;
                }
                if (res.json[key].type === "身") {
                    this.sharkJson[1].push(res.json[key]);
                    continue;
                }
                if (res.json[key].type === "鳍") {
                    this.sharkJson[2].push(res.json[key]);
                    continue;
                }
                if (res.json[key].type === "尾") {
                    this.sharkJson[3].push(res.json[key]);
                    continue;
                }
                if (res.json[key].type === "头") {
                    this.sharkJson[4].push(res.json[key]);
                    continue;
                }
<<<<<<< HEAD
                if (res.json[key].type === "器械") {
=======
<<<<<<< HEAD
                if (res.json[key].type === "器械") {
=======
                if(res.json[key].type === "器械"){
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
                    this.sharkJson[5].push(res.json[key]);
                    continue;
                }
            }
            num--;
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("sharkJson", this.sharkJson);
=======
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("sharkJson", this.sharkJson);
=======
            if (cb) cb(num,target);
            console.log("sharkJson", this.sharkJson);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        });
        cc.resources.load("./json/Enemy", cc.JsonAsset, (err, res: cc.JsonAsset) => {
            if (err) {
                console.error("json加载Enemy", err);
                return;
            }
            // console.log("Enemy", res.json)
            for (const key in res.json) {
                this.enemyJson.push(res.json[key]);
            }
            num--;
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("enemyJson", this.enemyJson);
=======
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("enemyJson", this.enemyJson);
=======
            if (cb) cb(num,target);
            console.log("enemyJson", this.enemyJson);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        });
        cc.resources.load("./json/Upgrade", cc.JsonAsset, (err, res: cc.JsonAsset) => {
            if (err) {
                console.error("json加载Upgrade", err);
                return;
            }
            // console.log("Upgrade", res.json)
            for (const key in res.json) {
                this.gradeJson.push(res.json[key]);
            }
            num--;
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("UpgradeJson", this.gradeJson);
=======
<<<<<<< HEAD
            if (cb) cb(num, target);
            // console.log("UpgradeJson", this.gradeJson);
=======
            if (cb) cb(num,target);
            console.log("UpgradeJson", this.gradeJson);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        })
    };

    /**鲨鱼整体换肤 */
    sharkChangeSkin(target: cc.Node) {
        const data = StorageMag.Ins.getStorage(StorageKey.Using);
        const armature = target.getComponent(dragonBones.ArmatureDisplay).armature();
        const slotArr: string[] = ["body", "chin", "Maxilla", "Fins", "tail"];
        const dataArr: string[] = data ? [data.yushen, data.yuzui, data.yuzui, data.yuqi, data.yuwei] : [0, 0, 0, 0, 0];
        for (let i = 0; i < slotArr.length; i++) {
            armature.getSlot(slotArr[i]).displayIndex = dataArr[i];
        };
    }
    /**鲨鱼单部件换肤 */
    sharkSingleSkin(target: cc.Node, sharkPart: SharkSkin, index: number) {
        const armature = target.getComponent(dragonBones.ArmatureDisplay).armature();
        armature.getSlot(sharkPart).displayIndex = index;
    }
    /**显示爆炸龙骨动效 */
    showBoom(target: cc.Node) {
        const boom = PoolMag.Ins.getBoomNode();
        boom.parent = cc.find("Canvas");
        boom.position = target.position;
        GameMag.Ins.killNum++;
        // console.log(GameMag.Ins.killNum, GameMag.Ins.needKillNum);
        ToolMag.Ins.playDragonBone(boom, "newAnimation", 1, () => {
            PoolMag.Ins.putBoomNode(boom);
        });
    }
}
