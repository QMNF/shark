import GameMag from "./GameMag";

export enum PoolType {
    BG,
    Water,
    WaterShade,
    Wall
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class PoolMag extends cc.Component {

    private static _instance: PoolMag = null;
    public static get Ins(): PoolMag {
        if (!this._instance || this._instance == null) {
            this._instance = new PoolMag();
        }
        return this._instance;
    }

    //游戏滚动资源相关
    pool: cc.NodePool = null;
    poolNode: cc.Node = null;
    poolPrefab: cc.Prefab = null;
    waterPool: cc.NodePool = null;
    waterShadePool: cc.NodePool = null;
    wallPool: cc.NodePool = null;
    waterPrefab: cc.Prefab = null;
    waterShadePrefab: cc.Prefab = null;
    wallPrefab: cc.Prefab = null;

    //游戏资源相关
    enemysPool: any[] = [];
    bulletPool: cc.NodePool = null;
    bossBulletPool: cc.NodePool = null;
    domPool: cc.NodePool = null;
    personPool: cc.NodePool = null;
    boatPool: cc.NodePool = null;
    boomPool: cc.NodePool = null;
    coinPool: cc.NodePool = null;
    bloodPool: cc.NodePool = null;
    UpPool: cc.NodePool = null;
    

    bulletPrefab: cc.Prefab = null;
    bossBulletPrefab: cc.Prefab = null;
    domPrefab: cc.Prefab = null;
    personPrefab: cc.Prefab = null;
    boatPrefab: cc.Prefab = null;
    boomPrefab: cc.Prefab = null;
    coinPrefab: cc.Prefab = null;
    bloodPrefab: cc.Prefab = null;
    UpPrefab: cc.Prefab = null;

    loadTimes: number = 100;
    loadCallBack: Function = null;

    initPools(cb: Function) {
        this.loadCallBack = cb;
        this.waterPool = new cc.NodePool();
        this.waterShadePool = new cc.NodePool();
        this.wallPool = new cc.NodePool();
        this.bulletPool = new cc.NodePool();
        this.bossBulletPool = new cc.NodePool();
        this.domPool = new cc.NodePool();
        this.personPool = new cc.NodePool();
        this.boatPool = new cc.NodePool();
        this.boomPool = new cc.NodePool();
        this.coinPool = new cc.NodePool();
        this.bloodPool = new cc.NodePool();
        this.UpPool = new cc.NodePool();

        this.loadTimes = 7;
        this.loadWaterPool();
        this.loadWaterShadePool();
        this.loadWallPool();
        this.loadBulletPool();
        this.loadPersonPool();
        this.loadEnemysPool(GameMag.Ins.enemyJson)
        this.loadBossBulletPool();
        this.loadDomPool();
        this.loadBloodPool();
        this.loadUpPool();
<<<<<<< HEAD
        // console.error("this.loadTimes", this.loadTimes);
=======
<<<<<<< HEAD
        // console.error("this.loadTimes", this.loadTimes);
=======
        console.error("this.loadTimes", this.loadTimes);
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.schedule(this.loadTimer, 0.2);
        cc.resources.load("boom", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("boom=>err", err);
                return;
            }
            this.boomPrefab = prefab;
            this.boomPool.put(cc.instantiate(prefab));
        });
        cc.resources.load("gameCoin", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("gameCoin=>err", err);
                return;
            }
            this.coinPrefab = prefab;
            this.coinPool.put(cc.instantiate(prefab));
        });
    }
    loadTimer() {
        // console.log("loadTimes", this.loadTimes);
        if (this.loadTimes <= 0) {
            this.loadCallBack(true);
            this.unschedule(this.loadTimer);
        }
    }
    loadWaterPool() {
        cc.resources.load("water", cc.Prefab, (err, waterPrefab: cc.Prefab) => {
            if (err) {
                console.error("waters=>err", err);
                return;
            }
            this.waterPrefab = waterPrefab;
            this.waterPool.put(cc.instantiate(waterPrefab));
            this.loadTimes--;
        });
    }
    loadUpPool() {
        cc.resources.load("up_Light", cc.Prefab, (err, upPrefab: cc.Prefab) => {
            if (err) {
                console.error("up_Light=>err", err);
                return;
            }
            this.UpPrefab = upPrefab;
            this.UpPool.put(cc.instantiate(upPrefab));
            // this.loadTimes--;
        });
    }
    loadWaterShadePool() {
        cc.resources.load("waterShade", cc.Prefab, (err, waterShadePrefab: cc.Prefab) => {
            if (err) {
                console.error("waterShade=>err", err);
                return;
            }
            this.waterShadePrefab = waterShadePrefab;
            this.waterShadePool.put(cc.instantiate(waterShadePrefab));
            this.loadTimes--;
        });
    }
    loadWallPool() {
        cc.resources.load("wall", cc.Prefab, (err, wallPrefab: cc.Prefab) => {
            if (err) {
                console.error("wall=>err", err);
                return;
            }
            this.wallPrefab = wallPrefab;
            this.wallPool.put(cc.instantiate(wallPrefab));
            this.loadTimes--;
        });
    }
    loadBulletPool() {
        cc.resources.load("bullet", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("bullet=>err", err);
                return;
            }
            this.bulletPrefab = prefab;
            for (let i = 0; i < 5; i++) {
                this.bulletPool.put(cc.instantiate(prefab));
            }
            this.loadTimes--;
        });
    }
    loadDomPool() {
        cc.resources.load("dom", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("dom=>err", err);
                return;
            }
            this.domPrefab = prefab;
            for (let i = 0; i < 5; i++) {
                this.domPool.put(cc.instantiate(prefab));
            }
            this.loadTimes--;
        });
    }
    loadBloodPool() {
        cc.resources.load("blood", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("blood=>err", err);
                return;
            }
            this.bloodPrefab = prefab;
            for (let i = 0; i < 5; i++) {
                this.bloodPool.put(cc.instantiate(prefab));
            }
            this.loadTimes--;
        });
    }
    loadPersonPool() {
        cc.resources.load("person", cc.Prefab, (err, prefab: cc.Prefab) => {
            if (err) {
                console.error("person=>err", err);
                return;
            }
            this.personPrefab = prefab;
            for (let i = 0; i < 4; i++) {
                this.personPool.put(cc.instantiate(prefab));
            }
            this.loadTimes--;
        });
    }
    loadEnemysPool(enemyJson: any[]) {
        for (let i = 1; i < enemyJson.length + 1; i++) {
            this.enemysPool.push({
                pool: new cc.NodePool(),
                prefab: null
            })
        }
    }
    bgPoolArr: any[] = [];
    bgIndex: number = 0;
    initBackground(index: number, cb: Function) {
        this.bgIndex = index;
        if (this.bgPoolArr.length === 0) {
            for (let i = 0; i < 3; i++) {
                this.bgPoolArr.push({
                    pool: new cc.NodePool(),
                    prefab: null
                });
            }
        }
        if (!this.bgPoolArr[index].prefab) {
            cc.resources.load("bgs/bg" + index, cc.Prefab, (err, prefab: cc.Prefab) => {
                if (err) {
                    console.error("bg=>err", err);
                    return;
                }
                this.bgPoolArr[index].prefab = prefab;
                this.bgPoolArr[index].pool.put(cc.instantiate(prefab));
                cb && cb(prefab);
            });
        }
    }
    getPoolNode(poolName: PoolType) {
        if (poolName === PoolType.BG) {
            // console.log(this.bgIndex,this.bgPoolArr[this.bgIndex]);
            if (this.bgPoolArr[this.bgIndex].pool.size() > 0) {
                return this.bgPoolArr[this.bgIndex].pool.get();
            }
            return cc.instantiate(this.bgPoolArr[this.bgIndex].prefab);
        }
        switch (poolName) {
            case PoolType.Water:
                this.pool = this.waterPool;
                this.poolPrefab = this.waterPrefab;
                break;
            case PoolType.WaterShade:
                this.pool = this.waterShadePool;
                this.poolPrefab = this.waterShadePrefab;
                break;
            case PoolType.Wall:
                this.pool = this.wallPool;
                this.poolPrefab = this.wallPrefab;
                break;
            default:
                break;
        }
        if (!this.pool) return;
        if (this.pool.size() > 0) {
            this.poolNode = this.pool.get();
        } else {
            this.poolNode = cc.instantiate(this.poolPrefab);
        }
        return this.poolNode;
    }
    //回收
    putPoolNode(poolName: PoolType, target: cc.Node) {
        switch (poolName) {
            case PoolType.BG:
                this.bgPoolArr[this.bgIndex].pool.put(target);
                return;
            case PoolType.Water:
                this.waterPool.put(target);
                return;
            case PoolType.WaterShade:
                this.waterShadePool.put(target);
                return;
            case PoolType.Wall:
                this.wallPool.put(target);
                return;
            default:
                console.error("该对象池不存在", poolName);
                return;
        }
    }
    /** 创建boss子弹*/
    loadBossBulletPool() {
        cc.resources.load("bossBullet", cc.Prefab, (err, res: cc.Prefab) => {
            if (err) {
                console.error("bossBullet=>err", err);
                return;
            }
            this.bossBulletPrefab = res;
            this.bossBulletPool.put(cc.instantiate(res));
        });
    }
    /** 创建boss子弹*/
    getBossBullet() {
        if (this.bossBulletPool.size() > 0) {
            return this.bossBulletPool.get();
        }
        return cc.instantiate(this.bossBulletPrefab);
    }
    /** 创建boss子弹*/
    putBossBullet(node) {
        this.bossBulletPool.put(node);
    }
    /** 创建鲨鱼子弹*/
    getBullet() {
        if (this.bulletPool.size() > 0) {
            return this.bulletPool.get();
        }
        return cc.instantiate(this.bulletPrefab);
    }
    getDom() {
        if (this.domPool.size() > 0) {
            return this.domPool.get();
        }
        return cc.instantiate(this.domPrefab)
    }
    /** 回收鲨鱼子弹*/
    putBullet(node: cc.Node) {
        this.bulletPool.put(node);
    }
    /**回收炮弹 */
    putDom(node: cc.Node) {
        this.domPool.put(node);
    }
    /**创建敌人*/
    getEnemy(isPerson: boolean, flag: number = null) {
        if (isPerson) {
            if (this.personPool.size() > 0) {
                return this.personPool.get();
            }
            return cc.instantiate(this.personPrefab);
        }
        let result = this.enemysPool[flag];
        if (result.pool.size() > 0) {
            return result.pool.get();
        }
        if (result.prefab) {
            return cc.instantiate(result.prefab);
        }
        return new Promise(
            (resolve: (node: cc.Node) => void, reject) => {
                let path: string = flag < 8 ? "elite/enemy" : "boss/enemy";
                path += flag;
                if (flag >= 16) { //机器人
                    path = "boss/robot";
                }
                cc.resources.load(path, cc.Prefab, (err, res: cc.Prefab) => {
                    if (err) {
                        console.error("getEnemy=>err", err);
                        return reject(err);
                    }
                    // console.log(res);
                    this.enemysPool[flag].prefab = res;
                    resolve(cc.instantiate(res));
                });
            }
        );
    }
    getUpLight() {
        if (this.UpPool.size() > 0) {
            return this.UpPool.get();
        }
        return cc.instantiate(this.UpPrefab)
    }
    /** 回收升级特效*/
    putUpLight(node: cc.Node) {
        this.UpPool.put(node);
    }
    /**
     * 回收敌人
     * @param node  要回收的节点
     * @param flag  敌人类别(序号)
     * @param isPerson 是不是小人,不是小人直接放空
     */
    putEnemy(node: cc.Node, flag: number, isPerson: boolean = false) {
        if (isPerson) {
            this.personPool.put(node);
            return;
        }
        this.enemysPool[flag].pool.put(node);
    }
    /**创建爆炸效果节点 */
    getBoomNode(): cc.Node {
        if (this.boomPool.size() > 0) {
            return this.boomPool.get();
        }
        return cc.instantiate(this.boomPrefab);
    }
    /**回收爆炸效果节点 */
    putBoomNode(node) {
        this.boomPool.put(node);
    }
    getBloodNode(): cc.Node {
        if (this.bloodPool.size() > 0) {
            return this.bloodPool.get();
        }
        return cc.instantiate(this.bloodPrefab);
    }
    putBloodNode(node) {
        this.bloodPool.put(node);
    }
    /**创建敌人掉落的金币 */
    getCoinNode(): cc.Node {
        if (this.coinPool.size() > 0) {
            return this.coinPool.get();
        }
        return cc.instantiate(this.coinPrefab);
    }
    /**回收敌人掉落的金币 */
    putCoinNode(node) {
        this.coinPool.put(node);
    }
}
