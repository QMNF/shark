// enum ClickType {
//     TOUCH_START = "touchstart",
//     TOUCH_MOVE = "touchmove",
//     TOUCH_END = "",
//     TOUCH_CANCEL = ""
// }

import PoolMag from "./PoolMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ToolMag extends cc.Component {

    private static _instance: ToolMag = null;
    public static get Ins(): ToolMag {
        if (!this._instance || this._instance == null) {
            this._instance = new ToolMag();
        }
        return this._instance;
    }

    screenSize: cc.Size = null;

    init() {
        this.screenSize = cc.view.getVisibleSize();
        this.OpenContact();
    }

    private num = 0;

    OpenContact(debugDrawFlags: boolean = false) {
        // console.log(debugDrawFlags)
        cc.director.getCollisionManager().enabled = true;
        cc.director.getCollisionManager().enabledDebugDraw = debugDrawFlags;
        // cc.director.getPhysicsManager().enabled = true;
        // cc.director.getPhysicsManager().gravity = cc.v2(0, -720);
        // cc.director.getPhysicsManager().debugDrawFlags = debugDrawFlags ? 1 : 0;

        // cc.director.getPhysicsManager().enabledAccumulator = true;

        //物理步长，默认 FIXED_TIME_STEP 是 1/60
        // cc.PhysicsManager.FIXED_TIME_STEP = 1 / 30;

        // 每次更新物理系统处理速度的迭代次数，默认为 10
        // cc.PhysicsManager.VELOCITY_ITERATIONS = 10;

        // 每次更新物理系统处理位置的迭代次数，默认为 10
        // cc.PhysicsManager.POSITION_ITERATIONS = 10;
    }

    /**
     * Tool |   弧度制转角度制
     * @param {*} Radian 
     */
    radianToAngle(Radian: number) {
        return Radian * 180 / Math.PI
    }
    /**
     * Tool |   角度制转弧度制
     * @param {*} Angle 
     */
    angleToRadian(Angle: number) {
        return Angle * Math.PI / 180
    }
    /**
    * 工具 |   生成随机整数
    * @param {*} minNum 
    * @param {*} maxNum 
    * @param {*} isFloor  是否向下取整
    */
    randomNum(minNum: number, maxNum: number, isFloor: boolean = true) {
        let num = Math.random() * ((maxNum + 1) - minNum) + minNum;
        return isFloor ? Math.floor(num) : num;
    }
    /**
     * @param animateNode  
     * @param animName 
     * @param times 播放次数 -1:龙骨默认值  0:无限循环  >0:循环次数
     * @param cb 
     */
    //播放龙骨动画方法
    playDragonBone(animateNode: cc.Node, animName: string, times: number, cb: Function = null) {
        // console.log(animName);
        if (!animateNode.active) animateNode.active = true;
        let dragonDisplay = animateNode.getComponent(dragonBones.ArmatureDisplay);
        // console.log(dragonDisplay);
        if (times == 0) {
            dragonDisplay.removeEventListener(dragonBones.EventObject.LOOP_COMPLETE);
            dragonDisplay.addEventListener(dragonBones.EventObject.LOOP_COMPLETE, () => {
                cb && cb();
            }, this);
        } else {
            dragonDisplay.removeEventListener(dragonBones.EventObject.COMPLETE);
            dragonDisplay.addEventListener(dragonBones.EventObject.COMPLETE, () => {
                cb && cb();
            }, this);
        }
        dragonDisplay.playAnimation(animName, times);
    }
    removeDragonBone(animateNode: cc.Node) {
        let dragonDisplay = animateNode.getComponent(dragonBones.ArmatureDisplay);
        // console.log(animateNode,dragonDisplay);
        dragonDisplay.removeEventListener(dragonBones.EventObject.COMPLETE);
    }
<<<<<<< HEAD
    showBlood(node: cc.Node, power: number) {
=======
<<<<<<< HEAD
    showBlood(node: cc.Node, power: number) {
=======
    showBlood(node: cc.Node) {
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        let blood = PoolMag.Ins.getBloodNode();
        blood.parent = cc.find("Canvas/gameMain/Terrain/bloodBox")
        blood.position = node.position
        blood.getComponent(cc.Animation).play();
<<<<<<< HEAD
        blood.getChildByName("power").getComponent(cc.Label).string = power + ""
=======
<<<<<<< HEAD
        blood.getChildByName("power").getComponent(cc.Label).string = power + ""
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    }
    showUpLight(shark: cc.Node) {
        let up = PoolMag.Ins.getUpLight();
        up.parent = shark;
        up.x = -5
        up.getComponent(cc.Animation).on("finished", () => {
            PoolMag.Ins.putUpLight(up)
            up.getComponent(cc.Animation).off("finished");
        })
        up.getComponent(cc.Animation).play();
    }
    /**
     * 二分查找法
     * @param arr 
     * @param target 
     */
    binarySearch(arr, key) {
        var low = 0;
        var high = arr.length - 1;
        while (low <= high) {
            var mid = Math.floor((low + high) / 2);
            if (key === arr[mid]) {
                return true;
            } else if (key > arr[mid]) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return false;
    }
    /**长震动 */
    public vibrateLong(): void {
        //@ts-ignore
        wx.vibrateLong();
    }
    /** 短震动 */
    public vibrateShort(): void {
        this.schedule(this._50msVibrate, 0.015)
    }

    _50msVibrate() {
        //@ts-ignore
        wx.vibrateShort();
        this.num++
        if (this.num >= 3) {
            this.num = 0
            this.unschedule(this._50msVibrate);
        }
    }
}