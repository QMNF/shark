// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ToolMag from "./ToolMag";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundMag {

    private static _instance: SoundMag = null;
    public static get Ins(): SoundMag {
        if (!this._instance || this._instance == null) {
            this._instance = new SoundMag();
        }
        return this._instance;
    }
    private HumanSound: cc.AudioClip[] = new Array<cc.AudioClip>();
    private boomSound: cc.AudioClip[] = new Array<cc.AudioClip>();
    private gunSound: cc.AudioClip[] = new Array<cc.AudioClip>();
    private GameBG: cc.AudioClip[] = new Array<cc.AudioClip>();
    private waterEff: cc.AudioClip[] = new Array<cc.AudioClip>();

    private sharkJump: cc.AudioClip = null;
    private sharkSwim: cc.AudioClip = null;
    private sharkMake: cc.AudioClip = null;
    private LvlUp: cc.AudioClip = null;
    private UpStar: cc.AudioClip = null;
<<<<<<< HEAD
    private unLockPart: cc.AudioClip = null;
=======
<<<<<<< HEAD
    private unLockPart: cc.AudioClip = null;
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97

    homeBG_ID: number;
    gameBG_ID: number;
    makeBG_ID: number;
    variationBG_ID: number;
    swim_ID: number;


    Init() {
        this.initHumanSound();
        this.initboomSound();
        this.initgunSound();
        this.initGameBG()
        this.initsharkJump();
        this.initsharkswim();
        this.initsharkMake();
        this.initLvLUP();
        this.initUpStar();
        this.initoutWater();
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        this.initunLockPart();
    }
    initunLockPart() {
        cc.loader.loadRes("bgm/获得新的部件", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.unLockPart = res;
        })
    }

    getunLockPart() {
        return this.unLockPart
    }

<<<<<<< HEAD
=======
=======
    }
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
    initoutWater() {
        let paths = ["bgm/出水", "bgm/入水1"]
        cc.loader.loadResArray(paths, cc.AudioClip, (error, res: cc.AudioClip[]) => {
            if (error) {
                console.error(error)
                return;
            }
            this.waterEff = res;
        })
    }

<<<<<<< HEAD
    getWater(index: number) {
=======
<<<<<<< HEAD
    getWater(index: number) {
=======
    getWater(index:number) {
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        return this.waterEff[index]
    }

    initLvLUP() {
        cc.loader.loadRes("bgm/升级成功", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.LvlUp = res;
        })
    }

    initUpStar() {
        cc.loader.loadRes("bgm/升星成功", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.UpStar = res;
        })
    }

    initsharkswim() {
        cc.loader.loadRes("bgm/鲨鱼游动1", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.sharkSwim = res;
        })
    }
    initsharkMake() {
        cc.loader.loadRes("bgm/变异音效", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.sharkMake = res;
        })
    }

    initsharkJump() {
        cc.loader.loadRes("bgm/鲨鱼跃出水面1", cc.AudioClip, (error, res: cc.AudioClip) => {
            if (error) {
                console.error(error)
                return;
            }
            this.sharkJump = res;
        })
    }
    getsharkJump() {
        return this.sharkJump;
    }

    getLvlUp() {
        return this.LvlUp;
    }

    getUpStar() {
        return this.UpStar;
    }

    getsharkMake() {
        return this.sharkMake;
    }

    getsharkSwim() {
        return this.sharkSwim;
    }

    initHumanSound() {
        let strarr = [];
        for (let i = 0; i < 9; i++) {
            strarr.push("bgm/humanSound/尖叫" + (i + 1))
        }
        cc.loader.loadResArray(strarr, (error, resArr) => {
            if (error) {
                console.error(error);
                return;
            }
            this.HumanSound = resArr;
        })
    }
    initboomSound() {
        let strarr = [];
        for (let i = 0; i < 7; i++) {
            strarr.push("bgm/boomSound/爆炸" + (i + 1))
        }
        cc.loader.loadResArray(strarr, (error, resArr) => {
            if (error) {
                console.error(error);
                return;
            }
            this.boomSound = resArr;
        })
    }
    initgunSound() {
        let strarr = [];
        for (let i = 0; i < 11; i++) {
            strarr.push("bgm/gunSound/枪声" + (i + 1))
        }
        cc.loader.loadResArray(strarr, (error, resArr) => {
            if (error) {
                console.error(error);
                return;
            }
            this.gunSound = resArr;
        })
    }
    initGameBG() {
        let strarr = [];
        for (let i = 0; i < 4; i++) {
            strarr.push("bgm/gameBG/鲨鱼战斗BGM" + (i + 1))
        }
        cc.loader.loadResArray(strarr, (error, resArr) => {
            if (error) {
                console.error(error);
                return;
            }
            this.GameBG = resArr;
        })
    }

    getHumanSoundNum() {
        return this.HumanSound.length
    }
    getHumanSound(index) {
        return this.HumanSound[index];
    }

    getgunSoundNum() {
        return this.gunSound.length
    }
    getgunSound(index = null) {
        if (!index) return this.gunSound[0];
        return this.gunSound[index];
    }

    getboomSoundNum() {
        return this.boomSound.length
    }
    getboomSound(index) {
        if (index)
            return this.boomSound[index];
    }
    playboomSound() {
        cc.audioEngine.playEffect(this.boomSound[ToolMag.Ins.randomNum(0, this.boomSound.length - 1)], false)
    }

    getGameBGNum() {
        return this.GameBG.length
    }
    getGameBG(index) {
        return this.GameBG[index];
    }
}
