import makeItem from "./makeItem";
import GameMag, { SharkSkin } from "./manage/GameMag";
import SoundMag from "./manage/SoundMag";
import StorageMag, { StorageKey } from "./manage/StorageMag";
import ToolMag from "./manage/ToolMag";


const { ccclass, property } = cc._decorator;
//改造界面
@ccclass
export default class MakePage extends cc.Component {

    @property(cc.SpriteAtlas)
    atlas: cc.SpriteAtlas = null;
    @property(cc.Node)
    shark: cc.Node = null;
    @property(cc.Sprite)
    sharkGun: cc.Sprite = null;
    @property(cc.Node)
    tabFalse: cc.Node = null;
    @property(cc.Node)
    tabTrue: cc.Node = null;
    @property(cc.Node)
    backBtn: cc.Node = null;
    @property(cc.ScrollView)
    scrollView: cc.ScrollView = null;
    @property(cc.Node)
    useBtn: cc.Node = null;
    @property(cc.Node)
    endBtn: cc.Node = null;
    @property(cc.Node)
    currency: cc.Node = null;
    @property(cc.Animation)
    electric: cc.Animation = null;
    @property(cc.Prefab)
    makeItem: cc.Prefab = null;
    @property(cc.Label)
    varyNum: cc.Label = null;
<<<<<<< HEAD

    @property(cc.Node)
    homeWater: cc.Node = null;
=======
<<<<<<< HEAD

    @property(cc.Node)
    homeWater: cc.Node = null;
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97

    clickItem: cc.Node;
    nowIndex: number = null;
    lastIndex: number = null;
    makeData: any[] = [[], [], [], [], []]

    onLoad() {
        var self = this;
        this.backBtn.on(cc.Node.EventType.TOUCH_END, () => {
            cc.game.emit("freshHomeShark");
            if (GameMag.Ins.isHomeIn) {
                cc.game.emit("initHomeUI")
            } else {
                cc.game.emit("onSwitchLevel");
            }
            this.node.active = false;
            this.useBtn.getComponent(cc.Button).interactable = true;
            this.electric.node.active = false;
            // this.electric.stop();
            cc.audioEngine.stopAllEffects();
        }, this);
        this.endBtn.on(cc.Node.EventType.TOUCH_END, () => {
            return;
        }, this);
        cc.game.on("changeEnd", this.changeVaryNum, this);
        this.electric.on("finished", () => {
            this.electric.node.active = false;
            this.useBtn.getComponent(cc.Button).interactable = true;
        })
        this.electric.on(cc.Animation.EventType.PLAY, () => {
            this.electric.node.active = true;
        });

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        cc.tween(this.homeWater)
            .then(cc.sequence(cc.moveBy(1, cc.v2(-84, 0)), cc.callFunc(() => {
                self.homeWater.x = 0
            }, this)))
            .repeatForever()
            .start();
<<<<<<< HEAD
=======
=======
>>>>>>> d66bc0d29d2c3fbf887f91cd2377b2b310d64250
>>>>>>> e10dfe02ca27b83a4c9e15a7639db423d2c8ec97
        // this.useBtn.on(cc.Node.EventType.TOUCH_END, this.onUseBtn, this);
    }

    changeVaryNum() {
        this.varyNum.string =
            (GameMag.Ins.sharkDiv
                + GameMag.Ins.sharkHp
                + GameMag.Ins.sharkPower
                + GameMag.Ins.sharkBitePower
            ) + ""
    }

    startMake() {
        //开始变异
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        this.useBtn.getComponent(cc.Button).interactable = false;
        let usingData = StorageMag.Ins.getStorage(StorageKey.Using);
        let num = 0;
        switch (this.nowIndex) {
            case 1:
                usingData.yushen = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.yushenStar = this.clickItem.getComponent(makeItem).data.star;
                break;
            case 2:
                usingData.yuqi = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.yuqiStar = this.clickItem.getComponent(makeItem).data.star;
                break;
            case 3:
                usingData.yuwei = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.yuweiStar = this.clickItem.getComponent(makeItem).data.star;
                break;
            case 0:
                usingData.yuzui = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.yuzuiStar = this.clickItem.getComponent(makeItem).data.star;
                break;
            case 5:
                usingData.futou = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.futouStar = this.clickItem.getComponent(makeItem).data.star;
                break;
            default:
                usingData.gun = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
                usingData.gunStar = this.clickItem.getComponent(makeItem).data.star;
                break;
        }
        num = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2;
        StorageMag.Ins.freshUsingData(usingData);
        this.itemClick(null);
        // ToolMag.Ins.playDragonBone(this.electric, "animation1", 1, () => {
        //     this.useBtn.getComponent(cc.Button).interactable = true;
        // })
        this.electric.node.active = true
        this.electric.play();
        this.freshSharkSkin();
        cc.audioEngine.playEffect(SoundMag.Ins.getsharkMake(), false)
        for (let i = 0; i < this.scrollView.content.childrenCount; i++) {
            if (i == num) {
                this.scrollView.content.children[num].getChildByName("light").active = true;
            } else {
                this.scrollView.content.children[i].getChildByName("light").active = false;
            }
        }
        cc.game.emit("changeSharkData");
    }

    getData() {
        var data = StorageMag.Ins.getStorage(StorageKey.Get);
        this.makeData = [[], [], [], [], [], []]
        for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < data[i].length; j++) {
                if (Math.floor(data[i][j].ID % 1000) == 5) {
                    this.makeData[0].push(data[i][j]);
                }
                if (Math.floor(data[i][j].ID % 1000) == 2) {
                    this.makeData[1].push(data[i][j]);
                }
                if (Math.floor(data[i][j].ID % 1000) == 3) {
                    this.makeData[2].push(data[i][j]);
                }
                if (Math.floor(data[i][j].ID % 1000) == 4) {
                    this.makeData[3].push(data[i][j]);
                }
                if (Math.floor(data[i][j].ID % 1000) == 1) {
                    this.makeData[4].push(data[i][j]);
                }
                if (Math.floor(data[i][j].ID % 1000) == 6) {
                    this.makeData[5].push(data[i][j]);
                }
            }
        }
        // console.log("makeData", this.makeData)
    }

    setmakeData(index) {
        let tmp = 0;
        if (index == 0) {
            tmp = 4
        } else if (index == 4) {
            tmp = 0;
        } else {
            tmp = index
        }
        this.scrollView.scrollToPercentVertical(1)
        for (let i = 0; i < this.makeData[tmp].length; i++) {
            if (!this.makeData[tmp][i].geted) continue;
            let item = cc.instantiate(this.makeItem);
            item.getComponent(makeItem).setInfo(this.makeData[tmp][i]);
            this.scrollView.content.addChild(item);
            item.x = 0
            item.getChildByName("light").active = false;
            item.on(cc.Node.EventType.TOUCH_END, this.itemClick, this)
        }
        let num = 0;
        let usingData3 = StorageMag.Ins.getStorage(StorageKey.Using);
        switch (index) {
            case 1:
                num = usingData3.yushen
                break;
            case 2:
                num = usingData3.yuqi
                break;
            case 3:
                num = usingData3.yuwei
                break;
            case 0:
                num = usingData3.yuzui
                break;
            case 5:
                num = usingData3.futou
                break;
            default:
                num = usingData3.gun
                break;
        }
        this.clickItem = this.scrollView.content.children[num];
        this.clickItem.getChildByName("light").active = true;
        this.itemClick({ target: this.clickItem });
        this.initSharkSkin();
    }

    itemClick(e) {
        if (!e) {
            this.clickItem.getChildByName("switch").active = true;
        } else {
            this.clickItem.getChildByName("switch").active = false;
            this.clickItem = e.target;
            this.clickItem.getChildByName("switch").active = true;
        }
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        let num = 0;
        let usingData3 = StorageMag.Ins.getStorage(StorageKey.Using);
        switch (this.nowIndex) {
            case 1:
                num = usingData3.yushen
                break;
            case 2:
                num = usingData3.yuqi
                break;
            case 3:
                num = usingData3.yuwei
                break;
            case 0:
                num = usingData3.yuzui
                break;
            case 5:
                num = usingData3.futou
                break;
            default:
                num = usingData3.gun
                break;
        }
        this.endBtn.active = Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2 == num
        this.shark.getChildByName("knifeNode").active = this.nowIndex == 5
        this.shark.getChildByName("knifeNode").children.forEach((child) => {
            child.active = false;
        })
        this.shark.getChildByName("knifeNode").children[StorageMag.Ins.getStorage(StorageKey.Using).futou].active = true
    }

    cleanContent() {
        this.scrollView.content.removeAllChildren();
    }

    onEnable() {
        this.lastIndex = 0;
        this.tabClick(null, 0);
        cc.game.emit("changeSharkData");
        GameMag.Ins.sharkChangeSkin(this.shark);
    }
    onDisable() {
        this.tabFalse.children[this.lastIndex].active = true;
        this.tabTrue.children[this.nowIndex].active = false;
        this.electric.off("finished", () => {
            this.electric.node.active = false;
            this.useBtn.getComponent(cc.Button).interactable = true;
        })
        this.electric.off(cc.Animation.EventType.PLAY, () => {
            this.electric.node.active = true;
        });
    }
    tabClick(e, index) {
        // this.pageView.setCurrentPageIndex(0);
        if (cc.sys.platform === cc.sys.WECHAT_GAME && StorageMag.Ins.getStorage(StorageKey.HaveShock))
            ToolMag.Ins.vibrateShort();
        this.nowIndex = Number(index);
        this.tabFalse.children[this.lastIndex].active = true;
        this.tabFalse.children[this.nowIndex].active = false;
        this.tabTrue.children[this.lastIndex].active = false;
        this.tabTrue.children[this.nowIndex].active = true;
        let str: string = null;
        switch (this.nowIndex) {
            case 0:
                str = "yuzui";
                break;
            case 1:
                str = "yushen";
                break;
            case 2:
                str = "yuqi";
                break;
            case 3:
                str = "yuwei";
                break;
            case 4:
                str = "gun";
                break;
            case 5:
                str = "futou"
                break;
            default:
                break;
        }
        // this.pageView.getPages().forEach((node, i) => {
        //     node.getChildByName("icon").getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(str + i);
        // })
        // this.scrollPage(this.getSkin());
        this.lastIndex = this.nowIndex;
        this.getData();
        this.cleanContent();
        this.setmakeData(this.nowIndex);
    }
    freshSharkSkin() {
        let part: SharkSkin = null;
        let tmp: SharkSkin = null;
        switch (this.nowIndex) {
            case 1:
                part = SharkSkin.Body;
                break;
            case 2:
                part = SharkSkin.Fins;
                break;
            case 3:
                part = SharkSkin.Tail;
                break;
            case 4:
                this.sharkGun.spriteFrame = this.atlas.getSpriteFrame(this.clickItem.getComponent(makeItem).data.img);
                return;
            case 5:
                // this.sharkGun.spriteFrame = this.atlas.getSpriteFrame(this.clickItem.getComponent(makeItem).data.img);
                this.shark.getChildByName("knifeNode").children.forEach((child) => {
                    child.active = false;
                })
                this.shark.getChildByName("knifeNode").children[StorageMag.Ins.getStorage(StorageKey.Using).futou].active = true
                return;
            default:
                part = SharkSkin.Chin;
                tmp = SharkSkin.Maxilla;
                break;
        }
        GameMag.Ins.sharkSingleSkin(this.shark, part, Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2);
        if (tmp) {
            GameMag.Ins.sharkSingleSkin(this.shark, tmp, Math.floor(this.clickItem.getComponent(makeItem).data.ID / 1000) - 2);
            tmp = null;
        }
    }
    initSharkSkin() {
        const using = StorageMag.Ins.getStorage(StorageKey.Using);
        const gunID = using ? using.gun : 0;
        this.sharkGun.spriteFrame = this.atlas.getSpriteFrame("gun" + gunID);
        GameMag.Ins.sharkChangeSkin(this.shark);
    }
    getSkin(): number {
        let usingData = StorageMag.Ins.getStorage(StorageKey.Using);
        switch (this.nowIndex) {
            case 1:
                return usingData.yushen;
            case 2:
                return usingData.yuqi;
            case 3:
                return usingData.yuwei;
            case 0:
                return usingData.yuzui;
            case 0:
                return usingData.futou;
            default:
                return usingData.gun;
        }
    }
    onUseBtn() {
        let usingData = JSON.parse(JSON.stringify(StorageMag.Ins.getStorage(StorageKey.Using)));
        // let index = this.pageView.getCurrentPageIndex();
        let index = 1
        switch (this.nowIndex) {
            case 1:
                usingData.yushen = index;
                break;
            case 2:
                usingData.yushen = index;
                break;
            case 3:
                usingData.yuwei = index;
                break;
            case 0:
                usingData.yuzui = index;
                break;
            default:
                usingData.gun = index;
                break;
        }
        StorageMag.Ins.freshUsingData(usingData);
    }
    onDiamondBtn() {
        // const index = this.pageView.getCurrentPageIndex();
        let num = 0;
        if (this.nowIndex == 0) {
            num = 4
        } else if (this.nowIndex == 4) {
            num = 0;
        } else {
            num = this.nowIndex
        }
        const data = GameMag.Ins.sharkJson[num][1];
        cc.game.emit("addDiamond", -data.getByDiamond, (success) => {
            if (!success) return;
            this.rewardSkin();
        });
    }
    onVideoBtn() {
        this.rewardSkin();
    }
    rewardSkin() {
        // const index = this.pageView.getCurrentPageIndex();
        // StorageMag.Ins.freshGetData(this.nowIndex, 1);
        // this.scrollPage(index);
        this.onUseBtn();
    }
}
