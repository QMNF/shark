import ADpage from "./ADpage";
import StorageMag, { StorageKey } from "./manage/StorageMag";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Currency extends cc.Component {

    @property(cc.Node)
    ADpage: cc.Node = null;

    @property(cc.Node)
    coinNode: cc.Node = null;
    @property(cc.Node)
    diamondNode: cc.Node = null;
    @property(cc.Node)
    SanNode: cc.Node = null;

    @property(cc.Label)
    coinLab: cc.Label = null;
    @property(cc.Label)
    diamondLab: cc.Label = null;
    @property(cc.Label)
    SanLab: cc.Label = null;

    addODNode: cc.Node;
    addCoinNode: cc.Node;
    addSanNode: cc.Node;

    onLoad() {
        cc.game.on("addCoin", this.freshCoin, this);
        cc.game.on("addDiamond", this.freshDiamond, this);
        this.diamondNode.on(cc.Node.EventType.TOUCH_END, this.addOD, this)
        this.coinNode.on(cc.Node.EventType.TOUCH_END, this.addCoin, this)
        this.SanNode.on(cc.Node.EventType.TOUCH_END, this.addSan, this)
    }
    onEnable() {

    }

    init() {
        let data = StorageMag.Ins.getStorage(StorageKey.Currency);
        this.coinLab.string = this.cashUI(data.coin);
        this.diamondLab.string = this.cashUI(data.diamond);
    }

    freshCoin(num: number, cb: Function = null) {
        let data = StorageMag.Ins.getStorage(StorageKey.Currency);
        if (data.coin + num < 0) {
            cb && cb(false);
            return;
        }
        data.coin += num;
        this.cashUI(data.coin);
        this.coinLab.string = this.cashUI(data.coin);
        StorageMag.Ins.freshCurrency(data);
        cb && cb(true);
    }
    freshDiamond(num: number, cb: Function = null) {
        let data = StorageMag.Ins.getStorage(StorageKey.Currency);
        if (data.diamond + num < 0) {
            cb && cb(false);
            return;
        }
        data.diamond += num;
        this.diamondLab.string = this.cashUI(data.diamond);
        StorageMag.Ins.freshCurrency(data);
        cb && cb(true);
    }
    cashUI(num: number) {
        let str = "";
        let tmp = parseInt(num + "").toString();
        if (num > 10000) {
            str = "w";
            tmp = (num / 10000).toFixed(1);
        }
        return tmp + str;
    }

    addSan() {
        this.ADpage.getComponent(ADpage).show(3)
    }

    addOD() {
        this.ADpage.getComponent(ADpage).show(2)
    }

    addCoin() {
        this.ADpage.getComponent(ADpage).show(1)
    }
}
